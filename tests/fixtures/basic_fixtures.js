const express = require('express');

module.exports = {
    user: {
        build: 'user',
        object: null
    },
    other_user: {
        build: 'user',
        object: null
    },
    application: {
        build: 'application',
        object: null
    },
    not_root_application: {
        build: 'application',
        object: null
    },
    other_user_application: {
        build: 'application',
        object: null
    },
    entity: {
        build: 'entity',
        object: null
    },
    not_root_entity: {
        build: 'entity',
        object: null
    },
    status_active: {
        build: 'status',
        object: null
    },
    status_inactive: {
        build: 'status',
        object: null
    },
    not_root_status: {
        build: 'status',
        object: null
    },
    workflow: {
        build: 'workflow',
        object: null
    },
    not_root_workflow: {
        build: 'workflow',
        object: null
    },
    transition_initial: {
        build: 'transition',
        object: null
    },
    conditional_transition_initial_1: {
        build: 'transition',
        object: null
    },
    conditional_transition_initial_2: {
        build: 'transition',
        object: null
    },
    conditional_transition_initial_3: {
        build: 'transition',
        object: null
    },
    transition_active_inactive: {
        build: 'transition',
        object: null
    },
    transition_inactive_active: {
        build: 'transition',
        object: null
    },
    not_root_transition: {
        build: 'transition',
        object: null
    },
    conditional_transition_1: {
        build: 'transition',
        object: null
    },
    conditional_transition_2: {
        build: 'transition',
        object: null
    },
    conditional_transition_3: {
        build: 'transition',
        object: null
    },
    after_function: {
        build: 'after_function',
        object: null
    },
    before_function: {
        build: 'before_function',
        object: null
    },
    validation: {
        build: 'validation',
        object: null
    },
    validation_linked_1: {
        build: 'validation',
        object: null
    },
    validation_linked_2: {
        build: 'validation',
        object: null
    },
    validation_linked_3: {
        build: 'validation',
        object: null
    },
    new_validation_linked: {
        build: 'validation',
        object: null
    },
    validation_w_exp_1: {
        build: 'validation',
        object: null
    },
    condition: {
        build: 'condition',
        object: null
    },
    condition_linked_1: {
        build: 'condition',
        object: null
    },
    condition_linked_2: {
        build: 'condition',
        object: null
    },
    condition_linked_3: {
        build: 'condition',
        object: null
    },
    new_condition_linked: {
        build: 'condition',
        object: null
    },
    condition_w_exp_1: {
        build: 'condition',
        object: null
    },
    validation_transition: {
        build: 'validation_transition',
        object: null
    },
    validation_transition_2: {
        build: 'validation_transition',
        object: null
    },
    validation_transition_3: {
        build: 'validation_transition',
        object: null
    },
    validation_w_exp_transition_1: {
        build: 'validation_transition',
        object: null
    },
    validation_w_exp_transition_2: {
        build: 'validation_transition',
        object: null
    },
    validation_w_exp_initial_1: {
        build: 'validation_transition',
        object: null
    },
    validation_w_exp_initial_2: {
        build: 'validation_transition',
        object: null
    },
    condition_transition: {
        build: 'condition_transition',
        object: null
    },
    condition_transition_2: {
        build: 'condition_transition',
        object: null
    },
    condition_transition_3: {
        build: 'condition_transition',
        object: null
    },
    condition_w_exp_transition_1: {
        build: 'condition_transition',
        object: null
    },
    condition_w_exp_transition_2: {
        build: 'condition_transition',
        object: null
    },
    condition_w_exp_transition_3: {
        build: 'condition_transition',
        object: null
    },
    condition_w_exp_initial_1: {
        build: 'condition_transition',
        object: null
    },
    condition_w_exp_initial_2: {
        build: 'condition_transition',
        object: null
    },
    condition_w_exp_initial_3: {
        build: 'condition_transition',
        object: null
    },
    after_function_transition: {
        build: 'after_function_transition',
        object: null
    },
    before_function_transition: {
        build: 'before_function_transition',
        object: null
    }
};
