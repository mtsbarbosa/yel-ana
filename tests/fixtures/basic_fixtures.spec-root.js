let factoryGirlConfig = require('../config/factory-girl-config');
const requireDir = require('require-dir');
let models = requireDir('../../models');
let basic_fixtures = require('./basic_fixtures');

before(async() => {
  try {
      await factoryGirlConfig.loadFactories(factoryGirlConfig);

      let userBuild = await factoryGirlConfig.factoryGirl.build('user');
      let user = await models.user.model().create(userBuild);
      basic_fixtures.user.object = user;

      let otherUserBuild = await factoryGirlConfig.factoryGirl.build('other_user');
      let otherUser = await models.user.model().create(otherUserBuild);
      basic_fixtures.other_user.object = otherUser;

      let applicationBuild = await factoryGirlConfig.factoryGirl.build('application', {user: user._id});
      let application = await models.application.model().create(applicationBuild);
      basic_fixtures.application.object = application;

      let notRootApplicationBuild = await factoryGirlConfig.factoryGirl.build('not_root_application', {user: user._id});
      let notRootApplication = await models.application.model().create(notRootApplicationBuild);
      basic_fixtures.not_root_application.object = notRootApplication;

      let otherUserApplicationBuild = await factoryGirlConfig.factoryGirl.build('other_user_application', {user: otherUser._id});
      let otherUserApplication = await models.application.model().create(otherUserApplicationBuild);
      basic_fixtures.other_user_application.object = otherUserApplication;

      let entityBuild = await factoryGirlConfig.factoryGirl.build('entity', {application: application._id});
      let entity = await models.entity.model().create(entityBuild);
      basic_fixtures.entity.object = entity;

      let notRootEntityBuild = await factoryGirlConfig.factoryGirl.build('not_root_entity', {application: notRootApplication._id});
      let notRootEntity = await models.entity.model().create(notRootEntityBuild);
      basic_fixtures.not_root_entity.object = notRootEntity;

      let status_active_build = await factoryGirlConfig.factoryGirl.build('status_active', {entity: entity._id, application: application._id});
      let status_active = await models.status.model().create(status_active_build);
      basic_fixtures.status_active.object = status_active;

      let status_inactive_build = await factoryGirlConfig.factoryGirl.build('status_inactive', {entity: entity._id, application: application._id});
      let status_inactive = await models.status.model().create(status_inactive_build);
      basic_fixtures.status_inactive.object = status_inactive;

      let not_root_status_build = await factoryGirlConfig.factoryGirl.build('not_root_status', {entity: notRootEntity._id, application: notRootApplication._id});
      let not_root_status = await models.status.model().create(not_root_status_build);
      basic_fixtures.not_root_status.object = not_root_status;

      let workflowBuild = await factoryGirlConfig.factoryGirl.build('workflow', {entity: entity._id, application: application._id});
      let workflow = await models.workflow.model().create(workflowBuild);
      basic_fixtures.workflow.object = workflow;

      let notRootWorkflowBuild = await factoryGirlConfig.factoryGirl.build('not_root_workflow', {entity: notRootEntity._id, application: notRootApplication._id});
      let notRootWorkflow = await models.workflow.model().create(notRootWorkflowBuild);
      basic_fixtures.not_root_workflow.object = notRootWorkflow;

      let transition_initial_build =
          await factoryGirlConfig.factoryGirl.build('transition_initial', {
              workflow: workflow._id,
              status_to: status_active._id,
              application: application._id
          });
      let transition_initial = await models.transition.model().create(transition_initial_build);
      basic_fixtures.transition_initial.object = transition_initial;

      let conditional_transition_initial_1_build =
          await factoryGirlConfig.factoryGirl.build('conditional_transition_initial_1', {
              workflow: workflow._id,
              status_to: status_active._id,
              application: application._id
          });
      let conditional_transition_initial_1 = await models.transition.model().create(conditional_transition_initial_1_build);
      basic_fixtures.conditional_transition_initial_1.object = conditional_transition_initial_1;

      let conditional_transition_initial_2_build =
          await factoryGirlConfig.factoryGirl.build('conditional_transition_initial_2', {
              workflow: workflow._id,
              status_to: status_active._id,
              application: application._id
          });
      let conditional_transition_initial_2 = await models.transition.model().create(conditional_transition_initial_2_build);
      basic_fixtures.conditional_transition_initial_2.object = conditional_transition_initial_2;

      let conditional_transition_initial_3_build =
          await factoryGirlConfig.factoryGirl.build('conditional_transition_initial_3', {
              workflow: workflow._id,
              status_to: status_active._id,
              application: application._id
          });
      let conditional_transition_initial_3 = await models.transition.model().create(conditional_transition_initial_3_build);
      basic_fixtures.conditional_transition_initial_3.object = conditional_transition_initial_3;

      let transition_active_inactive_build =
          await factoryGirlConfig.factoryGirl.build('transition_active_inactive', {
              workflow: workflow._id,
              status_from: status_active._id,
              status_to: status_inactive._id,
              application: application._id
          });
      let transition_active_inactive = await models.transition.model().create(transition_active_inactive_build);
      basic_fixtures.transition_active_inactive.object = transition_active_inactive;

      let transition_inactive_active_build = await factoryGirlConfig.factoryGirl.build('transition_inactive_active', {
          workflow: workflow._id,
          status_from: status_inactive._id,
          status_to: status_active._id,
          application: application._id
      });
      let transition_inactive_active = await models.transition.model().create(transition_inactive_active_build);
      basic_fixtures.transition_inactive_active.object = transition_inactive_active;

      let conditional_transition_1_build = await factoryGirlConfig.factoryGirl.build('conditional_transition_1', {
          workflow: workflow._id,
          status_from: status_active._id,
          status_to: status_inactive._id,
          application: application._id
      });
      let conditional_transition_1 = await models.transition.model().create(conditional_transition_1_build);
      basic_fixtures.conditional_transition_1.object = conditional_transition_1;

      let conditional_transition_2_build = await factoryGirlConfig.factoryGirl.build('conditional_transition_2', {
          workflow: workflow._id,
          status_from: status_active._id,
          status_to: status_inactive._id,
          application: application._id
      });
      let conditional_transition_2 = await models.transition.model().create(conditional_transition_2_build);
      basic_fixtures.conditional_transition_2.object = conditional_transition_2;

      let conditional_transition_3_build = await factoryGirlConfig.factoryGirl.build('conditional_transition_3', {
          workflow: workflow._id,
          status_from: status_active._id,
          status_to: status_inactive._id,
          application: application._id
      });
      let conditional_transition_3 = await models.transition.model().create(conditional_transition_3_build);
      basic_fixtures.conditional_transition_3.object = conditional_transition_3;

      let after_function_build = await factoryGirlConfig.factoryGirl.build('after_function', {entity: entity._id, application: application._id});
      let after_function = await models.after_function.model().create(after_function_build);
      basic_fixtures.after_function.object = after_function;

      let before_function_build = await factoryGirlConfig.factoryGirl.build('before_function', {entity: entity._id, application: application._id});
      let before_function = await models.before_function.model().create(before_function_build);
      basic_fixtures.before_function.object = before_function;

      let validation_build = await factoryGirlConfig.factoryGirl.build('validation', {entity: entity._id, application: application._id});
      let validation = await models.validation.model().create(validation_build);
      basic_fixtures.validation.object = validation;

      let condition_build = await factoryGirlConfig.factoryGirl.build('condition', {entity: entity._id, application: application._id});
      let condition = await models.condition.model().create(condition_build);
      basic_fixtures.condition.object = condition;

      let validation_linked_1_build = await factoryGirlConfig.factoryGirl.build('validation_linked_1', {entity: entity._id, application: application._id});
      let validation_linked_1 = await models.validation.model().create(validation_linked_1_build);
      basic_fixtures.validation_linked_1.object = validation_linked_1;

      let validation_linked_2_build = await factoryGirlConfig.factoryGirl.build('validation_linked_2', {entity: entity._id, application: application._id});
      let validation_linked_2 = await models.validation.model().create(validation_linked_2_build);
      basic_fixtures.validation_linked_2.object = validation_linked_2;

      let validation_linked_3_build = await factoryGirlConfig.factoryGirl.build('validation_linked_3', {entity: entity._id, application: application._id});
      let validation_linked_3 = await models.validation.model().create(validation_linked_3_build);
      basic_fixtures.validation_linked_3.object = validation_linked_3;

      let new_validation_linked_build = await factoryGirlConfig.factoryGirl.build('new_validation_linked', {entity: entity._id, application: application._id});
      let new_validation_linked = await models.validation.model().create(new_validation_linked_build);
      basic_fixtures.new_validation_linked.object = new_validation_linked;

      let validation_w_exp_1_build = await factoryGirlConfig.factoryGirl.build('validation_w_exp_1', {
          entity: entity._id,
          expression: 'id of transition in ("' + conditional_transition_2._id.toString() + '", "' + conditional_transition_initial_2._id.toString() + '") and message of data of req("GET","http://localhost:3000/yel-ana/test/bool_data")',
          application: application._id});
      let validation_w_exp_1 = await models.validation.model().create(validation_w_exp_1_build);
      basic_fixtures.validation_w_exp_1.object = validation_w_exp_1;

      let validation_transition_build = await factoryGirlConfig.factoryGirl.build('validation_transition', {
          validation: validation_linked_1._id,
          transition: transition_active_inactive._id,
          application: application._id
      });
      let validation_transition = await models.validation_transition.model().create(validation_transition_build);
      basic_fixtures.validation_transition.object = validation_transition;

      let validation_transition_2_build = await factoryGirlConfig.factoryGirl.build('validation_transition', {
          validation: validation_linked_2._id,
          transition: transition_active_inactive._id,
          application: application._id
      });
      let validation_transition_2 = await models.validation_transition.model().create(validation_transition_2_build);
      basic_fixtures.validation_transition_2.object = validation_transition_2;

      let validation_transition_3_build = await factoryGirlConfig.factoryGirl.build('validation_transition', {
          validation: validation_linked_3._id,
          transition: transition_active_inactive._id,
          application: application._id
      });
      let validation_transition_3 = await models.validation_transition.model().create(validation_transition_3_build);
      basic_fixtures.validation_transition_3.object = validation_transition_3;

      let validation_w_exp_transition_1_build = await factoryGirlConfig.factoryGirl.build('validation_w_exp_transition_1', {
          validation: validation_w_exp_1._id,
          transition: conditional_transition_1._id,
          application: application._id
      });
      let validation_w_exp_transition_1 = await models.validation_transition.model().create(validation_w_exp_transition_1_build);
      basic_fixtures.validation_w_exp_transition_1.object = validation_w_exp_transition_1;

      let validation_w_exp_transition_2_build = await factoryGirlConfig.factoryGirl.build('validation_w_exp_transition_2', {
          validation: validation_w_exp_1._id,
          transition: conditional_transition_2._id,
          application: application._id
      });
      let validation_w_exp_transition_2 = await models.validation_transition.model().create(validation_w_exp_transition_2_build);
      basic_fixtures.validation_w_exp_transition_2.object = validation_w_exp_transition_2;

      let validation_w_exp_initial_1_build = await factoryGirlConfig.factoryGirl.build('validation_w_exp_initial_1', {
          validation: validation_w_exp_1._id,
          transition: conditional_transition_initial_1._id,
          application: application._id
      });
      let validation_w_exp_initial_1 = await models.validation_transition.model().create(validation_w_exp_initial_1_build);
      basic_fixtures.validation_w_exp_initial_1.object = validation_w_exp_initial_1;

      let validation_w_exp_initial_2_build = await factoryGirlConfig.factoryGirl.build('validation_w_exp_initial_2', {
          validation: validation_w_exp_1._id,
          transition: conditional_transition_initial_2._id,
          application: application._id
      });
      let validation_w_exp_initial_2 = await models.validation_transition.model().create(validation_w_exp_initial_2_build);
      basic_fixtures.validation_w_exp_initial_2.object = validation_w_exp_initial_2;

      let condition_linked_1_build = await factoryGirlConfig.factoryGirl.build('condition_linked_1', {entity: entity._id, application: application._id});
      let condition_linked_1 = await models.condition.model().create(condition_linked_1_build);
      basic_fixtures.condition_linked_1.object = condition_linked_1;

      let condition_linked_2_build = await factoryGirlConfig.factoryGirl.build('condition_linked_2', {entity: entity._id, application: application._id});
      let condition_linked_2 = await models.condition.model().create(condition_linked_2_build);
      basic_fixtures.condition_linked_2.object = condition_linked_2;

      let condition_linked_3_build = await factoryGirlConfig.factoryGirl.build('condition_linked_3', {entity: entity._id, application: application._id});
      let condition_linked_3 = await models.condition.model().create(condition_linked_3_build);
      basic_fixtures.condition_linked_3.object = condition_linked_3;

      let new_condition_linked_build = await factoryGirlConfig.factoryGirl.build('new_condition_linked', {entity: entity._id, application: application._id});
      let new_condition_linked = await models.condition.model().create(new_condition_linked_build);
      basic_fixtures.new_condition_linked.object = new_condition_linked;

      let condition_w_exp_1_build = await factoryGirlConfig.factoryGirl.build('condition_w_exp_1', {
          entity: entity._id,
          expression: 'id of transition in ("' + conditional_transition_1._id.toString() + '", "' + conditional_transition_2._id.toString() + '", "' + conditional_transition_initial_1._id.toString() + '", "' + conditional_transition_initial_2._id.toString() + '") and message of data of req("GET","http://localhost:3000/yel-ana/test/bool_data")',
          application: application._id});
      let condition_w_exp_1 = await models.condition.model().create(condition_w_exp_1_build);
      basic_fixtures.condition_w_exp_1.object = condition_w_exp_1;

      let condition_transition_build = await factoryGirlConfig.factoryGirl.build('condition_transition', {
          condition: condition_linked_1._id,
          transition: transition_active_inactive._id,
          application: application._id
      });
      let condition_transition = await models.condition_transition.model().create(condition_transition_build);
      basic_fixtures.condition_transition.object = condition_transition;

      let condition_transition_2_build = await factoryGirlConfig.factoryGirl.build('condition_transition', {
          condition: condition_linked_2._id,
          transition: transition_active_inactive._id,
          application: application._id
      });
      let condition_transition_2 = await models.condition_transition.model().create(condition_transition_2_build);
      basic_fixtures.condition_transition_2.object = condition_transition_2;

      let condition_transition_3_build = await factoryGirlConfig.factoryGirl.build('condition_transition', {
          condition: condition_linked_3._id,
          transition: transition_active_inactive._id,
          application: application._id
      });
      let condition_transition_3 = await models.condition_transition.model().create(condition_transition_3_build);
      basic_fixtures.condition_transition_3.object = condition_transition_3;

      let condition_w_exp_transition_1_build = await factoryGirlConfig.factoryGirl.build('condition_w_exp_transition_1', {
          condition: condition_w_exp_1._id,
          transition: conditional_transition_1._id,
          application: application._id
      });
      let condition_w_exp_transition_1 = await models.condition_transition.model().create(condition_w_exp_transition_1_build);
      basic_fixtures.condition_w_exp_transition_1.object = condition_w_exp_transition_1;

      let condition_w_exp_transition_2_build = await factoryGirlConfig.factoryGirl.build('condition_w_exp_transition_2', {
          condition: condition_w_exp_1._id,
          transition: conditional_transition_2._id,
          application: application._id
      });
      let condition_w_exp_transition_2 = await models.condition_transition.model().create(condition_w_exp_transition_2_build);
      basic_fixtures.condition_w_exp_transition_2.object = condition_w_exp_transition_2;

      let condition_w_exp_transition_3_build = await factoryGirlConfig.factoryGirl.build('condition_w_exp_transition_3', {
          condition: condition_w_exp_1._id,
          transition: conditional_transition_3._id,
          application: application._id
      });
      let condition_w_exp_transition_3 = await models.condition_transition.model().create(condition_w_exp_transition_3_build);
      basic_fixtures.condition_w_exp_transition_3.object = condition_w_exp_transition_3;

      let condition_w_exp_initial_1_build = await factoryGirlConfig.factoryGirl.build('condition_w_exp_initial_1', {
          condition: condition_w_exp_1._id,
          transition: conditional_transition_initial_1._id,
          application: application._id
      });
      let condition_w_exp_initial_1 = await models.condition_transition.model().create(condition_w_exp_initial_1_build);
      basic_fixtures.condition_w_exp_initial_1.object = condition_w_exp_initial_1;

      let condition_w_exp_initial_2_build = await factoryGirlConfig.factoryGirl.build('condition_w_exp_initial_2', {
          condition: condition_w_exp_1._id,
          transition: conditional_transition_initial_2._id,
          application: application._id
      });
      let condition_w_exp_initial_2 = await models.condition_transition.model().create(condition_w_exp_initial_2_build);
      basic_fixtures.condition_w_exp_initial_2.object = condition_w_exp_initial_2;

      let condition_w_exp_initial_3_build = await factoryGirlConfig.factoryGirl.build('condition_w_exp_initial_3', {
          condition: condition_w_exp_1._id,
          transition: conditional_transition_initial_3._id,
          application: application._id
      });
      let condition_w_exp_initial_3 = await models.condition_transition.model().create(condition_w_exp_initial_3_build);
      basic_fixtures.condition_w_exp_initial_3.object = condition_w_exp_initial_3;

      let after_function_transition_build = await factoryGirlConfig.factoryGirl.build('after_function_transition', {
          after_function: after_function._id,
          transition: transition_active_inactive._id,
          application: application._id
      });
      let after_function_transition = await models.after_function_transition.model().create(after_function_transition_build);
      basic_fixtures.after_function_transition.object = after_function_transition;

      let before_function_transition_build = await factoryGirlConfig.factoryGirl.build('before_function_transition', {
          before_function: before_function._id,
          transition: transition_active_inactive._id,
          application: application._id
      });
      let before_function_transition = await models.before_function_transition.model().create(before_function_transition_build);
      basic_fixtures.before_function_transition.object = before_function_transition;
  }catch(error){
     console.log(error);
  }

});

after(async() => {
  let mongoose = require('mongoose');
  await factoryGirlConfig.db.connection.db.dropDatabase();
});
