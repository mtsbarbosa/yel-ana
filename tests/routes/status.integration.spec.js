const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
let basic_fixtures = require('../fixtures/basic_fixtures');
const authentication_helper = require('../helpers/authentication');
const chaiHttp = require('chai-http');
let server = require('../../app');
let factoryGirlConfig = require('../config/factory-girl-config');

chai.use(chaiHttp);

let auth;
let new_status;
let no_code_status;
let no_name_status;
let no_entity_status;
let no_description_status;
let wrong_id_status;
let not_present_status;

before(async() => {
  auth = await authentication_helper.authenticate(basic_fixtures);
});

describe('Status', () => {
  describe('GET#status/:status_id', () => {
    it('Should return status with specified id', async() => {
      try{
        let status =
            await chai.request(server)
                      .get('/yel-ana/status/'+basic_fixtures.status_active.object._id)
                      .set('x-access-token', auth.body.token);

        expect(status).to.have.status(200);
        assert.equal(basic_fixtures.status_active.object.name, status.body.message.name);

      }catch(err){
        console.log('Error to get status => ', err);
        assert(false);
      }

    });

    it('Should return 404 when status is not found', async() => {
      try{
        let status =
            await chai.request(server)
                      .get('/yel-ana/status/' + '123')
                      .set('x-access-token', auth.body.token);
        expect(status).to.have.status(404);
        expect(status.body.message).to.be.equal('Not found');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });
  });

  describe('GET#status/entity/:entity_id', () => {
    it('Should return all status with specified entity id', async() => {
      try{
        let status =
            await chai.request(server)
                      .get('/yel-ana/status/entity/'+basic_fixtures.entity.object._id)
                      .set('x-access-token', auth.body.token);

        expect(status).to.have.status(200);
        expect(status.body.message).to.not.be.empty;
        expect(status.body.message[0].name).to.be.equal(basic_fixtures.status_active.object.name);
        expect(status.body.message[1].name).to.be.equal(basic_fixtures.status_inactive.object.name);

      }catch(err){
        console.log('Error to get status => ', err);
        assert(false);
      }
    });
  });

  describe('New Status Services', () => {
    before(async() => {
      new_status = await factoryGirlConfig.factoryGirl.build('new_status',{entity: basic_fixtures.entity.object._id, application: basic_fixtures.application.object._id});
      no_code_status = JSON.parse(JSON.stringify(new_status));
      delete no_code_status.code;
      no_name_status = JSON.parse(JSON.stringify(new_status));
      delete no_name_status.name;
      no_entity_status = JSON.parse(JSON.stringify(new_status));
      delete no_entity_status.entity;
      no_description_status = JSON.parse(JSON.stringify(new_status));
      delete no_description_status.description;
      wrong_id_status = JSON.parse(JSON.stringify(new_status));
      wrong_id_status._id = 'aaa';
      not_present_status = JSON.parse(JSON.stringify(new_status));
      not_present_status._id = require('mongoose').Types.ObjectId();
    });

    it('POST#status => Should return 400 in case of trying to create without mandatory params', async() => {
      let status;
      try{
        status =
            await chai.request(server)
                      .post('/yel-ana/status')
                      .set('x-access-token', auth.body.token)
                      .send(no_code_status);
         expect(status).to.have.status(400);
         expect(status.body.message).to.be.equal('code is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }

      try{
        status =
            await chai.request(server)
                      .post('/yel-ana/status')
                      .set('x-access-token', auth.body.token)
                      .send(no_name_status);
        expect(status).to.have.status(400);
        expect(status.body.message).to.be.equal('name is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }

      try{
        status =
            await chai.request(server)
                      .post('/yel-ana/status')
                      .set('x-access-token', auth.body.token)
                      .send(no_entity_status);
        expect(status).to.have.status(400);
        expect(status.body.message).to.be.equal('entity is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('POST#status => Should successfully insert status into the entity', async() => {
        let status;
        try{
          status =
              await chai.request(server)
                        .post('/yel-ana/status')
                        .set('x-access-token', auth.body.token)
                        .send(new_status);

        }catch(err){
          console.log('Error to post status => ', err);
        }
        expect(status).to.have.status(204);

        try{
          status =
              await chai.request(server)
                      .get('/yel-ana/status/entity/'+basic_fixtures.entity.object._id)
                      .set('x-access-token', auth.body.token);

        }catch(err){
          console.log('Error to get status => ', err);
        }

        expect(status).to.have.status(200);
        expect(status.body.message[2].name).to.be.equal(new_status.name);
        new_status._id = status.body.message[2]._id;
    });

    it('PUT#status => Should return 400 in case of trying to update without mandatory params', async() => {
      let status;
      let no_id_status = JSON.parse(JSON.stringify(new_status));
      delete no_id_status._id;

      try{
        status =
            await chai.request(server)
                      .put('/yel-ana/status')
                      .set('x-access-token', auth.body.token)
                      .send(no_id_status);
        expect(status).to.have.status(400);
        expect(status.body.message).to.be.equal('_id is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }

      try{
        status =
            await chai.request(server)
                      .put('/yel-ana/status')
                      .set('x-access-token', auth.body.token)
                      .send(no_code_status);
        expect(status).to.have.status(400);
        expect(status.body.message).to.be.equal('code is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }

      try{
        status =
            await chai.request(server)
                      .put('/yel-ana/status')
                      .set('x-access-token', auth.body.token)
                      .send(no_name_status);
        expect(status).to.have.status(400);
        expect(status.body.message).to.be.equal('name is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('PUT#status => Should return 400 in case of trying to update with id wrongly formatted', async() => {
      let status;
      try{
        status =
            await chai.request(server)
                      .put('/yel-ana/status')
                      .set('x-access-token', auth.body.token)
                      .send(wrong_id_status);
        expect(status).to.have.status(400);
        expect(status.body.message).to.be.equal('_id provided is invalid');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('PUT#status => Should return 404 in case of trying to update with id not present yet', async() => {
      let status;
      try{
        status =
            await chai.request(server)
                      .put('/yel-ana/status')
                      .set('x-access-token', auth.body.token)
                      .send(not_present_status);
        expect(status).to.have.status(404);
        expect(status.body.message).to.be.equal('Not found');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('PUT#status => Should successfully update status', async() => {
        let status;
        let new_name = "abcdef";
        let new_code = "aaa";
        let new_description = "aaa aaa";
        new_status.name = new_name;
        new_status.code = new_code;
        new_status.description = new_description;
        new_status.application = basic_fixtures.application.object._id;

        try{
          status =
              await chai.request(server)
                        .put('/yel-ana/status')
                        .set('x-access-token', auth.body.token)
                        .send(new_status);

        }catch(err){
          console.log('Error to put status => ', err);
        }
        expect(status).to.have.status(204);

        try{
          status =
              await chai.request(server)
                      .get('/yel-ana/status/entity/'+basic_fixtures.entity.object._id)
                      .set('x-access-token', auth.body.token);

        }catch(err){
          console.log('Error to get status => ', err);
        }

        expect(status).to.have.status(200);
        expect(status.body.message[2].name).to.be.equal(new_name);
        expect(status.body.message[2].code).to.be.equal(new_code);
        expect(status.body.message[2].description).to.be.equal(new_description);
    });

    it('DELETE#status => Should return 404 when status is not found', async() => {
      try{
        let status =
            await chai.request(server)
                      .delete('/yel-ana/status/' + require('mongoose').Types.ObjectId())
                      .set('x-access-token', auth.body.token);

        expect(status).to.have.status(404);
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('DELETE#status => Should delete status successfully', async() => {
      let status;
      try{
        status =
            await chai.request(server)
                      .delete('/yel-ana/status/' + new_status._id)
                      .set('x-access-token', auth.body.token);
        expect(status).to.have.status(204);
      }catch(err){
        assert(false);
      }

      try{
        status =
            await chai.request(server)
                      .get('/yel-ana/status/' + new_status._id)
                      .set('x-access-token', auth.body.token);
        expect(status).to.have.status(404);
        expect(status.body.message).to.be.equal('Not found');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });
  });
});
