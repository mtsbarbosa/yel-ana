const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const basic_fixtures = require('../fixtures/basic_fixtures');
const authentication_helper = require('../helpers/authentication');
const chaiHttp = require('chai-http');
const server = require('../../app');

chai.use(chaiHttp);

let users;
let auth;
let status;

describe('Base', () => {
    describe('#authenticate()', () => {
        it('should return token', async () => {
            auth = await authentication_helper.authenticate(basic_fixtures);
            expect(auth).to.have.status(200);
            assert.isNotEmpty(auth.body.token);
        });
    });

    describe('#authenticate_app_by_user_token()', () => {
        it('should return 403 when trying to access other user\'s app', async () => {
            auth = await authentication_helper.authenticate_app_by_user_token(basic_fixtures,basic_fixtures.other_user_application.object._id);
            expect(auth).to.have.status(403);
        });

        it('should return token', async () => {
            auth = await authentication_helper.authenticate_app_by_user_token(basic_fixtures,basic_fixtures.application.object._id);
            expect(auth).to.have.status(200);
            assert.isNotEmpty(auth.body.token);
        });
    });

    describe('#authenticate_user()', () => {
        it('should return token', async () => {
            auth = await authentication_helper.authenticate_user(basic_fixtures);
            expect(auth).to.have.status(200);
            assert.isNotEmpty(auth.body.token);
        });
    });

    describe('Test app authentication permissions', () => {
        it('should have entity auto attached to the request', async () => {
            auth = await authentication_helper.authenticate(basic_fixtures);
            try {
                users =
                    await chai.request(server)
                        .get('/yel-ana/test')
                        .set('x-access-token', auth.body.token);

            } catch (err) {
                console.log('Error to get test => ', err);
            }
            assert.equal(basic_fixtures.application.object.name, users.body.message.name);
        });

        it('should bring status related to header application in case of being root', async () => {
            auth = await authentication_helper.authenticate(basic_fixtures);
            try {
                status =
                    await chai.request(server)
                        .get('/yel-ana/status/' + basic_fixtures.not_root_status.object._id)
                        .set('x-access-token', auth.body.token)
                        .set('application_id', basic_fixtures.not_root_application.object._id);

            } catch (err) {
                console.log('Error to get status not root => ', err);
            }
            expect(status).to.have.status(200);
            assert.equal(basic_fixtures.not_root_status.object.name, status.body.message.name);
        });

        it('should not bring status related to header application in case of not being root', async () => {
            auth = await authentication_helper.authenticate_not_root(basic_fixtures);
            try {
                status =
                    await chai.request(server)
                        .get('/yel-ana/status/' + basic_fixtures.status_active.object._id)
                        .set('x-access-token', auth.body.token)
                        .set('application_id', basic_fixtures.application.object._id);
                expect(status).to.have.status(404);
                expect(status.body.message).to.be.equal('Not found');
            } catch (err) {
                assert(false, "Error occurred");
            }
        });
    });

    describe('Test user authentication permissions', () => {

    });
});
