const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const chaiHttp = require('chai-http');
const server = require('../../app');
let factoryGirlConfig = require('../config/factory-girl-config');
const authentication_helper = require('../helpers/authentication');
let basic_fixtures = require('../fixtures/basic_fixtures');

chai.use(chaiHttp);

let auth;
let new_condition;
let no_name_condition;
let no_entity_condition;
let no_description_condition;
let wrong_id_condition;
let not_present_condition;

before(async() => {
    auth = await authentication_helper.authenticate(basic_fixtures);
});

describe('Condition', () => {
    describe('GET#condition/:condition_id', () => {
        it('Should return condition with specified id', async() => {
            try{
                let condition =
                    await chai.request(server)
                        .get('/yel-ana/condition/' + basic_fixtures.condition.object._id)
                        .set('x-access-token', auth.body.token);

                expect(condition).to.have.status(200);
                assert.equal(basic_fixtures.condition.object.name, condition.body.message.name);

            }catch(err){
                console.log('Error to get condition => ', err);
                assert(false);
            }

        });

        it('Should return 404 when condition is not found', async() => {
            try{
                let condition =
                    await chai.request(server)
                        .get('/yel-ana/condition/' + '123')
                        .set('x-access-token', auth.body.token);
                expect(condition).to.have.status(404);
                expect(condition.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });
    });

    describe('GET#condition/entity/:entity_id', () => {
        it('Should return all condition with specified entity id', async() => {
            try{
                let condition =
                    await chai.request(server)
                        .get('/yel-ana/condition/entity/'+basic_fixtures.entity.object._id)
                        .set('x-access-token', auth.body.token);

                expect(condition).to.have.status(200);
                expect(condition.body.message).to.not.be.empty;
                expect(condition.body.message[0].name).to.be.equal(basic_fixtures.condition.object.name);

            }catch(err){
                console.log('Error to get condition => ', err);
                assert(false);
            }
        });
    });

    describe('New Condition Services', () => {
        before(async() => {
            new_condition = await factoryGirlConfig.factoryGirl.build('new_condition',{entity: basic_fixtures.entity.object._id, application: basic_fixtures.application.object._id});
            no_name_condition = JSON.parse(JSON.stringify(new_condition));
            delete no_name_condition.name;
            no_entity_condition = JSON.parse(JSON.stringify(new_condition));
            delete no_entity_condition.entity;
            no_description_condition = JSON.parse(JSON.stringify(new_condition));
            delete no_description_condition.description;
            wrong_id_condition = JSON.parse(JSON.stringify(new_condition));
            wrong_id_condition._id = 'aaa';
            not_present_condition = JSON.parse(JSON.stringify(new_condition));
            not_present_condition._id = require('mongoose').Types.ObjectId();
        });

        it('POST#condition => Should return 400 in case of trying to create without mandatory params', async() => {
            let condition;
            try{
                condition =
                    await chai.request(server)
                        .post('/yel-ana/condition')
                        .set('x-access-token', auth.body.token)
                        .send(no_name_condition);
                expect(condition).to.have.status(400);
                expect(condition.body.message).to.be.equal('name is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                condition =
                    await chai.request(server)
                        .post('/yel-ana/condition')
                        .set('x-access-token', auth.body.token)
                        .send(no_entity_condition);
                expect(condition).to.have.status(400);
                expect(condition.body.message).to.be.equal('entity is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('POST#condition => Should successfully insert condition into the entity', async() => {
            let condition;
            try{
                condition =
                    await chai.request(server)
                        .post('/yel-ana/condition')
                        .set('x-access-token', auth.body.token)
                        .send(new_condition);

            }catch(err){
                console.log('Error to post condition => ', err);
            }
            expect(condition).to.have.status(200);
            expect(condition.body.message.name).to.be.equal(new_condition.name);

            try{
                condition =
                    await chai.request(server)
                        .get('/yel-ana/condition/entity/'+basic_fixtures.entity.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition => ', err);
            }
            expect(condition).to.have.status(200);
            expect(condition.body.message[condition.body.message.length - 1].name).to.be.equal(new_condition.name);
            new_condition._id = condition.body.message[1]._id;
        });

        it('PUT#condition => Should return 400 in case of trying to update without mandatory params', async() => {
            let condition;
            let no_id_condition = JSON.parse(JSON.stringify(new_condition));
            delete no_id_condition._id;

            try{
                condition =
                    await chai.request(server)
                        .put('/yel-ana/condition')
                        .set('x-access-token', auth.body.token)
                        .send(no_id_condition);
                expect(condition).to.have.status(400);
                expect(condition.body.message).to.be.equal('_id is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                condition =
                    await chai.request(server)
                        .put('/yel-ana/condition')
                        .set('x-access-token', auth.body.token)
                        .send(no_name_condition);
                expect(condition).to.have.status(400);
                expect(condition.body.message).to.be.equal('name is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('PUT#condition => Should return 400 in case of trying to update with id wrongly formatted', async() => {
            let condition;
            try{
                condition =
                    await chai.request(server)
                        .put('/yel-ana/condition')
                        .set('x-access-token', auth.body.token)
                        .send(wrong_id_condition);
                expect(condition).to.have.status(400);
                expect(condition.body.message).to.be.equal('_id provided is invalid');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('PUT#condition => Should return 404 in case of trying to update with id not present yet', async() => {
            let condition;
            try{
                condition =
                    await chai.request(server)
                        .put('/yel-ana/condition')
                        .set('x-access-token', auth.body.token)
                        .send(not_present_condition);
                expect(condition).to.have.status(404);
                expect(condition.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('PUT#condition => Should successfully update condition', async() => {
            let condition;
            let new_name = "abcdef";
            let new_description = "aaa aaa";
            new_condition.name = new_name;
            new_condition.description = new_description;
            new_condition.application = basic_fixtures.application.object._id;

            try{
                condition =
                    await chai.request(server)
                        .put('/yel-ana/condition')
                        .set('x-access-token', auth.body.token)
                        .send(new_condition);

            }catch(err){
                console.log('Error to put condition => ', err);
            }
            expect(condition).to.have.status(200);
            expect(condition.body.message.name).to.be.equal(new_condition.name);

            try{
                condition =
                    await chai.request(server)
                        .get('/yel-ana/condition/entity/'+basic_fixtures.entity.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition => ', err);
            }

            expect(condition).to.have.status(200);
            expect(condition.body.message[1].name).to.be.equal(new_name);
            expect(condition.body.message[1].description).to.be.equal(new_description);
        });

        it('DELETE#condition => Should return 404 when condition is not found', async() => {
            try{
                let condition =
                    await chai.request(server)
                        .delete('/yel-ana/condition/' + require('mongoose').Types.ObjectId())
                        .set('x-access-token', auth.body.token);

                expect(condition).to.have.status(404);
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('DELETE#condition => Should delete condition successfully', async() => {
            let condition;
            try{
                condition =
                    await chai.request(server)
                        .delete('/yel-ana/condition/' + new_condition._id)
                        .set('x-access-token', auth.body.token);
                expect(condition).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                condition =
                    await chai.request(server)
                        .get('/yel-ana/condition/' + new_condition._id)
                        .set('x-access-token', auth.body.token);
                expect(condition).to.have.status(404);
                expect(condition.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });
    });
});