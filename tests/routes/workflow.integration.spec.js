const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
let basic_fixtures = require('../fixtures/basic_fixtures');
const authentication_helper = require('../helpers/authentication');
const chaiHttp = require('chai-http');
let server = require('../../app');
let factoryGirlConfig = require('../config/factory-girl-config');

chai.use(chaiHttp);

let auth;
let new_workflow;
let no_code_workflow;
let no_name_workflow;
let no_entity_workflow;
let no_description_workflow;
let wrong_id_workflow;
let not_present_workflow;

before(async() => {
  auth = await authentication_helper.authenticate(basic_fixtures);
});

describe('Workflow', () => {
  describe('GET#workflow/:workflow_id', () => {
    it('Should return workflow with specified id', async() => {
      try{
        let workflow =
            await chai.request(server)
                      .get('/yel-ana/workflow/'+basic_fixtures.workflow.object._id)
                      .set('x-access-token', auth.body.token);

        expect(workflow).to.have.status(200);
        assert.equal(basic_fixtures.workflow.object.name, workflow.body.message.name);

      }catch(err){
        console.log('Error to get workflow => ', err);
        assert(false);
      }

    });

    it('Should return 404 when workflow is not found', async() => {
      try{
        let workflow =
            await chai.request(server)
                      .get('/yel-ana/workflow/' + '123')
                      .set('x-access-token', auth.body.token);

        expect(workflow).to.have.status(404);
        expect(workflow.body.message).to.be.equal('Not found');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });
  });

  describe('GET#workflow/entity/:entity_id', () => {
    it('Should return all workflow with specified entity id', async() => {
      try{
        let workflow =
            await chai.request(server)
                      .get('/yel-ana/workflow/entity/'+basic_fixtures.entity.object._id)
                      .set('x-access-token', auth.body.token);

        expect(workflow).to.have.status(200);
        expect(workflow.body.message).to.not.be.empty;
        expect(workflow.body.message[0].name).to.be.equal(basic_fixtures.workflow.object.name);

      }catch(err){
        console.log('Error to get workflow => ', err);
        assert(false);
      }
    });
  });

  describe('New Workflow Services', () => {
    before(async() => {
      new_workflow = await factoryGirlConfig.factoryGirl.build('new_workflow',{entity: basic_fixtures.entity.object._id, application: basic_fixtures.application.object._id});
      no_code_workflow = JSON.parse(JSON.stringify(new_workflow));
      delete no_code_workflow.code;
      no_name_workflow = JSON.parse(JSON.stringify(new_workflow));
      delete no_name_workflow.name;
      no_entity_workflow = JSON.parse(JSON.stringify(new_workflow));
      delete no_entity_workflow.entity;
      no_description_workflow = JSON.parse(JSON.stringify(new_workflow));
      delete no_description_workflow.description;
      wrong_id_workflow = JSON.parse(JSON.stringify(new_workflow));
      wrong_id_workflow._id = 'aaa';
      not_present_workflow = JSON.parse(JSON.stringify(new_workflow));
      not_present_workflow._id = require('mongoose').Types.ObjectId();
    });

    it('POST#workflow => Should return 400 in case of trying to create without mandatory params', async() => {
      let workflow;
      try{
        workflow =
            await chai.request(server)
                      .post('/yel-ana/workflow')
                      .set('x-access-token', auth.body.token)
                      .send(no_code_workflow);
        expect(workflow).to.have.status(400);
        expect(workflow.body.message).to.be.equal('code is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }

      try{
        workflow =
            await chai.request(server)
                      .post('/yel-ana/workflow')
                      .set('x-access-token', auth.body.token)
                      .send(no_name_workflow);
        expect(workflow).to.have.status(400);
        expect(workflow.body.message).to.be.equal('name is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }

      try{
        workflow =
            await chai.request(server)
                      .post('/yel-ana/workflow')
                      .set('x-access-token', auth.body.token)
                      .send(no_entity_workflow);
        expect(workflow).to.have.status(400);
        expect(workflow.body.message).to.be.equal('entity is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('POST#workflow => Should successfully insert workflow into the entity', async() => {
        let workflow;
        try{
          workflow =
              await chai.request(server)
                        .post('/yel-ana/workflow')
                        .set('x-access-token', auth.body.token)
                        .send(new_workflow);

        }catch(err){
          console.log('Error to post workflow => ', err);
        }
        expect(workflow).to.have.status(204);

        try{
          workflow =
              await chai.request(server)
                      .get('/yel-ana/workflow/entity/'+basic_fixtures.entity.object._id)
                      .set('x-access-token', auth.body.token);

        }catch(err){
          console.log('Error to get workflow => ', err);
        }

        expect(workflow).to.have.status(200);
        expect(workflow.body.message[1].name).to.be.equal(new_workflow.name);
        new_workflow._id = workflow.body.message[1]._id;
    });

    it('PUT#workflow => Should return 400 in case of trying to update without mandatory params', async() => {
      let workflow;
      let no_id_workflow = JSON.parse(JSON.stringify(new_workflow));
      delete no_id_workflow._id;

      try{
        workflow =
            await chai.request(server)
                      .put('/yel-ana/workflow')
                      .set('x-access-token', auth.body.token)
                      .send(no_id_workflow);
        expect(workflow).to.have.status(400);
        expect(workflow.body.message).to.be.equal('_id is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }

      try{
        workflow =
            await chai.request(server)
                      .put('/yel-ana/workflow')
                      .set('x-access-token', auth.body.token)
                      .send(no_code_workflow);
        expect(workflow).to.have.status(400);
        expect(workflow.body.message).to.be.equal('code is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }

      try{
        workflow =
            await chai.request(server)
                      .put('/yel-ana/workflow')
                      .set('x-access-token', auth.body.token)
                      .send(no_name_workflow);
        expect(workflow).to.have.status(400);
        expect(workflow.body.message).to.be.equal('name is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('PUT#workflow => Should return 400 in case of trying to update with id wrongly formatted', async() => {
      let workflow;
      try{
        workflow =
            await chai.request(server)
                      .put('/yel-ana/workflow')
                      .set('x-access-token', auth.body.token)
                      .send(wrong_id_workflow);
        expect(workflow).to.have.status(400);
        expect(workflow.body.message).to.be.equal('_id provided is invalid');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('PUT#workflow => Should return 404 in case of trying to update with id not present yet', async() => {
      let workflow;
      try{
        workflow =
            await chai.request(server)
                      .put('/yel-ana/workflow')
                      .set('x-access-token', auth.body.token)
                      .send(not_present_workflow);
        expect(workflow).to.have.status(404);
        expect(workflow.body.message).to.be.equal('Not found');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('PUT#workflow => Should successfully update workflow', async() => {
        let workflow;
        let new_name = "abcdef";
        let new_code = "aaa";
        let new_description = "aaa aaa";
        new_workflow.name = new_name;
        new_workflow.code = new_code;
        new_workflow.description = new_description;
        new_workflow.application = basic_fixtures.application.object._id;

        try{
          workflow =
              await chai.request(server)
                        .put('/yel-ana/workflow')
                        .set('x-access-token', auth.body.token)
                        .send(new_workflow);

        }catch(err){
          console.log('Error to put workflow => ', err);
        }
        expect(workflow).to.have.status(204);

        try{
          workflow =
              await chai.request(server)
                      .get('/yel-ana/workflow/entity/'+basic_fixtures.entity.object._id)
                      .set('x-access-token', auth.body.token);

        }catch(err){
          console.log('Error to get workflow => ', err);
        }

        expect(workflow).to.have.status(200);
        expect(workflow.body.message[1].name).to.be.equal(new_name);
        expect(workflow.body.message[1].code).to.be.equal(new_code);
        expect(workflow.body.message[1].description).to.be.equal(new_description);
    });

    it('DELETE#workflow => Should return 404 when workflow is not found', async() => {
      try{
        let workflow =
            await chai.request(server)
                      .delete('/yel-ana/workflow/' + require('mongoose').Types.ObjectId())
                      .set('x-access-token', auth.body.token);

        expect(workflow).to.have.status(404);
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('DELETE#workflow => Should delete workflow successfully', async() => {
      let workflow;
      try{
        workflow =
            await chai.request(server)
                      .delete('/yel-ana/workflow/' + new_workflow._id)
                      .set('x-access-token', auth.body.token);
        expect(workflow).to.have.status(204);
      }catch(err){
        assert(false);
      }

      try{
        workflow =
            await chai.request(server)
                      .get('/yel-ana/workflow/' + new_workflow._id)
                      .set('x-access-token', auth.body.token);
        expect(workflow).to.have.status(404);
        expect(workflow.body.message).to.be.equal('Not found');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });
  });
});
