const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const chaiHttp = require('chai-http');
let server = require('../../app');
let factoryGirlConfig = require('../config/factory-girl-config');
let basic_fixtures = require('../fixtures/basic_fixtures');

chai.use(chaiHttp);

let new_user;
let no_name_user;
let no_email_user;
let no_password_user;
let user_id;
let user_token;

before(async() => {
});

describe('User', () => {
    describe('New User Services', () => {
        before(async() => {
            new_user = await factoryGirlConfig.factoryGirl.build('new_user');
            no_email_user = JSON.parse(JSON.stringify(new_user));
            delete no_email_user.email;
            no_name_user = JSON.parse(JSON.stringify(new_user));
            delete no_name_user.name;
            no_password_user = JSON.parse(JSON.stringify(new_user));
            delete no_password_user.password;
        });

        it('POST#user => Should return 400 in case of trying to create without mandatory params', async() => {
            let resp;
            try{
                resp =
                    await chai.request(server)
                        .post('/yel-ana/user')
                        .set('x-access-token', 'secret')
                        .send(no_name_user);
                expect(resp).to.have.status(400);
                expect(resp.body.message).to.be.equal('name is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                resp =
                    await chai.request(server)
                        .post('/yel-ana/user')
                        .set('x-access-token', 'secret')
                        .send(no_email_user);
                expect(resp).to.have.status(400);
                expect(resp.body.message).to.be.equal('email is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                resp =
                    await chai.request(server)
                        .post('/yel-ana/user')
                        .set('x-access-token', 'secret')
                        .send(no_password_user);
                expect(resp).to.have.status(400);
                expect(resp.body.message).to.be.equal('password is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('POST#user => Should successfully insert user', async() => {
            let user;
            try{
                user =
                    await chai.request(server)
                        .post('/yel-ana/user')
                        .set('x-access-token', 'secret')
                        .send(new_user);
            }catch(err){
                console.log('Error to post user => ', err);
            }
            expect(user).to.have.status(200);
            expect(user.body.message.name).to.be.equal(new_user.name);
            expect(user.body.message.email).to.be.equal(new_user.email);
            expect(user.body.message.blocked).to.be.true;
            expect(user.body.message).to.not.have.property('password');
            expect(user.body.message).to.not.have.property('email_token');
            expect(user.body.message).to.have.property('_id');
            user_id = user.body.message._id;
        });

        it('POST#user => New User should not be able to authenticate', async() => {
            let auth;
            try{
                 auth =
                    await chai.request(server)
                        .post('/yel-ana/user/authenticate')
                        .set('content-type', 'application/x-www-form-urlencoded')
                        .send({email: new_user.email, password: new_user.password});
                expect(auth).to.have.status(403);
                expect(auth.body.message).to.be.equal('User is blocked');
            }catch(err){
                console.log('Error to auth user => ', err);
                assert(false);
            }
        });

        it('POST#user => Wrong token should not activate user', async() => {
            let resp;
            try{
                resp =
                    await chai.request(server)
                        .get('/yel-ana/user/activate/dfe4f52d2fd1dcccc0dkd')
                        .set('x-access-token', 'secret');
                expect(resp).to.have.status(404);
                expect(resp.body.message).to.be.equal('Not found');
            }catch(err){
                console.log('Error to activate user => ', err);
                assert(false);
            }
        });

        it('POST#user => Token should activate user', async() => {
            let resp;
            try{
                resp =
                    await chai.request(server)
                        .get('/yel-ana/test/get_token/' + user_id)
                        .set('x-access-token', 'secret');
                expect(resp).to.have.status(200);
                expect(resp.body.message).to.not.be.empty;
                resp =
                    await chai.request(server)
                        .get('/yel-ana/user/activate/' + resp.body.message)
                        .set('x-access-token', 'secret');
                expect(resp).to.redirectTo('http://localhost:3000/yel-ana/test/activate');
                expect(resp).to.have.status(200);
                let auth =
                    await chai.request(server)
                        .post('/yel-ana/user/authenticate')
                        .set('content-type', 'application/x-www-form-urlencoded')
                        .send({email: new_user.email, password: new_user.password});
                expect(auth).to.have.status(200);
                assert.isNotEmpty(auth.body.token);
            }catch(err){
                console.log('Error to activate user => ', err);
                assert(false);
            }
        });

        it('POST#user/reset_password => should return 400 if email is not send', async() => {
            let user;
            try{
                user =
                    await chai.request(server)
                        .post('/yel-ana/user/reset_password')
                        .set('x-access-token', 'secret')
                        .send({});
            }catch(err){
                console.log('Error to reset user password => ', err);
            }
            expect(user).to.have.status(400);
            expect(user.body.message).to.be.equal('email is mandatory');
        });

        it('POST#user/reset_password => should return 400 if email is blank', async() => {
            let user;
            try{
                user =
                    await chai.request(server)
                        .post('/yel-ana/user/reset_password')
                        .set('x-access-token', 'secret')
                        .send({ email: ""});
            }catch(err){
                console.log('Error to reset user password => ', err);
            }
            expect(user).to.have.status(400);
            expect(user.body.message).to.be.equal('email is mandatory');
        });

        it('POST#user/reset_password => should return 200 always when resetting password', async() => {
            let user;
            try{
                user =
                    await chai.request(server)
                        .post('/yel-ana/user/reset_password')
                        .set('x-access-token', 'secret')
                        .send({email: 'any_random@test.com'});

                expect(user).to.have.status(200);
                expect(user.body.message).to.be.equal('Reset email was sent to you');
                user =
                    await chai.request(server)
                        .post('/yel-ana/user/reset_password')
                        .set('x-access-token', 'secret')
                        .send({email: new_user.email});

                expect(user).to.have.status(200);
                expect(user.body.message).to.be.equal('Reset email was sent to you');
            }catch(err){
                console.log('Error to reset user password => ', err);
            }
        });

        it('POST#user/reset_password => reset user should generate a token', async() => {
            let resp =
                await chai.request(server)
                    .get('/yel-ana/test/get_token/' + user_id)
                    .set('x-access-token', 'secret');
            expect(resp).to.have.status(200);
            expect(resp.body.message).to.not.be.empty;
            user_token = resp.body.message;
        });

        it('POST#redirect-reset/:token => Wrong token should not redirect', async() => {
            let resp;
            try{
                resp =
                    await chai.request(server)
                        .get('/yel-ana/user/redirect-reset/dfe4f52d2fd1dcccc0dkd')
                        .set('x-access-token', 'secret');
                expect(resp).to.have.status(404);
                expect(resp.body.message).to.be.equal('Not found');
            }catch(err){
                console.log('Error to activate user => ', err);
                assert(false);
            }
        });

        it('POST#redirect-reset/:token => Reset password should still not block user and token should redirect user', async() => {
            let resp;
            try{
                resp =
                    await chai.request(server)
                        .get('/yel-ana/test/get_token/' + user_id)
                        .set('x-access-token', 'secret');
                expect(resp).to.have.status(200);
                expect(resp.body.message).to.not.be.empty;
                resp =
                    await chai.request(server)
                        .get('/yel-ana/user/redirect-reset/' + resp.body.message)
                        .set('x-access-token', 'secret');
                expect(resp).to.redirectTo('http://localhost:3000/yel-ana/test/change-password/' + new_user.email + "/" + user_token);
                expect(resp).to.have.status(200);
                let auth =
                    await chai.request(server)
                        .post('/yel-ana/user/authenticate')
                        .set('content-type', 'application/x-www-form-urlencoded')
                        .send({email: new_user.email, password: new_user.password});
                expect(auth).to.have.status(200);
                assert.isNotEmpty(auth.body.token);
            }catch(err){
                console.log('Error to activate user => ', err);
                assert(false);
            }
        });

        it('PATCH#user/password => passing a wrong token should return 400', async() => {
            let resp =
                await chai.request(server)
                    .patch('/yel-ana/user/password')
                    .set('x-access-token', 'secret')
                    .send({ token: 'fdkosakfdoskfosakfda', password_new: 'secret2', password_new_confirmation: 'secret2' });
            expect(resp).to.have.status(400);
            expect(resp.body.message).to.be.equal('Failed to authenticate token.');
        });

        it('PATCH#user/password => should return 400 in case of not sending mandatory params', async() => {
            let resp =
                await chai.request(server)
                    .patch('/yel-ana/user/password')
                    .set('x-access-token', 'secret')
                    .send({});
            expect(resp).to.have.status(400);
            expect(resp.body.message).to.be.equal('token,password_new,password_new_confirmation is mandatory');
        });

        it('PATCH#user/password => should return 400 in case of not matching password', async() => {
            let resp =
                await chai.request(server)
                    .patch('/yel-ana/user/password')
                    .set('x-access-token', 'secret')
                    .send({ token: user_token, password_new: 'secret2', password_new_confirmation: 'secret3' });
            expect(resp).to.have.status(400);
            expect(resp.body.message).to.be.equal('password_new does not match password_new_confirmation');
        });

        it('PATCH#user/password => should return 204 in case of updating password', async() => {
            let resp =
                await chai.request(server)
                    .patch('/yel-ana/user/password')
                    .set('x-access-token', 'secret')
                    .send({ token: user_token, password_new: 'secret2', password_new_confirmation: 'secret2' });
            expect(resp).to.have.status(204);
        });

        it('PATCH#user/password => should allow user to authenticate with new password', async() => {
            let auth =
                await chai.request(server)
                    .post('/yel-ana/user/authenticate')
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .send({email: new_user.email, password: 'secret2'});
            expect(auth).to.have.status(200);
            assert.isNotEmpty(auth.body.token);
        });

        it('PATCH#user/password => should have no more token', async() => {
            let resp =
                await chai.request(server)
                    .get('/yel-ana/test/get_token/' + user_id)
                    .set('x-access-token', 'secret');
            expect(resp).to.have.status(200);
            expect(resp.body).to.not.have.property('message');
        });

        after(async() => {
            try{
                /********
                 *
                 *
                 * SETTING USER PASSWORD BACK
                 *
                 *
                 ********/
                let user =
                    await chai.request(server)
                        .post('/yel-ana/user/reset_password')
                        .set('x-access-token', 'secret')
                        .send({email: new_user.email});

                expect(user).to.have.status(200);
                expect(user.body.message).to.be.equal('Reset email was sent to you');

                user =
                    await chai.request(server)
                        .get('/yel-ana/test/get_token/' + user_id)
                        .set('x-access-token', 'secret');
                expect(user).to.have.status(200);
                expect(user.body.message).to.not.be.empty;
                user_token = user.body.message;

                user =
                    await chai.request(server)
                        .patch('/yel-ana/user/password')
                        .set('x-access-token', 'secret')
                        .send({ token: user_token, password_new: new_user.password, password_new_confirmation: new_user.password });
                expect(user).to.have.status(204);

                let auth =
                    await chai.request(server)
                        .post('/yel-ana/user/authenticate')
                        .set('content-type', 'application/x-www-form-urlencoded')
                        .send({email: new_user.email, password: new_user.password});
                expect(auth).to.have.status(200);
                assert.isNotEmpty(auth.body.token);
            }catch(err){
                console.log('Error to reset hash_key to application => ', err);
                assert(false);
            }
        });
    });
});
