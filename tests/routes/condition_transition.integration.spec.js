const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const chaiHttp = require('chai-http');
const server = require('../../app');
let factoryGirlConfig = require('../config/factory-girl-config');
const authentication_helper = require('../helpers/authentication');
let basic_fixtures = require('../fixtures/basic_fixtures');

chai.use(chaiHttp);

let auth;
let new_condition_transition;
let no_condition_condition_transition;
let no_transition_condition_transition;
let wrong_condition_condition_transition;
let wrong_transition_condition_transition;
let not_present_condition_transition;

before(async() => {
    auth = await authentication_helper.authenticate(basic_fixtures);
});

describe('Condition Transition', () => {
    describe('GET#condition_transition/transition/:transition_id', () => {
        it('Should return condition_transition with specified transition id', async() => {
            try{
                let condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/' + basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

                expect(condition_transition).to.have.status(200);
                assert.equal(basic_fixtures.condition_transition.object.condition, condition_transition.body.message[0].condition._id);
                assert.equal(basic_fixtures.condition_linked_1.object.name, condition_transition.body.message[0].condition.name);
                assert.equal(basic_fixtures.condition_transition_2.object.condition, condition_transition.body.message[1].condition._id);
                assert.equal(basic_fixtures.condition_linked_2.object.name, condition_transition.body.message[1].condition.name);
                assert.equal(basic_fixtures.condition_transition_3.object.condition, condition_transition.body.message[2].condition._id);
                assert.equal(basic_fixtures.condition_linked_3.object.name, condition_transition.body.message[2].condition.name);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
                assert(false);
            }

        });

        it('Should return 404 when condition_transition is not found', async() => {
            try{
                let condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/' + require('mongoose').Types.ObjectId())
                        .set('x-access-token', auth.body.token);
                expect(condition_transition).to.have.status(404);
                expect(condition_transition.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });
    });

    describe('New Condition Transition Services', () => {
        before(async () => {
            new_condition_transition = await factoryGirlConfig.factoryGirl.build('new_condition_transition', {
                condition: basic_fixtures.new_condition_linked.object._id,
                transition: basic_fixtures.transition_active_inactive.object._id,
                application: basic_fixtures.application.object._id
            });
            no_condition_condition_transition = JSON.parse(JSON.stringify(new_condition_transition));
            delete no_condition_condition_transition.condition;
            no_transition_condition_transition = JSON.parse(JSON.stringify(new_condition_transition));
            delete no_transition_condition_transition.transition;
            wrong_condition_condition_transition = JSON.parse(JSON.stringify(new_condition_transition));
            wrong_condition_condition_transition.condition = require('mongoose').Types.ObjectId();
            wrong_transition_condition_transition = JSON.parse(JSON.stringify(new_condition_transition));
            wrong_transition_condition_transition.transition = require('mongoose').Types.ObjectId();
            not_present_condition_transition = JSON.parse(JSON.stringify(new_condition_transition));
            not_present_condition_transition._id = require('mongoose').Types.ObjectId();
        });

        it('POST#condition_transition => Should return 400 in case of trying to create without mandatory params', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .post('/yel-ana/condition_transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_condition_condition_transition);
                expect(condition_transition).to.have.status(400);
                expect(condition_transition.body.message).to.be.equal('condition is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                condition_transition =
                    await chai.request(server)
                        .post('/yel-ana/condition_transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_transition_condition_transition);
                expect(condition_transition).to.have.status(400);
                expect(condition_transition.body.message).to.be.equal('transition is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('POST#condition_transition => Should successfully insert condition_transition into the entity', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .post('/yel-ana/condition_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_condition_transition);

            }catch(err){
                console.log('Error to post condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.name).to.be.equal(new_condition_transition.name);

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message[condition_transition.body.message.length - 1].name).to.be.equal(new_condition_transition.name);
        });

        it('DELETE#condition_transition => Should return 404 when condition_transition is not found', async() => {
            try{
                let condition_transition =
                    await chai.request(server)
                        .delete('/yel-ana/condition_transition/transition/' + new_condition_transition.transition + '/condition/' + require('mongoose').Types.ObjectId())
                        .set('x-access-token', auth.body.token);

                expect(condition_transition).to.have.status(404);
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                let condition_transition =
                    await chai.request(server)
                        .delete('/yel-ana/condition_transition/transition/' + require('mongoose').Types.ObjectId() + '/condition/' + new_condition_transition.condition)
                        .set('x-access-token', auth.body.token);

                expect(condition_transition).to.have.status(404);
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('DELETE#condition_transition => Should delete condition_transition successfully', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .delete('/yel-ana/condition_transition/transition/' + new_condition_transition.transition + '/condition/' + new_condition_transition.condition)
                        .set('x-access-token', auth.body.token);
                expect(condition_transition).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/' + new_condition_transition.transition)
                        .set('x-access-token', auth.body.token);
                expect(condition_transition).to.have.status(200);
                expect(condition_transition.body.message[condition_transition.body.message.length - 1].condition.name).to.not.be.equal(basic_fixtures.new_condition_linked.object.name);

                //check order sequence
                for(let i = 0; i < condition_transition.body.message.length; i++){
                    expect(i).to.be.equal(condition_transition.body.message[i].order);
                }
            }catch(err){
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('POST#condition_transition => Should successfully insert condition_transition into the first position', async() => {
            let condition_transition;
            new_condition_transition.order = 0;
            try{
                condition_transition =
                    await chai.request(server)
                        .post('/yel-ana/condition_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_condition_transition);

            }catch(err){
                console.log('Error to post condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.name).to.be.equal(new_condition_transition.name);

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message[0].name).to.be.equal(new_condition_transition.name);
            //check order sequence
            for(let i = 0; i < condition_transition.body.message.length; i++){
                expect(i).to.be.equal(condition_transition.body.message[i].order);
            }
        });

        it('DELETE#condition_transition => Should delete condition_transition successfully at first position', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .delete('/yel-ana/condition_transition/transition/' + new_condition_transition.transition + '/condition/' + new_condition_transition.condition)
                        .set('x-access-token', auth.body.token);
                expect(condition_transition).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/' + new_condition_transition.transition)
                        .set('x-access-token', auth.body.token);
                expect(condition_transition).to.have.status(200);
                expect(condition_transition.body.message[0].condition.name).to.not.be.equal(basic_fixtures.new_condition_linked.object.name);

                //check order sequence
                for(let i = 0; i < condition_transition.body.message.length; i++){
                    expect(i).to.be.equal(condition_transition.body.message[i].order);
                }
            }catch(err){
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('POST#condition_transition => Should successfully insert condition_transition into the second position', async() => {
            let condition_transition;
            new_condition_transition.order = 1;
            try{
                condition_transition =
                    await chai.request(server)
                        .post('/yel-ana/condition_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_condition_transition);

            }catch(err){
                console.log('Error to post condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.name).to.be.equal(new_condition_transition.name);

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message[1].name).to.be.equal(new_condition_transition.name);
            //check order sequence
            for(let i = 0; i < condition_transition.body.message.length; i++){
                expect(i).to.be.equal(condition_transition.body.message[i].order);
            }
        });

        it('DELETE#condition_transition => Should delete condition_transition successfully at second position', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .delete('/yel-ana/condition_transition/transition/' + new_condition_transition.transition + '/condition/' + new_condition_transition.condition)
                        .set('x-access-token', auth.body.token);
                expect(condition_transition).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/' + new_condition_transition.transition)
                        .set('x-access-token', auth.body.token);
                expect(condition_transition).to.have.status(200);
                expect(condition_transition.body.message[1].condition.name).to.not.be.equal(basic_fixtures.new_condition_linked.object.name);

                //check order sequence
                for(let i = 0; i < condition_transition.body.message.length; i++){
                    expect(i).to.be.equal(condition_transition.body.message[i].order);
                }
            }catch(err){
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('POST#condition_transition => Should successfully insert condition_transition into the end', async() => {
            let condition_transition;
            new_condition_transition.order = 3;
            try{
                condition_transition =
                    await chai.request(server)
                        .post('/yel-ana/condition_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_condition_transition);

            }catch(err){
                console.log('Error to post condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.name).to.be.equal(new_condition_transition.name);

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message[3].name).to.be.equal(new_condition_transition.name);
            //check order sequence
            for(let i = 0; i < condition_transition.body.message.length; i++){
                expect(i).to.be.equal(condition_transition.body.message[i].order);
            }
        });

        it('POST#condition_transition => Should return 400 in case of inserting the same condition', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .post('/yel-ana/condition_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_condition_transition);

            }catch(err){
                console.log('Error to post condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(400);

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.length).to.be.equal(4);
            expect(condition_transition.body.message[3].name).to.be.equal(new_condition_transition.name);
            //check order sequence
            for(let i = 0; i < condition_transition.body.message.length; i++){
                expect(i).to.be.equal(condition_transition.body.message[i].order);
            }
        });

        it('DELETE#condition_transition => Should delete condition_transition successfully from the end', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .delete('/yel-ana/condition_transition/transition/' + new_condition_transition.transition + '/condition/' + new_condition_transition.condition)
                        .set('x-access-token', auth.body.token);
                expect(condition_transition).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/' + new_condition_transition.transition)
                        .set('x-access-token', auth.body.token);
                expect(condition_transition).to.have.status(200);
                expect(condition_transition.body.message.length).to.not.be.equal(4);

                //check order sequence
                for(let i = 0; i < condition_transition.body.message.length; i++){
                    expect(condition_transition.body.message[i].condition.name).to.not.be.equal(new_condition_transition.name);
                    expect(i).to.be.equal(condition_transition.body.message[i].order);
                }
            }catch(err){
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('POST#condition_transition => Should successfully insert condition_transition into the last position', async() => {
            let condition_transition;
            new_condition_transition.order = 2;
            try{
                condition_transition =
                    await chai.request(server)
                        .post('/yel-ana/condition_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_condition_transition);

            }catch(err){
                console.log('Error to post condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.name).to.be.equal(new_condition_transition.name);

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message[2].name).to.be.equal(new_condition_transition.name);
            //check order sequence
            for(let i = 0; i < condition_transition.body.message.length; i++){
                expect(i).to.be.equal(condition_transition.body.message[i].order);
            }
        });

        it('PATCH#condition_transition/order => Should successfully change order to first', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .patch('/yel-ana/condition_transition/transition/' + basic_fixtures.transition_active_inactive.object._id + '/condition/' + new_condition_transition.condition + '/order/0')
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to patch condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.name).to.be.equal(new_condition_transition.name);
            expect(condition_transition.body.message.order).to.be.equal('0');

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message[0].name).to.be.equal(new_condition_transition.name);
            //check order sequence
            for(let i = 0; i < condition_transition.body.message.length; i++){
                expect(i).to.be.equal(condition_transition.body.message[i].order);
            }
        });

        it('PATCH#condition_transition/order => Should successfully change order to last', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .patch('/yel-ana/condition_transition/transition/' + basic_fixtures.transition_active_inactive.object._id + '/condition/' + new_condition_transition.condition + '/order/3')
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to patch condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.name).to.be.equal(new_condition_transition.name);
            expect(condition_transition.body.message.order).to.be.equal('3');

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message[3].name).to.be.equal(new_condition_transition.name);
            //check order sequence
            for(let i = 0; i < condition_transition.body.message.length; i++){
                expect(i).to.be.equal(condition_transition.body.message[i].order);
            }
        });

        it('PATCH#condition_transition/order => Should successfully change order to second', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .patch('/yel-ana/condition_transition/transition/' + basic_fixtures.transition_active_inactive.object._id + '/condition/' + new_condition_transition.condition + '/order/1')
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to patch condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.name).to.be.equal(new_condition_transition.name);
            expect(condition_transition.body.message.order).to.be.equal('1');

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message[1].name).to.be.equal(new_condition_transition.name);
            //check order sequence
            for(let i = 0; i < condition_transition.body.message.length; i++){
                expect(i).to.be.equal(condition_transition.body.message[i].order);
            }
        });

        it('PATCH#condition_transition/order => Should successfully change order to last if order received is too high', async() => {
            let condition_transition;
            try{
                condition_transition =
                    await chai.request(server)
                        .patch('/yel-ana/condition_transition/transition/' + basic_fixtures.transition_active_inactive.object._id + '/condition/' + new_condition_transition.condition + '/order/15')
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to patch condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message.name).to.be.equal(new_condition_transition.name);
            expect(condition_transition.body.message.order).to.be.equal(3);

            try{
                condition_transition =
                    await chai.request(server)
                        .get('/yel-ana/condition_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get condition_transition => ', err);
            }
            expect(condition_transition).to.have.status(200);
            expect(condition_transition.body.message[3].name).to.be.equal(new_condition_transition.name);
            //check order sequence
            for(let i = 0; i < condition_transition.body.message.length; i++){
                expect(i).to.be.equal(condition_transition.body.message[i].order);
            }
        });
    });
});