const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const requireDir = require('require-dir');
let models = requireDir('../../models');
let basic_fixtures = require('../fixtures/basic_fixtures');
const authentication_helper = require('../helpers/authentication');
const chaiHttp = require('chai-http');
let server = require('../../app');
let factoryGirlConfig = require('../config/factory-girl-config');

chai.use(chaiHttp);

let auth;
let new_transition;
let no_code_transition;
let no_name_transition;
let no_status_to_transition;
let no_workflow_transition;
let no_description_transition;
let wrong_id_transition;
let not_present_transition;

before(async () => {
    auth = await authentication_helper.authenticate(basic_fixtures);
});

describe('Transition', () => {
    describe('GET#transition/:transition_id', () => {
        it('Should return transition with specified id', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/' + basic_fixtures.transition_initial.object._id)
                        .set('x-access-token', auth.body.token);

                expect(transition).to.have.status(200);
                assert.equal(basic_fixtures.transition_initial.object.name, transition.body.message.name);

            } catch (err) {
                console.log('Error to get transition => ', err);
                assert(false);
            }

        });

        it('Should return 404 when transition is not found', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/' + '123')
                        .set('x-access-token', auth.body.token);

                expect(transition).to.have.status(404);
                expect(transition.body.message).to.be.equal('Not found');
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });
    });

    describe('GET#transition/workflow/:workflow_id', () => {
        it('Should return all transitions with specified workflow id', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/workflow/' + basic_fixtures.workflow.object._id)
                        .set('x-access-token', auth.body.token);

                expect(transition).to.have.status(200);
                expect(transition.body.message).to.not.be.empty;
                expect(transition.body.message[0].name).to.be.equal(basic_fixtures.transition_initial.object.name);
                expect(transition.body.message[1].name).to.be.equal(basic_fixtures.conditional_transition_initial_1.object.name);
                expect(transition.body.message[2].name).to.be.equal(basic_fixtures.conditional_transition_initial_2.object.name);
                expect(transition.body.message[3].name).to.be.equal(basic_fixtures.conditional_transition_initial_3.object.name);
                expect(transition.body.message[4].name).to.be.equal(basic_fixtures.transition_active_inactive.object.name);
                expect(transition.body.message[5].name).to.be.equal(basic_fixtures.transition_inactive_active.object.name);
                expect(transition.body.message[6].name).to.be.equal(basic_fixtures.conditional_transition_1.object.name);
                expect(transition.body.message[7].name).to.be.equal(basic_fixtures.conditional_transition_2.object.name);
                expect(transition.body.message[8].name).to.be.equal(basic_fixtures.conditional_transition_3.object.name);

            } catch (err) {
                console.log('Error to get transition => ', err);
                assert(false);
            }
        });
    });

    describe('GET#transition/workflow/:workflow_id/status/:status_id', () => {
        it('Should return all possible transitions with given workflow id and status id', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/workflow/' + basic_fixtures.workflow.object._id + '/status/' + basic_fixtures.status_active.object._id)
                        .set('x-access-token', auth.body.token);

                expect(transition).to.have.status(200);
                expect(transition.body.message).to.not.be.empty;
                expect(transition.body.message[0].name).to.be.equal(basic_fixtures.transition_active_inactive.object.name);
                expect(transition.body.message[1].name).to.be.equal(basic_fixtures.conditional_transition_1.object.name);
                expect(transition.body.message[2].name).to.be.equal(basic_fixtures.conditional_transition_2.object.name);
                expect(transition.body.message[3].name).to.be.equal(basic_fixtures.conditional_transition_3.object.name);
                expect(transition.body.message.length).to.be.equal(4);

            } catch (err) {
                console.log('Error to get transition => ', err);
                assert(false);
            }
        });

        it('Should return all initial transitions by given workflow', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/initial/workflow/' + basic_fixtures.workflow.object._id)
                        .set('x-access-token', auth.body.token);

                expect(transition).to.have.status(200);
                expect(transition.body.message).to.not.be.empty;
                expect(transition.body.message[0].name).to.be.equal(basic_fixtures.transition_initial.object.name);
                expect(transition.body.message[1].name).to.be.equal(basic_fixtures.conditional_transition_initial_1.object.name);
                expect(transition.body.message[2].name).to.be.equal(basic_fixtures.conditional_transition_initial_2.object.name);
                expect(transition.body.message[3].name).to.be.equal(basic_fixtures.conditional_transition_initial_3.object.name);
                expect(transition.body.message.length).to.be.equal(4);

            } catch (err) {
                console.log('Error to get transition => ', err);
                assert(false);
            }
        });
    });

    describe('GET#transition/allowed/workflow/:workflow_id/status/:status_id', () => {
        it('Should return all possible transitions with given workflow id and status id', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/allowed/workflow/' + basic_fixtures.workflow.object._id + '/status/' + basic_fixtures.status_active.object._id)
                        .set('x-access-token', auth.body.token);

                expect(transition).to.have.status(200);
                expect(transition.body.message).to.not.be.empty;
                expect(transition.body.message[0].name).to.be.equal(basic_fixtures.transition_active_inactive.object.name);
                expect(transition.body.message[1].name).to.be.equal(basic_fixtures.conditional_transition_1.object.name);
                expect(transition.body.message[2].name).to.be.equal(basic_fixtures.conditional_transition_2.object.name);
                expect(transition.body.message.length).to.be.equal(3);

            } catch (err) {
                console.log('Error to get transition => ', err);
                assert(false);
            }
        });

        it('Should return all initial transitions by given workflow', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/allowed/initial/workflow/' + basic_fixtures.workflow.object._id)
                        .set('x-access-token', auth.body.token);

                expect(transition).to.have.status(200);
                expect(transition.body.message).to.not.be.empty;
                expect(transition.body.message[0].name).to.be.equal(basic_fixtures.transition_initial.object.name);
                expect(transition.body.message[1].name).to.be.equal(basic_fixtures.conditional_transition_initial_1.object.name);
                expect(transition.body.message[2].name).to.be.equal(basic_fixtures.conditional_transition_initial_2.object.name);
                expect(transition.body.message.length).to.be.equal(3);

            } catch (err) {
                console.log('Error to get transition => ', err);
                assert(false);
            }
        });
    });

    describe('New Transition Services', () => {
        before(async () => {
            new_transition = await factoryGirlConfig.factoryGirl.build('new_transition', {
                workflow: basic_fixtures.workflow.object._id,
                status_to: basic_fixtures.status_active.object._id,
                application: basic_fixtures.application.object._id
            });
            no_code_transition = JSON.parse(JSON.stringify(new_transition));
            delete no_code_transition.code;
            no_name_transition = JSON.parse(JSON.stringify(new_transition));
            delete no_name_transition.name;
            no_workflow_transition = JSON.parse(JSON.stringify(new_transition));
            delete no_workflow_transition.workflow;
            no_status_to_transition = JSON.parse(JSON.stringify(new_transition));
            delete no_status_to_transition.status_to;
            no_description_transition = JSON.parse(JSON.stringify(new_transition));
            delete no_description_transition.description;
            wrong_id_transition = JSON.parse(JSON.stringify(new_transition));
            wrong_id_transition._id = 'aaa';
            not_present_transition = JSON.parse(JSON.stringify(new_transition));
            not_present_transition._id = require('mongoose').Types.ObjectId();
        });

        it('POST#transition => Should return 400 in case of trying to create without mandatory params', async () => {
            let transition;
            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_code_transition);
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('code is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }

            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_name_transition);
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('name is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }

            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_workflow_transition);
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('workflow is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }

            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_status_to_transition);
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('status_to is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('POST#transition => Should successfully insert transition into the workflow', async () => {
            let transition;
            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_transition);

            } catch (err) {
                console.log('Error to post transition => ', err);
            }
            expect(transition).to.have.status(204);

            try {
                transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/workflow/' + basic_fixtures.workflow.object._id)
                        .set('x-access-token', auth.body.token);

            } catch (err) {
                console.log('Error to get transition => ', err);
            }

            expect(transition).to.have.status(200);
            expect(transition.body.message[transition.body.message.length - 1].name).to.be.equal(new_transition.name);
            new_transition._id = transition.body.message[transition.body.message.length - 1]._id;
        });

        it('PUT#transition => Should return 400 in case of trying to update without mandatory params', async () => {
            let transition;
            let no_id_transition = JSON.parse(JSON.stringify(new_transition));
            delete no_id_transition._id;

            try {
                transition =
                    await chai.request(server)
                        .put('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_id_transition);
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('_id is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }

            try {
                transition =
                    await chai.request(server)
                        .put('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_code_transition);
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('code is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }

            try {
                transition =
                    await chai.request(server)
                        .put('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_name_transition);
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('name is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }

            try {
                transition =
                    await chai.request(server)
                        .put('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_status_to_transition);
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('status_to is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('PUT#transition => Should return 400 in case of trying to update with id wrongly formatted', async () => {
            let transition;
            try {
                transition =
                    await chai.request(server)
                        .put('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(wrong_id_transition);
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('_id provided is invalid');
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('PUT#transition => Should return 404 in case of trying to update with id not present yet', async () => {
            let transition;
            try {
                transition =
                    await chai.request(server)
                        .put('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(not_present_transition);
                expect(transition).to.have.status(404);
                expect(transition.body.message).to.be.equal('Not found');
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('PUT#transition => Should successfully update transition', async () => {
            let transition;
            let new_name = "abcdef";
            let new_code = "aaa";
            let new_description = "aaa aaa";
            new_transition.name = new_name;
            new_transition.code = new_code;
            new_transition.description = new_description;
            new_transition.status_from = basic_fixtures.status_inactive.object._id;
            new_transition.status_to = basic_fixtures.status_active.object._id;
            new_transition.application = basic_fixtures.application.object._id;

            try {
                transition =
                    await chai.request(server)
                        .put('/yel-ana/transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_transition);

            } catch (err) {
                console.log('Error to put transition => ', err);
            }
            expect(transition).to.have.status(204);

            try {
                transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/workflow/' + basic_fixtures.workflow.object._id)
                        .set('x-access-token', auth.body.token);

            } catch (err) {
                console.log('Error to get transition => ', err);
            }

            expect(transition).to.have.status(200);
            expect(transition.body.message[transition.body.message.length - 1].name).to.be.equal(new_name);
            expect(transition.body.message[transition.body.message.length - 1].code).to.be.equal(new_code);
            expect(transition.body.message[transition.body.message.length - 1].description).to.be.equal(new_description);
            expect(transition.body.message[transition.body.message.length - 1].status_from).to.be.equal(basic_fixtures.status_inactive.object._id.toString());
            expect(transition.body.message[transition.body.message.length - 1].status_to).to.be.equal(basic_fixtures.status_active.object._id.toString());
        });

        it('DELETE#transition => Should return 404 when transition is not found', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .delete('/yel-ana/transition/' + require('mongoose').Types.ObjectId())
                        .set('x-access-token', auth.body.token);
                expect(transition).to.have.status(404);
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('DELETE#transition => Should delete transition successfully', async () => {
            let transition;
            try {
                transition =
                    await chai.request(server)
                        .delete('/yel-ana/transition/' + new_transition._id)
                        .set('x-access-token', auth.body.token);
                expect(transition).to.have.status(204);
            } catch (err) {
                assert(false);
            }

            try {
                transition =
                    await chai.request(server)
                        .get('/yel-ana/transition/' + new_transition._id)
                        .set('x-access-token', auth.body.token);
                expect(transition).to.have.status(404);
                expect(transition.body.message).to.be.equal('Not found');
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });
    });

    describe('POST#transition/advance', () => {
        it('Should return 404 when transition is not found', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: require('mongoose').Types.ObjectId(),
                            object_id: require('mongoose').Types.ObjectId()
                        });
                expect(transition).to.have.status(404);
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('Should return 400 when object_id is not sent', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: require('mongoose').Types.ObjectId()
                        });
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('object_id is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('Should return 400 when transition_id is not sent', async () => {
            try {
                let transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            object_id: require('mongoose').Types.ObjectId()
                        });
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('transition_id is mandatory');
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('Should successfully advance initial transition', async () => {
            let transition;
            let object_id = require('mongoose').Types.ObjectId();
            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.transition_initial.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(200);
                expect(transition.body.message.status.name).to.be.equal(basic_fixtures.status_active.object.name);
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('Should forbid advance initial transition when not allowed by condition', async () => {
            let transition;
            let object_id = require('mongoose').Types.ObjectId();
            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.conditional_transition_initial_3.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('transition is not allowed by condition');
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('Should forbid advance initial transition when not allowed by validation', async () => {
            let transition;
            let object_id = require('mongoose').Types.ObjectId();
            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.conditional_transition_initial_1.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('transition is not allowed by validation');
            } catch (err) {
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('Should successfully advance transition', async () => {
            let transition;
            let object_id = require('mongoose').Types.ObjectId();
            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.transition_active_inactive.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(200);
                expect(transition.body.message.status.name).to.be.equal(basic_fixtures.status_inactive.object.name);
            } catch (err) {
                assert(false, 'An error occurred');
            }
        });

        it('Should successfully advance transition allowed by validation', async () => {
            let transition;
            let object_id = require('mongoose').Types.ObjectId();
            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.transition_active_inactive.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(200);
                expect(transition.body.message.status.name).to.be.equal(basic_fixtures.status_inactive.object.name);
            } catch (err) {
                assert(false, 'An error occurred');
            }

            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.conditional_transition_2.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(200);
                expect(transition.body.message.status.name).to.be.equal(basic_fixtures.status_inactive.object.name);
            } catch (err) {
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('Should not allow advance transition forbidden by condition', async () => {
            let transition;
            let object_id = require('mongoose').Types.ObjectId();
            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.transition_inactive_active.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(200);
                expect(transition.body.message.status.name).to.be.equal(basic_fixtures.status_active.object.name);
            } catch (err) {
                console.log(err);
                assert(false, 'An error occurred');
            }

            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.conditional_transition_3.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('transition is not allowed by condition');
            } catch (err) {
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('Should not allow advance transition forbidden by validation', async () => {
            let transition;
            let object_id = require('mongoose').Types.ObjectId();
            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.transition_inactive_active.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(200);
                expect(transition.body.message.status.name).to.be.equal(basic_fixtures.status_active.object.name);
            } catch (err) {
                console.log(err);
                assert(false, 'An error occurred');
            }

            try {
                transition =
                    await chai.request(server)
                        .post('/yel-ana/transition/advance')
                        .set('x-access-token', auth.body.token)
                        .send({
                            transition_id: basic_fixtures.conditional_transition_1.object._id,
                            object_id: object_id
                        });
                expect(transition).to.have.status(400);
                expect(transition.body.message).to.be.equal('transition is not allowed by validation');
            } catch (err) {
                console.log(err);
                assert(false, 'An error occurred');
            }
        });
    });
});
