const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const chaiHttp = require('chai-http');
const server = require('../../app');
let factoryGirlConfig = require('../config/factory-girl-config');
const authentication_helper = require('../helpers/authentication');
let basic_fixtures = require('../fixtures/basic_fixtures');

chai.use(chaiHttp);

let auth;
let new_validation;
let no_name_validation;
let no_entity_validation;
let no_description_validation;
let wrong_id_validation;
let not_present_validation;

before(async() => {
    auth = await authentication_helper.authenticate(basic_fixtures);
});

describe('Validation', () => {
    describe('GET#validation/:validation_id', () => {
        it('Should return validation with specified id', async() => {
            try{
                let validation =
                    await chai.request(server)
                        .get('/yel-ana/validation/' + basic_fixtures.validation.object._id)
                        .set('x-access-token', auth.body.token);

                expect(validation).to.have.status(200);
                assert.equal(basic_fixtures.validation.object.name, validation.body.message.name);

            }catch(err){
                console.log('Error to get validation => ', err);
                assert(false);
            }

        });

        it('Should return 404 when validation is not found', async() => {
            try{
                let validation =
                    await chai.request(server)
                        .get('/yel-ana/validation/' + '123')
                        .set('x-access-token', auth.body.token);
                expect(validation).to.have.status(404);
                expect(validation.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });
    });

    describe('GET#validation/entity/:entity_id', () => {
        it('Should return all validation with specified entity id', async() => {
            try{
                let validation =
                    await chai.request(server)
                        .get('/yel-ana/validation/entity/'+basic_fixtures.entity.object._id)
                        .set('x-access-token', auth.body.token);

                expect(validation).to.have.status(200);
                expect(validation.body.message).to.not.be.empty;
                expect(validation.body.message[0].name).to.be.equal(basic_fixtures.validation.object.name);

            }catch(err){
                console.log('Error to get validation => ', err);
                assert(false);
            }
        });
    });

    describe('New Validation Services', () => {
        before(async() => {
            new_validation = await factoryGirlConfig.factoryGirl.build('new_validation',{entity: basic_fixtures.entity.object._id, application: basic_fixtures.application.object._id});
            no_name_validation = JSON.parse(JSON.stringify(new_validation));
            delete no_name_validation.name;
            no_entity_validation = JSON.parse(JSON.stringify(new_validation));
            delete no_entity_validation.entity;
            no_description_validation = JSON.parse(JSON.stringify(new_validation));
            delete no_description_validation.description;
            wrong_id_validation = JSON.parse(JSON.stringify(new_validation));
            wrong_id_validation._id = 'aaa';
            not_present_validation = JSON.parse(JSON.stringify(new_validation));
            not_present_validation._id = require('mongoose').Types.ObjectId();
        });

        it('POST#validation => Should return 400 in case of trying to create without mandatory params', async() => {
            let validation;
            try{
                validation =
                    await chai.request(server)
                        .post('/yel-ana/validation')
                        .set('x-access-token', auth.body.token)
                        .send(no_name_validation);
                expect(validation).to.have.status(400);
                expect(validation.body.message).to.be.equal('name is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                validation =
                    await chai.request(server)
                        .post('/yel-ana/validation')
                        .set('x-access-token', auth.body.token)
                        .send(no_entity_validation);
                expect(validation).to.have.status(400);
                expect(validation.body.message).to.be.equal('entity is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('POST#validation => Should successfully insert validation into the entity', async() => {
            let validation;
            try{
                validation =
                    await chai.request(server)
                        .post('/yel-ana/validation')
                        .set('x-access-token', auth.body.token)
                        .send(new_validation);

            }catch(err){
                console.log('Error to post validation => ', err);
            }
            expect(validation).to.have.status(200);
            expect(validation.body.message.name).to.be.equal(new_validation.name);

            try{
                validation =
                    await chai.request(server)
                        .get('/yel-ana/validation/entity/'+basic_fixtures.entity.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation => ', err);
            }
            expect(validation).to.have.status(200);
            expect(validation.body.message[validation.body.message.length - 1].name).to.be.equal(new_validation.name);
            new_validation._id = validation.body.message[1]._id;
        });

        it('PUT#validation => Should return 400 in case of trying to update without mandatory params', async() => {
            let validation;
            let no_id_validation = JSON.parse(JSON.stringify(new_validation));
            delete no_id_validation._id;

            try{
                validation =
                    await chai.request(server)
                        .put('/yel-ana/validation')
                        .set('x-access-token', auth.body.token)
                        .send(no_id_validation);
                expect(validation).to.have.status(400);
                expect(validation.body.message).to.be.equal('_id is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                validation =
                    await chai.request(server)
                        .put('/yel-ana/validation')
                        .set('x-access-token', auth.body.token)
                        .send(no_name_validation);
                expect(validation).to.have.status(400);
                expect(validation.body.message).to.be.equal('name is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('PUT#validation => Should return 400 in case of trying to update with id wrongly formatted', async() => {
            let validation;
            try{
                validation =
                    await chai.request(server)
                        .put('/yel-ana/validation')
                        .set('x-access-token', auth.body.token)
                        .send(wrong_id_validation);
                expect(validation).to.have.status(400);
                expect(validation.body.message).to.be.equal('_id provided is invalid');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('PUT#validation => Should return 404 in case of trying to update with id not present yet', async() => {
            let validation;
            try{
                validation =
                    await chai.request(server)
                        .put('/yel-ana/validation')
                        .set('x-access-token', auth.body.token)
                        .send(not_present_validation);
                expect(validation).to.have.status(404);
                expect(validation.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('PUT#validation => Should successfully update validation', async() => {
            let validation;
            let new_name = "abcdef";
            let new_description = "aaa aaa";
            new_validation.name = new_name;
            new_validation.description = new_description;
            new_validation.application = basic_fixtures.application.object._id;

            try{
                validation =
                    await chai.request(server)
                        .put('/yel-ana/validation')
                        .set('x-access-token', auth.body.token)
                        .send(new_validation);

            }catch(err){
                console.log('Error to put validation => ', err);
            }
            expect(validation).to.have.status(200);
            expect(validation.body.message.name).to.be.equal(new_validation.name);

            try{
                validation =
                    await chai.request(server)
                        .get('/yel-ana/validation/entity/'+basic_fixtures.entity.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation => ', err);
            }

            expect(validation).to.have.status(200);
            expect(validation.body.message[1].name).to.be.equal(new_name);
            expect(validation.body.message[1].description).to.be.equal(new_description);
        });

        it('DELETE#validation => Should return 404 when validation is not found', async() => {
            try{
                let validation =
                    await chai.request(server)
                        .delete('/yel-ana/validation/' + require('mongoose').Types.ObjectId())
                        .set('x-access-token', auth.body.token);

                expect(validation).to.have.status(404);
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('DELETE#validation => Should delete validation successfully', async() => {
            let validation;
            try{
                validation =
                    await chai.request(server)
                        .delete('/yel-ana/validation/' + new_validation._id)
                        .set('x-access-token', auth.body.token);
                expect(validation).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                validation =
                    await chai.request(server)
                        .get('/yel-ana/validation/' + new_validation._id)
                        .set('x-access-token', auth.body.token);
                expect(validation).to.have.status(404);
                expect(validation.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });
    });
});