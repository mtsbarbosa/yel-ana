const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const chaiHttp = require('chai-http');
const server = require('../../app');
let factoryGirlConfig = require('../config/factory-girl-config');
const authentication_helper = require('../helpers/authentication');
let basic_fixtures = require('../fixtures/basic_fixtures');

chai.use(chaiHttp);

let auth;
let new_application;
let no_name_application;
let no_hash_key_application;
let wrong_id_application;
let not_present_application;

let temp_token;

before(async() => {
    auth = await authentication_helper.authenticate_user(basic_fixtures);
});

describe('Application', () => {
    describe('GET#application/:application_id', () => {
        it('Should return application with specified id', async() => {
            try{
                let application =
                    await chai.request(server)
                        .get('/yel-ana/application/'+basic_fixtures.application.object._id)
                        .set('x-user-token', auth.body.token);

                expect(application).to.have.status(200);
                assert.equal(basic_fixtures.application.object.name, application.body.message.name);

            }catch(err){
                console.log('Error to get application => ', err);
                assert(false);
            }

        });

        it('Should return 404 when application is not found', async() => {
            try{
                let application =
                    await chai.request(server)
                        .get('/yel-ana/application/' + '123')
                        .set('x-user-token', auth.body.token);
                expect(application).to.have.status(404);
                expect(application.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });
    });

    describe('GET#application', () => {
        it('Should return all applications from logged user', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .get('/yel-ana/application')
                        .set('x-user-token', auth.body.token);

                expect(resp).to.have.status(200);
                expect(resp.body.message).to.not.be.empty;
                expect(resp.body.message[0].name).to.be.equal(basic_fixtures.application.object.name);
                expect(resp.body.message[1].name).to.be.equal(basic_fixtures.not_root_application.object.name);

            }catch(err){
                console.log('Error to get application => ', err);
                assert(false);
            }

        });
    });

    describe('Application -> change hash_key', () => {
        it('[POST]application/:application_id/hash_key => Should return 404 in case application is not found', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .post('/yel-ana/application/1323652/hash_key')
                        .set('x-user-token', auth.body.token)
                        .send({email: basic_fixtures.user.object.email, password: basic_fixtures.user.object.password});

                expect(resp).to.have.status(404);
                expect(resp.body.message).to.be.equal('Not found');

            }catch(err){
                console.log('Error to get token => ', err);
                assert(false);
            }
        });

        it('[POST]application/:application_id/hash_key => Should return 400 in case of not sending mandatory params', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .post('/yel-ana/application/'+ basic_fixtures.application.object._id +'/hash_key')
                        .set('x-user-token', auth.body.token)
                        .send({});

                expect(resp).to.have.status(400);
                expect(resp.body.message).to.be.equal('email,password is mandatory');
            }catch(err){
                console.log('Error to get token => ', err);
                assert(false);
            }
        });

        it('[POST]application/:application_id/hash_key => Should return 200 with token', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .post('/yel-ana/application/'+ basic_fixtures.application.object._id +'/hash_key')
                        .set('x-user-token', auth.body.token)
                        .send({email: basic_fixtures.user.object.email, password: basic_fixtures.user.object.password});

                expect(resp).to.have.status(200);
                expect(resp.body.token).to.not.be.empty;
                temp_token = resp.body.token;
            }catch(err){
                console.log('Error to get token => ', err);
                assert(false);
            }
        });

        it('[PATCH]application/:application_id/hash_key => Should return 403 in case of not sending token', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .patch('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send({});

                expect(resp).to.have.status(403);
                expect(resp.body.message).to.be.equal('No token provided.');
            }catch(err){
                console.log('Error to patch application hash_key => ', err);
                assert(false);
            }
        });

        it('[PATCH]application/:application_id/hash_key => Should return 403 in case of sending wrong token', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .patch('/yel-ana/application')
                        .set({'x-temp-token': '21346454344645464', 'x-user-token': auth.body.token})
                        .send({});

                expect(resp).to.have.status(403);
                expect(resp.body.message).to.be.equal('Failed to authenticate token.');
            }catch(err){
                console.log('Error to patch application hash_key => ', err);
                assert(false);
            }
        });

        it('[PATCH]application/:application_id/hash_key => Should return 400 in case of not sending mandatory params', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .patch('/yel-ana/application')
                        .set({'x-temp-token': temp_token, 'x-user-token': auth.body.token})
                        .send({});

                expect(resp).to.have.status(400);
                expect(resp.body.message).to.be.equal('_id,hash_key_new,hash_key_new_confirmation is mandatory');
            }catch(err){
                console.log('Error to patch application hash_key => ', err);
                assert(false);
            }
        });

        it('[PATCH]application/:application_id/hash_key => Should return 404 in case of not found application', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .patch('/yel-ana/application')
                        .set({'x-temp-token': temp_token, 'x-user-token': auth.body.token})
                        .send({_id: require('mongoose').Types.ObjectId(), hash_key_new: 'newSecret1', hash_key_new_confirmation: 'newSecret1'});

                expect(resp).to.have.status(404);
                expect(resp.body.message).to.be.equal('Not Found');
            }catch(err){
                console.log('Error to patch application hash_key => ', err);
                assert(false);
            }
        });

        it('[PATCH]application/:application_id/hash_key => Should return 400 in case of not matching passwords', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .patch('/yel-ana/application')
                        .set({'x-temp-token': temp_token, 'x-user-token': auth.body.token})
                        .send({_id: basic_fixtures.application.object._id, hash_key_new: 'newSecret3', hash_key_new_confirmation: 'newSecret2'});

                expect(resp).to.have.status(400);
                expect(resp.body.message).to.be.equal('hash_key_new does not match hash_key_new_confirmation');
            }catch(err){
                console.log('Error to patch application hash_key => ', err);
                assert(false);
            }
        });

        it('[PATCH]application/:application_id/hash_key => Should return 204 and change hash_key', async() => {
            try{
                let resp =
                    await chai.request(server)
                        .patch('/yel-ana/application')
                        .set({'x-temp-token': temp_token, 'x-user-token': auth.body.token})
                        .send({_id: basic_fixtures.application.object._id, hash_key_new: 'newSecret', hash_key_new_confirmation: 'newSecret'});
                expect(resp).to.have.status(204);

                let auth_app =
                    await chai.request(server)
                        .post('/yel-ana/authenticate')
                        .set('content-type', 'application/x-www-form-urlencoded')
                        .send({name: basic_fixtures.application.object.name, hash_key: 'newSecret'});

                expect(auth_app).to.have.status(200);
                assert.isNotEmpty(auth_app.body.token);

            }catch(err){
                console.log('Error to patch application hash_key => ', err);
                assert(false);
            }
        });

        after(async() => {
            try{
                let resp =
                    await chai.request(server)
                        .post('/yel-ana/application/'+ basic_fixtures.application.object._id +'/hash_key')
                        .set('x-user-token', auth.body.token)
                        .send({email: basic_fixtures.user.object.email, password: basic_fixtures.user.object.password});

                expect(resp).to.have.status(200);
                expect(resp.body.token).to.not.be.empty;
                temp_token = resp.body.token;

                resp =
                    await chai.request(server)
                        .patch('/yel-ana/application')
                        .set({'x-temp-token': temp_token, 'x-user-token': auth.body.token})
                        .send({_id: basic_fixtures.application.object._id, hash_key_new: basic_fixtures.application.object.hash_key, hash_key_new_confirmation: basic_fixtures.application.object.hash_key});

                expect(resp).to.have.status(204);
            }catch(err){
                console.log('Error to reset hash_key to application => ', err);
                assert(false);
            }
        });
    });

    describe('New Application Services', () => {
        before(async() => {
            new_application = await factoryGirlConfig.factoryGirl.build('application',{name: 'Test-3', user: basic_fixtures.user.object._id});
            no_name_application = JSON.parse(JSON.stringify(new_application));
            delete no_name_application.name;
            no_hash_key_application = JSON.parse(JSON.stringify(new_application));
            delete no_hash_key_application.hash_key;
            wrong_id_application = JSON.parse(JSON.stringify(new_application));
            wrong_id_application._id = 'aaa';
            not_present_application = JSON.parse(JSON.stringify(new_application));
            not_present_application._id = require('mongoose').Types.ObjectId();
        });

        it('POST#application => Should return 403 in case of not passing user token', async() => {
            let resp;
            try{
                resp =
                    await chai.request(server)
                        .post('/yel-ana/application')
                        .send(no_name_application);
                expect(resp).to.have.status(403);
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('POST#application => Should return 400 in case of trying to create without mandatory params', async() => {
            let resp;
            try{
                resp =
                    await chai.request(server)
                        .post('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send(no_name_application);
                expect(resp).to.have.status(400);
                expect(resp.body.message).to.be.equal('name is mandatory');
            }catch(err){
                console.log('ERR',err);
                assert(false, 'An error occurred');
            }

            try{
                resp =
                    await chai.request(server)
                        .post('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send(no_hash_key_application);
                expect(resp).to.have.status(400);
                expect(resp.body.message).to.be.equal('hash_key is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('POST#application => Should successfully insert application', async() => {
            let application;
            try{
                application =
                    await chai.request(server)
                        .post('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send(new_application);

            }catch(err){
                console.log('Error to post application => ', err);
            }
            expect(application).to.have.status(200);
            expect(application.body.message.name).to.be.equal(new_application.name);
            expect(application.body.message).to.not.have.property('hash_key');
            expect(application.body.message).to.have.property('_id');
        });

        it('PUT#application => Should return 400 in case of trying to update without mandatory params', async() => {
            let application;
            let no_id_application = JSON.parse(JSON.stringify(new_application));
            delete no_id_application._id;

            try{
                application =
                    await chai.request(server)
                        .put('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send(no_id_application);
                expect(application).to.have.status(400);
                expect(application.body.message).to.be.equal('_id is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                application =
                    await chai.request(server)
                        .put('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send(no_name_application);
                expect(application).to.have.status(400);
                expect(application.body.message).to.be.equal('name is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('PUT#application => Should return 400 in case of trying to update with id wrongly formatted', async() => {
            let application;
            try{
                application =
                    await chai.request(server)
                        .put('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send(wrong_id_application);
                expect(application).to.have.status(400);
                expect(application.body.message).to.be.equal('_id provided is invalid');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('PUT#application => Should return 404 in case of trying to update with id not present yet', async() => {
            let application;
            try{
                application =
                    await chai.request(server)
                        .put('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send(not_present_application);
                expect(application).to.have.status(404);
                expect(application.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('PUT#application => Should successfully update application', async() => {
            let application;
            const new_name = "abcdef";
            const new_description = "aaa aaa";
            new_application.name = new_name;
            new_application.description = new_description;

            const old_name = basic_fixtures.application.object.name;
            const old_description = basic_fixtures.application.object.description;

            try{
                application =
                    await chai.request(server)
                        .put('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send(new_application);

            }catch(err){
                console.log('Error to put application => ', err);
            }
            expect(application).to.have.status(200);
            expect(application.body.message._id).to.be.equal(basic_fixtures.application.object._id.toString());
            expect(application.body.message.name).to.be.equal(new_name);
            expect(application.body.message.description).to.be.equal(new_description);

            new_application.name = old_name;
            new_application.description = old_description;

            try{
                application =
                    await chai.request(server)
                        .put('/yel-ana/application')
                        .set('x-user-token', auth.body.token)
                        .send(new_application);

            }catch(err){
                console.log('Error to put application => ', err);
            }
            expect(application).to.have.status(200);
            expect(application.body.message.name).to.be.equal(old_name);
        });

        it('DELETE#application => Should return 404 when application is not found', async() => {
            try{
                const application =
                    await chai.request(server)
                        .delete('/yel-ana/application/' + require('mongoose').Types.ObjectId())
                        .set('x-user-token', auth.body.token);

                expect(application).to.have.status(404);
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('DELETE#application => Should delete application successfully', async() => {
            let application;
            try{
                application =
                    await chai.request(server)
                        .delete('/yel-ana/application/' + new_application._id)
                        .set('x-user-token', auth.body.token);
                expect(application).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                application =
                    await chai.request(server)
                        .get('/yel-ana/application/' + new_application._id)
                        .set('x-user-token', auth.body.token);
                expect(application).to.have.status(404);
                expect(application.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });
    });
});
