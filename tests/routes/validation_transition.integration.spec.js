const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const chaiHttp = require('chai-http');
const server = require('../../app');
let factoryGirlConfig = require('../config/factory-girl-config');
const authentication_helper = require('../helpers/authentication');
let basic_fixtures = require('../fixtures/basic_fixtures');

chai.use(chaiHttp);

let auth;
let new_validation_transition;
let no_validation_validation_transition;
let no_transition_validation_transition;
let wrong_validation_validation_transition;
let wrong_transition_validation_transition;
let not_present_validation_transition;

before(async() => {
    auth = await authentication_helper.authenticate(basic_fixtures);
});

describe('Validation Transition', () => {
    describe('GET#validation_transition/transition/:transition_id', () => {
        it('Should return validation_transition with specified transition id', async() => {
            try{
                let validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/' + basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

                expect(validation_transition).to.have.status(200);
                assert.equal(basic_fixtures.validation_transition.object.validation, validation_transition.body.message[0].validation._id);
                assert.equal(basic_fixtures.validation_linked_1.object.name, validation_transition.body.message[0].validation.name);
                assert.equal(basic_fixtures.validation_transition_2.object.validation, validation_transition.body.message[1].validation._id);
                assert.equal(basic_fixtures.validation_linked_2.object.name, validation_transition.body.message[1].validation.name);
                assert.equal(basic_fixtures.validation_transition_3.object.validation, validation_transition.body.message[2].validation._id);
                assert.equal(basic_fixtures.validation_linked_3.object.name, validation_transition.body.message[2].validation.name);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
                assert(false);
            }

        });

        it('Should return 404 when validation_transition is not found', async() => {
            try{
                let validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/' + require('mongoose').Types.ObjectId())
                        .set('x-access-token', auth.body.token);
                expect(validation_transition).to.have.status(404);
                expect(validation_transition.body.message).to.be.equal('Not found');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });
    });

    describe('New Validation Transition Services', () => {
        before(async () => {
            new_validation_transition = await factoryGirlConfig.factoryGirl.build('new_validation_transition', {
                validation: basic_fixtures.new_validation_linked.object._id,
                transition: basic_fixtures.transition_active_inactive.object._id,
                application: basic_fixtures.application.object._id
            });
            no_validation_validation_transition = JSON.parse(JSON.stringify(new_validation_transition));
            delete no_validation_validation_transition.validation;
            no_transition_validation_transition = JSON.parse(JSON.stringify(new_validation_transition));
            delete no_transition_validation_transition.transition;
            wrong_validation_validation_transition = JSON.parse(JSON.stringify(new_validation_transition));
            wrong_validation_validation_transition.validation = require('mongoose').Types.ObjectId();
            wrong_transition_validation_transition = JSON.parse(JSON.stringify(new_validation_transition));
            wrong_transition_validation_transition.transition = require('mongoose').Types.ObjectId();
            not_present_validation_transition = JSON.parse(JSON.stringify(new_validation_transition));
            not_present_validation_transition._id = require('mongoose').Types.ObjectId();
        });

        it('POST#validation_transition => Should return 400 in case of trying to create without mandatory params', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .post('/yel-ana/validation_transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_validation_validation_transition);
                expect(validation_transition).to.have.status(400);
                expect(validation_transition.body.message).to.be.equal('validation is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                validation_transition =
                    await chai.request(server)
                        .post('/yel-ana/validation_transition')
                        .set('x-access-token', auth.body.token)
                        .send(no_transition_validation_transition);
                expect(validation_transition).to.have.status(400);
                expect(validation_transition.body.message).to.be.equal('transition is mandatory');
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('POST#validation_transition => Should successfully insert validation_transition into the entity', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .post('/yel-ana/validation_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_validation_transition);

            }catch(err){
                console.log('Error to post validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.name).to.be.equal(new_validation_transition.name);

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message[validation_transition.body.message.length - 1].name).to.be.equal(new_validation_transition.name);
        });

        it('DELETE#validation_transition => Should return 404 when validation_transition is not found', async() => {
            try{
                let validation_transition =
                    await chai.request(server)
                        .delete('/yel-ana/validation_transition/transition/' + new_validation_transition.transition + '/validation/' + require('mongoose').Types.ObjectId())
                        .set('x-access-token', auth.body.token);

                expect(validation_transition).to.have.status(404);
            }catch(err){
                assert(false, 'An error occurred');
            }

            try{
                let validation_transition =
                    await chai.request(server)
                        .delete('/yel-ana/validation_transition/transition/' + require('mongoose').Types.ObjectId() + '/validation/' + new_validation_transition.validation)
                        .set('x-access-token', auth.body.token);

                expect(validation_transition).to.have.status(404);
            }catch(err){
                assert(false, 'An error occurred');
            }
        });

        it('DELETE#validation_transition => Should delete validation_transition successfully', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .delete('/yel-ana/validation_transition/transition/' + new_validation_transition.transition + '/validation/' + new_validation_transition.validation)
                        .set('x-access-token', auth.body.token);
                expect(validation_transition).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/' + new_validation_transition.transition)
                        .set('x-access-token', auth.body.token);
                expect(validation_transition).to.have.status(200);
                expect(validation_transition.body.message[validation_transition.body.message.length - 1].validation.name).to.not.be.equal(basic_fixtures.new_validation_linked.object.name);

                //check order sequence
                for(let i = 0; i < validation_transition.body.message.length; i++){
                    expect(i).to.be.equal(validation_transition.body.message[i].order);
                }
            }catch(err){
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('POST#validation_transition => Should successfully insert validation_transition into the first position', async() => {
            let validation_transition;
            new_validation_transition.order = 0;
            try{
                validation_transition =
                    await chai.request(server)
                        .post('/yel-ana/validation_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_validation_transition);

            }catch(err){
                console.log('Error to post validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.name).to.be.equal(new_validation_transition.name);

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message[0].name).to.be.equal(new_validation_transition.name);
            //check order sequence
            for(let i = 0; i < validation_transition.body.message.length; i++){
                expect(i).to.be.equal(validation_transition.body.message[i].order);
            }
        });

        it('DELETE#validation_transition => Should delete validation_transition successfully at first position', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .delete('/yel-ana/validation_transition/transition/' + new_validation_transition.transition + '/validation/' + new_validation_transition.validation)
                        .set('x-access-token', auth.body.token);
                expect(validation_transition).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/' + new_validation_transition.transition)
                        .set('x-access-token', auth.body.token);
                expect(validation_transition).to.have.status(200);
                expect(validation_transition.body.message[0].validation.name).to.not.be.equal(basic_fixtures.new_validation_linked.object.name);

                //check order sequence
                for(let i = 0; i < validation_transition.body.message.length; i++){
                    expect(i).to.be.equal(validation_transition.body.message[i].order);
                }
            }catch(err){
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('POST#validation_transition => Should successfully insert validation_transition into the second position', async() => {
            let validation_transition;
            new_validation_transition.order = 1;
            try{
                validation_transition =
                    await chai.request(server)
                        .post('/yel-ana/validation_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_validation_transition);

            }catch(err){
                console.log('Error to post validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.name).to.be.equal(new_validation_transition.name);

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message[1].name).to.be.equal(new_validation_transition.name);
            //check order sequence
            for(let i = 0; i < validation_transition.body.message.length; i++){
                expect(i).to.be.equal(validation_transition.body.message[i].order);
            }
        });

        it('DELETE#validation_transition => Should delete validation_transition successfully at second position', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .delete('/yel-ana/validation_transition/transition/' + new_validation_transition.transition + '/validation/' + new_validation_transition.validation)
                        .set('x-access-token', auth.body.token);
                expect(validation_transition).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/' + new_validation_transition.transition)
                        .set('x-access-token', auth.body.token);
                expect(validation_transition).to.have.status(200);
                expect(validation_transition.body.message[1].validation.name).to.not.be.equal(basic_fixtures.new_validation_linked.object.name);

                //check order sequence
                for(let i = 0; i < validation_transition.body.message.length; i++){
                    expect(i).to.be.equal(validation_transition.body.message[i].order);
                }
            }catch(err){
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('POST#validation_transition => Should successfully insert validation_transition into the end', async() => {
            let validation_transition;
            new_validation_transition.order = 3;
            try{
                validation_transition =
                    await chai.request(server)
                        .post('/yel-ana/validation_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_validation_transition);

            }catch(err){
                console.log('Error to post validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.name).to.be.equal(new_validation_transition.name);

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message[3].name).to.be.equal(new_validation_transition.name);
            //check order sequence
            for(let i = 0; i < validation_transition.body.message.length; i++){
                expect(i).to.be.equal(validation_transition.body.message[i].order);
            }
        });

        it('POST#validation_transition => Should return 400 in case of inserting the same validation', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .post('/yel-ana/validation_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_validation_transition);

            }catch(err){
                console.log('Error to post validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(400);

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.length).to.be.equal(4);
            expect(validation_transition.body.message[3].name).to.be.equal(new_validation_transition.name);
            //check order sequence
            for(let i = 0; i < validation_transition.body.message.length; i++){
                expect(i).to.be.equal(validation_transition.body.message[i].order);
            }
        });

        it('DELETE#validation_transition => Should delete validation_transition successfully from the end', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .delete('/yel-ana/validation_transition/transition/' + new_validation_transition.transition + '/validation/' + new_validation_transition.validation)
                        .set('x-access-token', auth.body.token);
                expect(validation_transition).to.have.status(204);
            }catch(err){
                assert(false);
            }

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/' + new_validation_transition.transition)
                        .set('x-access-token', auth.body.token);
                expect(validation_transition).to.have.status(200);
                expect(validation_transition.body.message.length).to.not.be.equal(4);

                //check order sequence
                for(let i = 0; i < validation_transition.body.message.length; i++){
                    expect(validation_transition.body.message[i].validation.name).to.not.be.equal(new_validation_transition.name);
                    expect(i).to.be.equal(validation_transition.body.message[i].order);
                }
            }catch(err){
                console.log(err);
                assert(false, 'An error occurred');
            }
        });

        it('POST#validation_transition => Should successfully insert validation_transition into the last position', async() => {
            let validation_transition;
            new_validation_transition.order = 2;
            try{
                validation_transition =
                    await chai.request(server)
                        .post('/yel-ana/validation_transition')
                        .set('x-access-token', auth.body.token)
                        .send(new_validation_transition);

            }catch(err){
                console.log('Error to post validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.name).to.be.equal(new_validation_transition.name);

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message[2].name).to.be.equal(new_validation_transition.name);
            //check order sequence
            for(let i = 0; i < validation_transition.body.message.length; i++){
                expect(i).to.be.equal(validation_transition.body.message[i].order);
            }
        });

        it('PATCH#validation_transition/order => Should successfully change order to first', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .patch('/yel-ana/validation_transition/transition/' + basic_fixtures.transition_active_inactive.object._id + '/validation/' + new_validation_transition.validation + '/order/0')
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to patch validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.name).to.be.equal(new_validation_transition.name);
            expect(validation_transition.body.message.order).to.be.equal('0');

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message[0].name).to.be.equal(new_validation_transition.name);
            //check order sequence
            for(let i = 0; i < validation_transition.body.message.length; i++){
                expect(i).to.be.equal(validation_transition.body.message[i].order);
            }
        });

        it('PATCH#validation_transition/order => Should successfully change order to last', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .patch('/yel-ana/validation_transition/transition/' + basic_fixtures.transition_active_inactive.object._id + '/validation/' + new_validation_transition.validation + '/order/3')
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to patch validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.name).to.be.equal(new_validation_transition.name);
            expect(validation_transition.body.message.order).to.be.equal('3');

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message[3].name).to.be.equal(new_validation_transition.name);
            //check order sequence
            for(let i = 0; i < validation_transition.body.message.length; i++){
                expect(i).to.be.equal(validation_transition.body.message[i].order);
            }
        });

        it('PATCH#validation_transition/order => Should successfully change order to second', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .patch('/yel-ana/validation_transition/transition/' + basic_fixtures.transition_active_inactive.object._id + '/validation/' + new_validation_transition.validation + '/order/1')
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to patch validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.name).to.be.equal(new_validation_transition.name);
            expect(validation_transition.body.message.order).to.be.equal('1');

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message[1].name).to.be.equal(new_validation_transition.name);
            //check order sequence
            for(let i = 0; i < validation_transition.body.message.length; i++){
                expect(i).to.be.equal(validation_transition.body.message[i].order);
            }
        });

        it('PATCH#validation_transition/order => Should successfully change order to last if order received is too high', async() => {
            let validation_transition;
            try{
                validation_transition =
                    await chai.request(server)
                        .patch('/yel-ana/validation_transition/transition/' + basic_fixtures.transition_active_inactive.object._id + '/validation/' + new_validation_transition.validation + '/order/15')
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to patch validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message.name).to.be.equal(new_validation_transition.name);
            expect(validation_transition.body.message.order).to.be.equal(3);

            try{
                validation_transition =
                    await chai.request(server)
                        .get('/yel-ana/validation_transition/transition/'+basic_fixtures.transition_active_inactive.object._id)
                        .set('x-access-token', auth.body.token);

            }catch(err){
                console.log('Error to get validation_transition => ', err);
            }
            expect(validation_transition).to.have.status(200);
            expect(validation_transition.body.message[3].name).to.be.equal(new_validation_transition.name);
            //check order sequence
            for(let i = 0; i < validation_transition.body.message.length; i++){
                expect(i).to.be.equal(validation_transition.body.message[i].order);
            }
        });
    });
});