const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const requireDir = require('require-dir');
const models = requireDir('../../models');
const basic_fixtures = require('../fixtures/basic_fixtures');
const authentication_helper = require('../helpers/authentication');
const chaiHttp = require('chai-http');
const server = require('../../app');
const factoryGirlConfig = require('../config/factory-girl-config');
const should = chai.should();

chai.use(chaiHttp);

let auth;
let new_entity;
let no_code_entity;
let no_name_entity;
let no_description_entity;
let wrong_id_entity;
let not_present_entity;

before(async() => {
  auth = await authentication_helper.authenticate(basic_fixtures);
});

describe("Entity", () => {
  describe('GET#entity/:entity_id', () => {
    it('Should return entity with specified id', async() => {
      try{
          let entity =
            await chai.request(server)
                      .get('/yel-ana/entity/'+basic_fixtures.entity.object._id)
                      .set('x-access-token', auth.body.token);

        expect(entity).to.have.status(200);
        assert.equal(basic_fixtures.entity.object.name, entity.body.message.name);

      }catch(err){
        console.log('Error to get entity => ', err);
        assert(false);
      }

    });

    it('Should return 404 when entity is not found', async() => {
      try{
          let status =
            await chai.request(server)
                      .get('/yel-ana/entity/' + '123')
                      .set('x-access-token', auth.body.token);
          expect(status).to.have.status(404);
          expect(status.body.message).to.be.equal('Not found');

      }catch(err){
          assert(false, "An error occurred");
      }
    });
  });

  describe('GET#entity', () => {
    it('Should return all entity with current application id of authenticated user', async() => {
      try{
          let entity =
            await chai.request(server)
                      .get('/yel-ana/entity/')
                      .set('x-access-token', auth.body.token);

        expect(entity).to.have.status(200);
        expect(entity.body.message).to.not.be.empty;
        expect(entity.body.message[0].name).to.be.equal(basic_fixtures.entity.object.name);

      }catch(err){
        console.log('Error to get entity => ', err);
        assert(false);
      }
    });
  });

  describe('New Entity Services', () => {
    before(async() => {
      new_entity = await factoryGirlConfig.factoryGirl.build('new_entity',{application: basic_fixtures.application.object._id});
      no_code_entity = JSON.parse(JSON.stringify(new_entity));
      delete no_code_entity.code;
      no_name_entity = JSON.parse(JSON.stringify(new_entity));
      delete no_name_entity.name;
      no_description_entity = JSON.parse(JSON.stringify(new_entity));
      delete no_description_entity.description;
      wrong_id_entity = JSON.parse(JSON.stringify(new_entity));
      wrong_id_entity._id = 'aaa';
      not_present_entity = JSON.parse(JSON.stringify(new_entity));
      not_present_entity._id = require('mongoose').Types.ObjectId();
    });

    it('POST#entity => Should return 400 in case of trying to create without mandatory params', async() => {
      let entity;
      try{
        entity =
            await chai.request(server)
                      .post('/yel-ana/entity')
                      .set('x-access-token', auth.body.token)
                      .send(no_code_entity);
          expect(entity).to.have.status(400);
          expect(entity.body.message).to.be.equal('code is mandatory');
      }catch(err){
          assert(false, "An error occurred");
      }

      try{
        entity =
            await chai.request(server)
                      .post('/yel-ana/entity')
                      .set('x-access-token', auth.body.token)
                      .send(no_name_entity);
        expect(entity).to.have.status(400);
        expect(entity.body.message).to.be.equal('name is mandatory');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('POST#entity => Should successfully insert entity', async() => {
        let entity;
        try{
          entity =
              await chai.request(server)
                        .post('/yel-ana/entity')
                        .set('x-access-token', auth.body.token)
                        .send(new_entity);

        }catch(err){
          console.log('Error to post entity => ', err);
        }
        expect(entity).to.have.status(204);

        try{
          entity =
              await chai.request(server)
                      .get('/yel-ana/entity/')
                      .set('x-access-token', auth.body.token);

        }catch(err){
          console.log('Error to get entity => ', err);
        }

        expect(entity).to.have.status(200);
        expect(entity.body.message[1].name).to.be.equal(new_entity.name);
        new_entity._id = entity.body.message[1]._id;
    });

    it('PUT#entity => Should return 400 in case of trying to update without mandatory params', async() => {
      let entity;
      let no_id_entity = JSON.parse(JSON.stringify(new_entity));
      delete no_id_entity._id;

      try{
        entity =
            await chai.request(server)
                      .put('/yel-ana/entity')
                      .set('x-access-token', auth.body.token)
                      .send(no_id_entity);
        expect(entity).to.have.status(400);
        expect(entity.body.message).to.be.equal('_id is mandatory');
      }catch(err){
        assert(false,'An error occurred');
      }

      try{
        entity =
            await chai.request(server)
                      .put('/yel-ana/entity')
                      .set('x-access-token', auth.body.token)
                      .send(no_code_entity);
        expect(entity).to.have.status(400);
        expect(entity.body.message).to.be.equal('code is mandatory');
      }catch(err){
          assert(false,'An error occurred');
      }

      try{
        entity =
            await chai.request(server)
                      .put('/yel-ana/entity')
                      .set('x-access-token', auth.body.token)
                      .send(no_name_entity);
         expect(entity).to.have.status(400);
         expect(entity.body.message).to.be.equal('name is mandatory');
      }catch(err){
          assert(false,'An error occurred');
      }
    });

    it('PUT#entity => Should return 400 in case of trying to update with id wrongly formatted', async() => {
      let entity;
      try{
        entity =
            await chai.request(server)
                      .put('/yel-ana/entity')
                      .set('x-access-token', auth.body.token)
                      .send(wrong_id_entity);
         expect(entity).to.have.status(400);
         expect(entity.body.message).to.be.equal('_id provided is invalid');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('PUT#entity => Should return 404 in case of trying to update with id not present yet', async() => {
      let entity;
      try{
        entity =
            await chai.request(server)
                      .put('/yel-ana/entity')
                      .set('x-access-token', auth.body.token)
                      .send(not_present_entity);

         expect(entity).to.have.status(404);
         expect(entity.body.message).to.be.equal('Not found');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('PUT#entity => Should successfully update entity', async() => {
        let entity;
        let new_name = "abcdef";
        let new_code = "aaa";
        let new_description = "aaa aaa";
        new_entity.name = new_name;
        new_entity.code = new_code;
        new_entity.description = new_description;

        try{
          entity =
              await chai.request(server)
                        .put('/yel-ana/entity')
                        .set('x-access-token', auth.body.token)
                        .send(new_entity);

        }catch(err){
          console.log('Error to put entity => ', err);
        }
        expect(entity).to.have.status(204);

        try{
          entity =
              await chai.request(server)
                      .get('/yel-ana/entity/')
                      .set('x-access-token', auth.body.token);

        }catch(err){
          console.log('Error to get entity => ', err);
        }

        expect(entity).to.have.status(200);
        expect(entity.body.message[1].name).to.be.equal(new_name);
        expect(entity.body.message[1].code).to.be.equal(new_code);
        expect(entity.body.message[1].description).to.be.equal(new_description);
    });

    it('DELETE#entity => Should return 404 when entity is not found', async() => {
      try{
          let entity =
            await chai.request(server)
                      .delete('/yel-ana/entity/' + require('mongoose').Types.ObjectId())
                      .set('x-access-token', auth.body.token);

        expect(entity).to.have.status(404);
      }catch(err){
        assert(false, 'An error occurred');
      }
    });

    it('DELETE#entity => Should delete entity successfully', async() => {
      let entity;
      try{
        entity =
            await chai.request(server)
                      .delete('/yel-ana/entity/' + new_entity._id)
                      .set('x-access-token', auth.body.token);
        expect(entity).to.have.status(204);
      }catch(err){
        console.log('err to delete entity => ',err);
        assert(false);
      }

      try{
        entity =
            await chai.request(server)
                      .get('/yel-ana/entity/' + new_entity._id)
                      .set('x-access-token', auth.body.token);
        expect(entity).to.have.status(404);
        expect(entity.body.message).to.be.equal('Not found');
      }catch(err){
        assert(false, 'An error occurred');
      }
    });
  });
});
