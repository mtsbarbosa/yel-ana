const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const Condition = require('../../models/condition').model();

factory.define('condition', Condition, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('new_condition', Condition, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('condition_linked_1', Condition, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('condition_linked_2', Condition, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('condition_linked_3', Condition, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('new_condition_linked', Condition, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('condition_w_exp_1', Condition, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: factory.chance('sentence',{words: 4}),
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});
