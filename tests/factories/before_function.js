const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const BeforeFunction = require('../../models/before_function').model();

factory.define('before_function', BeforeFunction, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: ' ',
    entity: factory.assoc('entity','_id'),
    application: factory.assoc('application','_id')
});
