const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const Workflow = require('../../models/workflow').model();

factory.define('workflow', Workflow, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('new_workflow', Workflow, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('not_root_workflow', Workflow, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    entity: factory.assoc('not_root_entity', '_id'),
    application: factory.assoc('not_root_application','_id')
});
