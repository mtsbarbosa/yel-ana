const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const ValidationTransition = require('../../models/validation_transition').model();

factory.define('validation_transition', ValidationTransition, {
    validation: factory.assoc('validation','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ValidationTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('new_validation_transition', ValidationTransition, {
    validation: factory.assoc('validation','_id'),
    transition: factory.assoc('transition','_id'),
    application: factory.assoc('application','_id')
});

factory.define('validation_w_exp_transition_1', ValidationTransition, {
    validation: factory.assoc('validation','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ValidationTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('validation_w_exp_transition_2', ValidationTransition, {
    validation: factory.assoc('validation','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ValidationTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('validation_w_exp_initial_1', ValidationTransition, {
    validation: factory.assoc('validation','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ValidationTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('validation_w_exp_initial_2', ValidationTransition, {
    validation: factory.assoc('validation','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ValidationTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});