const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const BeforeFunctionTransition = require('../../models/before_function_transition').model();

factory.define('before_function_transition', BeforeFunctionTransition, {
    _id: require('mongoose').Types.ObjectId(),
    before_function: factory.assoc('before_function','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('BeforeFunctionTransition.order', (n) => n),
    application: factory.assoc('application','_id')
});
