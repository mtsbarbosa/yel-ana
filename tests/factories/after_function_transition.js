const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const AfterFunctionTransition = require('../../models/after_function_transition').model();

factory.define('after_function_transition', AfterFunctionTransition, {
    _id: require('mongoose').Types.ObjectId(),
    after_function: factory.assoc('after_function','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('AfterFunctionTransition.order', (n) => n),
    application: factory.assoc('application','_id')
});
