const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const Validation = require('../../models/validation').model();

factory.define('validation', Validation, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('new_validation', Validation, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('validation_linked_1', Validation, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('validation_linked_2', Validation, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('validation_linked_3', Validation, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('new_validation_linked', Validation, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: '1 == 1',
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('validation_w_exp_1', Validation, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    expression: factory.chance('sentence',{words: 4}),
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});
