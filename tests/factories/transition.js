const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const Transition = require('../../models/transition').model();

factory.define('transition_initial', Transition, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_to: factory.assoc('status_active','_id'),
    application: factory.assoc('application','_id')
});

factory.define('conditional_transition_initial_1', Transition, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_to: factory.assoc('status_active','_id'),
    application: factory.assoc('application','_id')
});

factory.define('conditional_transition_initial_2', Transition, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_to: factory.assoc('status_active','_id'),
    application: factory.assoc('application','_id')
});

factory.define('conditional_transition_initial_3', Transition, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_to: factory.assoc('status_active','_id'),
    application: factory.assoc('application','_id')
});

factory.define('transition_active_inactive', Transition, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_from: factory.assoc('status_active','_id'),
    status_to: factory.assoc('status_inactive','_id'),
    application: factory.assoc('application','_id')
});

factory.define('transition_inactive_active', Transition, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_from: factory.assoc('status_inactive','_id'),
    status_to: factory.assoc('status_active','_id'),
    application: factory.assoc('application','_id')
});

factory.define('new_transition', Transition, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_to: factory.assoc('status_active','_id'),
    application: factory.assoc('application','_id')
});

factory.define('not_root_transition', Transition, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('not_root_workflow', '_id'),
    status_to: factory.assoc('not_root_status','_id'),
    application: factory.assoc('not_root_application','_id')
});

factory.define('conditional_transition_1', Transition, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_from: factory.assoc('status_active','_id'),
    status_to: factory.assoc('status_inactive','_id'),
    application: factory.assoc('application','_id')
});

factory.define('conditional_transition_2', Transition, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_from: factory.assoc('status_active','_id'),
    status_to: factory.assoc('status_inactive','_id'),
    application: factory.assoc('application','_id')
});

factory.define('conditional_transition_3', Transition, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    workflow: factory.assoc('workflow', '_id'),
    status_from: factory.assoc('status_active','_id'),
    status_to: factory.assoc('status_inactive','_id'),
    application: factory.assoc('application','_id')
});
