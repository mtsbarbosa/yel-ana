const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const Entity = require('../../models/entity').model();

factory.define('entity', Entity, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    application: factory.assoc('application','_id')
});

factory.define('new_entity', Entity, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    application: factory.assoc('application','_id')
});

factory.define('not_root_entity', Entity, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    application: factory.assoc('not_root_application','_id')
});
