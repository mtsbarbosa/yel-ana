const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const ConditionTransition = require('../../models/condition_transition').model();

factory.define('condition_transition', ConditionTransition, {
    condition: factory.assoc('condition','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ConditionTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('new_condition_transition', ConditionTransition, {
    condition: factory.assoc('condition','_id'),
    transition: factory.assoc('transition','_id'),
    application: factory.assoc('application','_id')
});

factory.define('condition_w_exp_transition_1', ConditionTransition, {
    condition: factory.assoc('condition','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ConditionTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('condition_w_exp_transition_2', ConditionTransition, {
    condition: factory.assoc('condition','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ConditionTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('condition_w_exp_transition_3', ConditionTransition, {
    condition: factory.assoc('condition','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ConditionTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('condition_w_exp_initial_1', ConditionTransition, {
    condition: factory.assoc('condition','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ConditionTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('condition_w_exp_initial_2', ConditionTransition, {
    condition: factory.assoc('condition','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ConditionTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});

factory.define('condition_w_exp_initial_3', ConditionTransition, {
    condition: factory.assoc('condition','_id'),
    transition: factory.assoc('transition','_id'),
    order: factory.sequence('ConditionTransition.order', (n) => n - 1),
    application: factory.assoc('application','_id')
});
