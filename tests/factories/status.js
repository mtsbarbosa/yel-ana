const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const Status = require('../../models/status').model();

factory.define('status_active', Status, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('status_inactive', Status, {
    _id: require('mongoose').Types.ObjectId(),
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('new_status', Status, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    entity: factory.assoc('entity', '_id'),
    application: factory.assoc('application','_id')
});

factory.define('not_root_status', Status, {
    code: factory.chance('word',{syllables: 3}),
    name: factory.chance('word',{syllables: 3}),
    description: factory.chance('sentence',{words: 8}),
    entity: factory.assoc('not_root_entity', '_id'),
    application: factory.assoc('not_root_application','_id')
});
