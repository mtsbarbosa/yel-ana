const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const User = require('../../models/user').model();

factory.define('user', User, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('name'),
    email: factory.chance('email'),
    password: factory.chance('string',{length: 10}),
    blocked: false,
});

factory.define('other_user', User, {
    _id: require('mongoose').Types.ObjectId(),
    name: factory.chance('name'),
    email: factory.chance('email'),
    password: factory.chance('string',{length: 10}),
    blocked: false,
});

factory.define('new_user', User, {
    name: factory.chance('name'),
    email: factory.chance('email'),
    password: factory.chance('string',{length: 10}),
});
