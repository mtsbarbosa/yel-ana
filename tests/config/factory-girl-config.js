const FactoryGirl = require('factory-girl');
const factory = FactoryGirl.factory;
const adapter = new FactoryGirl.MongooseAdapter();

// use the mongoose adapter as the default adapter
factory.setAdapter(adapter);

let path = require('path');
let fs = require('fs');
let requireDir = require('require-dir');

module.exports = {
    loadFactories: async(self) => {
      let _this = self;
      _this.config = require('../../config');
      _this.db = require('../../db');
      _this.db.connection = await _this.db.initialize();
      _this.factories = requireDir('../factories');
    },
    factories: null,
    factoryGirl: factory,
    config: null,
    db: null
};
