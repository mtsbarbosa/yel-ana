const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../app');

chai.use(chaiHttp);

class AuthenticationHelper {
    async authenticate(basic_fixtures){
        try {
            const auth =
                await chai.request(server)
                    .post('/yel-ana/authenticate')
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .send({name: basic_fixtures.application.object.name, hash_key: basic_fixtures.application.object.hash_key});
            return auth;
        } catch (err) {
            console.log('Error to authenticate => ', err);
        }
    }
    async authenticate_not_root(basic_fixtures){
        try {
            const auth =
                await chai.request(server)
                    .post('/yel-ana/authenticate')
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .send({name: basic_fixtures.not_root_application.object.name, hash_key: basic_fixtures.not_root_application.object.hash_key});
            return auth;
        } catch (err) {
            console.log('Error to authenticate not root => ', err);
        }
    }
    async authenticate_app_by_user_token(basic_fixtures, application_id){
        try {
            const userAuth = await this.authenticate_user(basic_fixtures);
            const auth =
                await chai.request(server)
                    .post('/yel-ana/authenticate/id/' + application_id)
                    .set('x-user-token', userAuth.body.token)
                    .send({});
            return auth;
        } catch (err) {
            console.log('Error to authenticate => ', err);
        }
    }
    async authenticate_user(basic_fixtures){
        try {
            const auth =
                await chai.request(server)
                    .post('/yel-ana/user/authenticate')
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .send({email: basic_fixtures.user.object.email, password: basic_fixtures.user.object.password});
            return auth;
        } catch (err) {
            console.log('Error to authenticate user => ', err);
        }
    }
}

module.exports = new AuthenticationHelper();
