const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const ExpressionConditionalOptionsVO = require('../../vo/expression_conditional_options');
const expressionHelper = require('../../helpers/expression');
const chaiHttp = require('chai-http');
const server = require('../../app');

chai.use(chaiHttp);


describe('Expression Helper', () => {
    describe('#compileBoolean req usage', () => {
        it('custom data must persist data', async() => {
            try{
                let response =
                    await chai.request(server)
                        .post('/yel-ana/test/custom_data')
                        .send({is_saved: true});

                expect(response).to.have.status(200);

                response =
                    await chai.request(server)
                        .get('/yel-ana/test/custom_data');

                expect(response).to.have.status(200);
                assert.isTrue(response.body.message.is_saved);

            }catch(err){
                console.log('Error to get expression => ', err);
                assert(false);
            }
        });

        it('compile should get value from req method', async() => {
            try{
                let conditional = new ExpressionConditionalOptionsVO({id:"234rfgt5rdgfgr"},{id:"234rfgt5workflow"},{id:"234rfgt5transition"},'12345',{id:"234rfgt5cond"});
                assert.isTrue(await expressionHelper.compileBoolean('status of req("GET","http://localhost:3000/yel-ana/test/custom_data") == 200',conditional));
                assert.isTrue(await expressionHelper.compileBoolean('is_saved of message of data of req("GET","http://localhost:3000/yel-ana/test/custom_data")',conditional));

                let response =
                    await chai.request(server)
                        .post('/yel-ana/test/custom_data')
                        .send({is_saved: false});

                expect(response).to.have.status(200);

                assert.isFalse(await expressionHelper.compileBoolean('is_saved of message of data of req("GET","http://localhost:3000/yel-ana/test/custom_data")',conditional));
                await expressionHelper.compile('req("POST","http://localhost:3000/yel-ana/test/custom_data",{},{\"is_saved\": true})',conditional);
                assert.isTrue(await expressionHelper.compileBoolean('is_saved of message of data of req("GET","http://localhost:3000/yel-ana/test/custom_data")',conditional));
                await expressionHelper.compile('req("POST","http://localhost:3000/yel-ana/test/custom_data",{\"header_present\": true},{\"is_saved\": true})',conditional);
                assert.isTrue(await expressionHelper.compileBoolean('header_present of headers of req("GET","http://localhost:3000/yel-ana/test/custom_data")',conditional));
            }catch(err){
                console.log('error to get value from req method in expression => ', err);
                assert(false);
            }
        });
    });
});
