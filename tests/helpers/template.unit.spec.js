const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const template_helper = require('../../helpers/template');
const should = chai.should();

describe('Template Helper', () => {
    describe('#renderTemplate()', () => {
        it('should convert with properties set', async() => {
            const rendered = await template_helper.renderTemplate('email/user_activation/html.pug',{name: 'Mr. Catra', link: 'http://pudim.com.br'});
            expect(rendered).to.be.equal('<p>Hi Mr. Catra,</p><p>Welcome to Yel-Ana</p><br/><p>In order to complete your user activation, please click on the link bellow:</p><p><a href="http://pudim.com.br">Activate my user</a></p><br/><br/><br/><br/><p><small>© Yel-Ana by Major Cluster Co-op 2019</small><br/><small>yel.ana.master@gmail.com</small><br/><small>Prague, Czech Republic</small></p>');
        });
    });
});