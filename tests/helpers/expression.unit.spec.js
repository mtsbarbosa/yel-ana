const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const ExpressionConditionalOptionsVO = require('../../vo/expression_conditional_options');
const expressionHelper = require('../../helpers/expression');
const should = chai.should();


describe('Expression Helper', () => {
    describe('#compileBoolean()', () => {
        it('check must work for all objects', async() => {
            let conditional = new ExpressionConditionalOptionsVO({id:"234rfgt5rdgfgr"},{id:"234rfgt5workflow"},{id:"234rfgt5transition"},'12345',{id:"234rfgt5cond"});
            assert.isTrue(await expressionHelper.compileBoolean('id of status_from == "234rfgt5rdgfgr"',conditional));
            assert.isFalse(await expressionHelper.compileBoolean('id of status_from == "234rfgt5rdgfg2"',conditional));

            assert.isTrue(await expressionHelper.compileBoolean('id of workflow == "234rfgt5workflow"',conditional));
            assert.isFalse(await expressionHelper.compileBoolean('id of workflow != "234rfgt5workflow"',conditional));

            assert.isTrue(await expressionHelper.compileBoolean('object_id == "12345"',conditional));
            assert.isFalse(await expressionHelper.compileBoolean('object_id == "123456"',conditional));

            assert.isTrue(await expressionHelper.compileBoolean('id of condition == "234rfgt5cond"',conditional));
            assert.isFalse(await expressionHelper.compileBoolean('id of condition != "234rfgt5cond"',conditional));

            conditional = new ExpressionConditionalOptionsVO({id:"234rfgt5rdgfgr"},{id:"234rfgt5workflow"},{id:"234rfgt5transition"},'12345',undefined,{id:"234rfgt5val"});

            assert.isTrue(await expressionHelper.compileBoolean('id of validation == "234rfgt5val"',conditional));
            assert.isFalse(await expressionHelper.compileBoolean('id of validation == "234rfgt5val2"',conditional));
        });
    });
});
