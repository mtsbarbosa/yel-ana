const express = require('express');
const Workflow = require('../models/workflow');
const mongoose = require('mongoose');
let error = require('../helpers/error');

class WorkflowController {
  async findOne(workflow_id, application_id) {
    if (!mongoose.Types.ObjectId.isValid(workflow_id)) {
      return null;
    }
    if (!mongoose.Types.ObjectId.isValid(application_id)) {
      return null;
    }
    return await Workflow
    .model()
    .findOne({_id: workflow_id, application: application_id})
    .select('_id code name description entity');
  }

  async findByEntity(entity_id, application_id) {
    if (!mongoose.Types.ObjectId.isValid(entity_id)) {
      return null;
    }
    if (!mongoose.Types.ObjectId.isValid(application_id)) {
      return null;
    }
    return await Workflow
    .model()
    .find({entity: entity_id, application: application_id})
    .select('_id code name description entity');
  }

  async save(workflow) {
    return await Workflow.model().create(workflow);
  }

  async update(workflow) {
    if (!mongoose.Types.ObjectId.isValid(workflow._id)) {
      throw new error.IdFormatError('_id provided is invalid');
    }
    let workflow_to_update = await this.findOne(workflow._id,
        workflow.application);
    if (workflow_to_update == null) {
      throw new error.NotFoundError('Not found');
    }

    workflow_to_update = Object.assign(workflow_to_update, workflow);
    return await workflow_to_update.save();
  }

  async delete(workflow_id, application_id) {
    if (!mongoose.Types.ObjectId.isValid(workflow_id)) {
      throw new error.IdFormatError('workflow_id provided is invalid');
    }
    if (!mongoose.Types.ObjectId.isValid(application_id)) {
      throw new error.IdFormatError('application_id provided is invalid');
    }
    const workflow_to_delete = await this.findOne(workflow_id,
        application_id);
    if (workflow_to_delete == null) {
      throw new error.NotFoundError('Not found');
    }

    await Workflow
    .model()
    .deleteOne({_id: workflow_id, application: application_id});
  }
}

module.exports = new WorkflowController();
