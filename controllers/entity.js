const express = require('express');
const Entity = require('../models/entity');
const mongoose = require('mongoose');
const error = require('../helpers/error');

class EntityController {
    async findOne(entity_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(entity_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Entity
        .model()
        .findOne({_id: entity_id})
        .select('_id code name description');
    }

    async findByApplication(application_id) {
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Entity
        .model()
        .find({application: application_id})
        .select('_id code name description');
    }

    async save(entity) {
        return await Entity.model().create(entity);
    }

    async update(entity) {
        if (!mongoose.Types.ObjectId.isValid(entity._id)) {
            throw new error.IdFormatError('_id provided is invalid');
        }
        let entity_to_update = await this.findOne(entity._id,
            entity.application);
        if (entity_to_update == null) {
            throw new error.NotFoundError('Not found');
        }

        entity_to_update = Object.assign(entity_to_update, entity);
        return await entity_to_update.save();
    }

    async delete(entity_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(entity_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('entity_id provided is invalid');
        }
        const entity_to_delete = await this.findOne(entity_id, application_id);
        if (entity_to_delete == null) {
            throw new error.NotFoundError('Not found');
        }

        await Entity
        .model()
        .deleteOne({_id: entity_id});
    }
}

module.exports = new EntityController();
