const express = require('express');
const Status = require('../models/status');
const mongoose = require('mongoose');
const error = require('../helpers/error');

class StatusController {
    async findOne(status_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(status_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Status
        .model()
        .findOne({_id: status_id, application: application_id})
        .select('_id code name description entity');
    }

    async findByEntity(entity_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(entity_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Status
        .model()
        .find({entity: entity_id, application: application_id})
        .select('_id code name description entity');
    }

    async save(status) {
        return await Status.model().create(status);
    }

    async update(status) {
        if (!mongoose.Types.ObjectId.isValid(status._id)) {
            throw new error.IdFormatError('_id provided is invalid');
        }
        let status_to_update = await this.findOne(status._id,
            status.application);
        if (status_to_update == null) {
            throw new error.NotFoundError('Not found');
        }

        status_to_update = Object.assign(status_to_update, status);
        return await status_to_update.save();
    }

    async delete(status_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(status_id)) {
            throw new error.IdFormatError('status_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        const status_to_delete = await this.findOne(status_id, application_id);
        if (status_to_delete == null) {
            throw new error.NotFoundError('Not found');
        }

        await Status
        .model()
        .deleteOne({_id: status_id, application: application_id});
    }
}

module.exports = new StatusController();
