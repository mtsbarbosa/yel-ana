const mongoose = require('mongoose');
const Transition = require('../models/transition');

class TransitionCoreController {
    async findOne(transition_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(transition_id)) {
            return null;
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Transition
        .model()
        .findOne({_id: transition_id, application: application_id})
        .select('_id code name description status_from status_to workflow');
    }
}

module.exports = new TransitionCoreController();