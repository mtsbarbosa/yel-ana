const express = require('express');
const Application = require('../models/application');
const bcrypt = require('bcryptjs');
const config = require('../config');
const mongoose = require('mongoose');
const error = require('../helpers/error');

class ApplicationController {
    async findByExample(application) {
        /*bcrypt.hash("hash_to_discover", config.bcrypt.saltRounds).then(function(hash) {
            console.log(hash);
        });*/
        return await Application
        .model()
        .findOne(application);
    }

    async findOne(application_id, user_id) {
        if (!mongoose.Types.ObjectId.isValid(user_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Application
        .model()
        .findOne({_id: application_id, user: user_id})
        .select('_id name description hash_key root user');
    }

    async findByUser(user_id) {
        if (!mongoose.Types.ObjectId.isValid(user_id)) {
            return null;
        }
        return await Application
        .model()
        .find({user: user_id})
        .select('_id name description');
    }

    async save(application) {
        const hash = await bcrypt.hash(application.hash_key,
            config.bcrypt.saltRounds);
        application.hash_key = hash;
        if (typeof (application.root) === 'undefined') {
            application.root = false;
        }
        application = await Application.model().create(application);
        let new_app = JSON.parse(JSON.stringify(application));
        delete new_app.hash_key;
        return new_app;
    }

    async update(application) {
        if (!mongoose.Types.ObjectId.isValid(application._id)) {
            throw new error.IdFormatError('_id provided is invalid');
        }
        let application_to_update = await this.findOne(application._id,
            application.user);
        if (application_to_update == null) {
            throw new error.NotFoundError('Not found');
        }

        application_to_update = Object.assign(application_to_update,
            application);
        return await application_to_update.save();
    }

    async delete(application_id, user_id) {
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(user_id)) {
            throw new error.IdFormatError('user_id provided is invalid');
        }
        let application_to_delete = await this.findOne(application_id, user_id);
        if (application_to_delete == null) {
            throw new error.NotFoundError('Not found');
        }

        await Application
        .model()
        .deleteOne({_id: application_id, user: user_id});
    }
}

module.exports = new ApplicationController();
