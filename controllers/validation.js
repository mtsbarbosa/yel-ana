const ExpressionConditionalOptionsVO = require(
    '../vo/expression_conditional_options');
const express = require('express');
const Validation = require('../models/validation');
const ValidationTransitionVO = require('../vo/validation_transition');
const ValidationTransition = require('../models/validation_transition');
const mongoose = require('mongoose');
const error = require('../helpers/error');
const array_helper = require('../helpers/array_helper');
const expression_helper = require('../helpers/expression');
const tcore_controller = require('./transition_core');

class ValidationController {
    async findOne(validation_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(validation_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Validation
        .model()
        .findOne({_id: validation_id, application: application_id})
        .select('_id name description expression order entity');
    }

    async findByEntity(entity_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(entity_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Validation
        .model()
        .find({entity: entity_id, application: application_id})
        .select('_id name description expression order entity');
    }

    async findByTransition(transition_id, application_id, fillMongoose) {
        if (!mongoose.Types.ObjectId.isValid(transition_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        let validationTransitionModels = await ValidationTransition
        .model()
        .find({transition: transition_id, application: application_id})
        .sort({order: 'asc'})
        .select('_id validation transition expression order');
        if (validationTransitionModels == null
            || validationTransitionModels.length == 0) {
            return null;
        }
        let validationTransitionVOs = [];
        await array_helper.asyncForEach(validationTransitionModels,
            async (validationTransitionModel) => {
                let validation = await this.findOne(
                    validationTransitionModel.validation, application_id);
                if (validation != null) {
                    if (fillMongoose) {
                        validationTransitionVOs.push(new ValidationTransitionVO(
                            validationTransitionModel._id,
                            validation,
                            validationTransitionModel.transition,
                            validationTransitionModel.expression,
                            validationTransitionModel.order,
                            validationTransitionModel
                        ));
                    } else {
                        validationTransitionVOs.push(new ValidationTransitionVO(
                            validationTransitionModel._id,
                            validation,
                            validationTransitionModel.transition,
                            validationTransitionModel.expression,
                            validationTransitionModel.order
                        ));
                    }
                }
            });
        return validationTransitionVOs;
    }

    async save(validation) {
        return await Validation.model().create(validation);
    }

    async update(validation) {
        if (!mongoose.Types.ObjectId.isValid(validation._id)) {
            throw new error.IdFormatError('_id provided is invalid');
        }
        let validation_to_update = await this.findOne(validation._id,
            validation.application);
        if (validation_to_update == null) {
            throw new error.NotFoundError('Not found');
        }

        validation_to_update = Object.assign(validation_to_update, validation);
        return await validation_to_update.save();
    }

    async delete(validation_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(validation_id)) {
            throw new error.IdFormatError('validation_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        let validation_to_delete = await this.findOne(validation_id,
            application_id);
        if (validation_to_delete == null) {
            throw new error.NotFoundError('Not found');
        }

        await Validation
        .model()
        .deleteOne({_id: validation_id, application: application_id});
    }

    async linkToTransition(toLink) {
        let validation_id = toLink.validation;
        let transition_id = toLink.transition;
        let application_id = toLink.application.toString();
        let order = toLink.order;

        if (!mongoose.Types.ObjectId.isValid(validation_id)) {
            throw new error.IdFormatError('validation_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(transition_id)) {
            throw new error.IdFormatError('transition_id provided is invalid');
        }
        let validation_to_link = await this.findOne(validation_id,
            application_id);
        if (validation_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let transition_to_link = await tcore_controller.findOne(transition_id,
            application_id);
        if (transition_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        if (!order) {
            const found = (await this.findByTransition(transition_id,
                application_id));
            let order = found === null ? 0 : found.length;
            return await ValidationTransition.model().create({
                validation: validation_id,
                transition: transition_id,
                order: order,
                application: application_id
            });
        }
        let validationTransitions = await this.findByTransition(transition_id,
            application_id, true);
        if (!validationTransitions || validationTransitions.length == 0) {
            return await ValidationTransition.model().create({
                validation: validation_id,
                transition: transition_id,
                order: 0,
                application: application_id
            });
        } else if (validationTransitions.filter(
            ct => ct.validation._id.toString() === validation_id).length > 0) {
            throw new error.AlreadyLinkedError(
                'validation_id is already linked');
        } else if (order >= validationTransitions.length) {
            return await ValidationTransition.model().create({
                validation: validation_id,
                transition: transition_id,
                order: validationTransitions.length,
                application: application_id
            });
        } else {
            for (let i = (validationTransitions.length - 1); i >= order; i--) {
                let validationTransitionToUpdate = Object.assign(
                    validationTransitions[i].validationTransition,
                    {order: validationTransitions[i].order + 1});
                await validationTransitionToUpdate.save();
            }
            return await ValidationTransition.model().create({
                validation: validation_id,
                transition: transition_id,
                order: order,
                application: application_id
            });
        }
    }

    async deleteLinkToTransition(validation_id, transition_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(validation_id)) {
            throw new error.IdFormatError('validation_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(transition_id)) {
            throw new error.IdFormatError('transition_id provided is invalid');
        }
        let validation_to_link = await this.findOne(validation_id,
            application_id);
        if (validation_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let transition_to_link = await tcore_controller.findOne(transition_id,
            application_id);
        if (transition_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let validationTransitions = await this.findByTransition(transition_id,
            application_id, true);
        let filteredByValidation = validationTransitions.filter(
            ct => ct.validation._id.toString() === validation_id);
        if (!filteredByValidation || filteredByValidation.length === 0) {
            throw new error.NotFoundError('Not found');
        }
        const order = filteredByValidation[0].order;
        await ValidationTransition
        .model()
        .deleteOne({
            validation: validation_id,
            transition: transition_id,
            application: application_id
        });
        for (let i = order + 1; i < validationTransitions.length; i++) {
            let validationTransitionToUpdate = Object.assign(
                validationTransitions[i].validationTransition,
                {order: validationTransitions[i].order - 1});
            await validationTransitionToUpdate.save();
        }
    }

    async moveLinkToTransitionOrder(validation_id, transition_id, new_order,
        application_id) {
        if (!mongoose.Types.ObjectId.isValid(validation_id)) {
            throw new error.IdFormatError('validation_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(transition_id)) {
            throw new error.IdFormatError('transition_id provided is invalid');
        }
        let validation_to_link = await this.findOne(validation_id,
            application_id);
        if (validation_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let transition_to_link = await tcore_controller.findOne(transition_id,
            application_id);
        if (transition_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let validationTransitions = await this.findByTransition(transition_id,
            application_id, true);
        let filteredByValidation = validationTransitions.filter(
            ct => ct.validation._id.toString() === validation_id);
        if (!filteredByValidation || filteredByValidation.length === 0) {
            throw new error.NotFoundError('Not found');
        }
        const order = filteredByValidation[0].order;
        if (validationTransitions.length === 1) {
            return validationTransitions[0];
        }
        if (new_order > validationTransitions.length) {
            new_order = validationTransitions.length - 1;
        } else if (new_order < 0) {
            new_order = 0;
        }
        let validationTransitionToUpdate = Object.assign(
            validationTransitions[order].validationTransition,
            {order: validationTransitions.length});
        await validationTransitionToUpdate.save();
        validationTransitionToUpdate = Object.assign(
            validationTransitions[new_order].validationTransition,
            {order: order});
        await validationTransitionToUpdate.save();
        validationTransitionToUpdate = Object.assign(
            validationTransitions[order].validationTransition,
            {order: new_order});
        await validationTransitionToUpdate.save();
        delete validationTransitions[order].validationTransition;
        validationTransitions[order].order = new_order;
        return validationTransitions[order];
    }

    async isAllowed(status_from, workflow, transition, object_id, validation) {
        if (!validation.expression) {
            return true;
        }
        let conditional = new ExpressionConditionalOptionsVO(status_from,
            workflow, transition, object_id, null, validation);
        return await expression_helper.compileBoolean(validation.expression,
            conditional);
    }
}

module.exports = new ValidationController();
