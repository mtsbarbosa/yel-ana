const ExpressionConditionalOptionsVO = require(
    '../vo/expression_conditional_options');
const express = require('express');
const Condition = require('../models/condition');
const ConditionTransitionVO = require('../vo/condition_transition');
const ConditionTransition = require('../models/condition_transition');
const mongoose = require('mongoose');
const error = require('../helpers/error');
const array_helper = require('../helpers/array_helper');
const expression_helper = require('../helpers/expression');
const tcore_controller = require('./transition_core');

class ConditionController {
    async findOne(condition_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(condition_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Condition
        .model()
        .findOne({_id: condition_id, application: application_id})
        .select('_id name description expression order entity');
    }

    async findByEntity(entity_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(entity_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Condition
        .model()
        .find({entity: entity_id, application: application_id})
        .select('_id name description expression order entity');
    }

    async findByTransition(transition_id, application_id, fillMongoose) {
        if (!mongoose.Types.ObjectId.isValid(transition_id)
            || !mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        let conditionTransitionModels = await ConditionTransition
        .model()
        .find({transition: transition_id, application: application_id})
        .sort({order: 'asc'})
        .select('_id condition transition expression order');
        if (conditionTransitionModels == null
            || conditionTransitionModels.length == 0) {
            return null;
        }
        let conditionTransitionVOs = [];
        await array_helper.asyncForEach(conditionTransitionModels,
            async (conditionTransitionModel) => {
                let condition = await this.findOne(
                    conditionTransitionModel.condition, application_id);
                if (condition != null) {
                    if (fillMongoose) {
                        conditionTransitionVOs.push(new ConditionTransitionVO(
                            conditionTransitionModel._id,
                            condition,
                            conditionTransitionModel.transition,
                            conditionTransitionModel.expression,
                            conditionTransitionModel.order,
                            conditionTransitionModel
                        ));
                    } else {
                        conditionTransitionVOs.push(new ConditionTransitionVO(
                            conditionTransitionModel._id,
                            condition,
                            conditionTransitionModel.transition,
                            conditionTransitionModel.expression,
                            conditionTransitionModel.order
                        ));
                    }
                }
            });
        return conditionTransitionVOs;
    }

    async save(condition) {
        return await Condition.model().create(condition);
    }

    async update(condition) {
        if (!mongoose.Types.ObjectId.isValid(condition._id)) {
            throw new error.IdFormatError('_id provided is invalid');
        }
        let condition_to_update = await this.findOne(condition._id,
            condition.application);
        if (condition_to_update == null) {
            throw new error.NotFoundError('Not found');
        }

        condition_to_update = Object.assign(condition_to_update, condition);
        return await condition_to_update.save();
    }

    async delete(condition_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(condition_id)) {
            throw new error.IdFormatError('condition_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        let condition_to_delete = await this.findOne(condition_id,
            application_id);
        if (condition_to_delete == null) {
            throw new error.NotFoundError('Not found');
        }

        await Condition
        .model()
        .deleteOne({_id: condition_id, application: application_id});
    }

    async linkToTransition(toLink) {
        let condition_id = toLink.condition;
        let transition_id = toLink.transition;
        let application_id = toLink.application.toString();
        let order = toLink.order;

        if (!mongoose.Types.ObjectId.isValid(condition_id)) {
            throw new error.IdFormatError('condition_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(transition_id)) {
            throw new error.IdFormatError('transition_id provided is invalid');
        }
        let condition_to_link = await this.findOne(condition_id,
            application_id);
        if (condition_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let transition_to_link = await tcore_controller.findOne(transition_id,
            application_id);
        if (transition_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        if (!order) {
            const found = (await this.findByTransition(transition_id,
                application_id));
            let order = found === null ? 0 : found.length;
            return await ConditionTransition.model().create({
                condition: condition_id,
                transition: transition_id,
                order: order,
                application: application_id
            });
        }
        let conditionTransitions = await this.findByTransition(transition_id,
            application_id, true);
        if (!conditionTransitions || conditionTransitions.length == 0) {
            return await ConditionTransition.model().create({
                condition: condition_id,
                transition: transition_id,
                order: 0,
                application: application_id
            });
        } else if (conditionTransitions.filter(
            ct => ct.condition._id.toString() === condition_id).length > 0) {
            throw new error.AlreadyLinkedError(
                'condition_id is already linked');
        } else if (order >= conditionTransitions.length) {
            return await ConditionTransition.model().create({
                condition: condition_id,
                transition: transition_id,
                order: conditionTransitions.length,
                application: application_id
            });
        } else {
            for (let i = (conditionTransitions.length - 1); i >= order; i--) {
                let conditionTransitionToUpdate = Object.assign(
                    conditionTransitions[i].conditionTransition,
                    {order: conditionTransitions[i].order + 1});
                await conditionTransitionToUpdate.save();
            }
            return await ConditionTransition.model().create({
                condition: condition_id,
                transition: transition_id,
                order: order,
                application: application_id
            });
        }
    }

    async deleteLinkToTransition(condition_id, transition_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(condition_id)) {
            throw new error.IdFormatError('condition_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(transition_id)) {
            throw new error.IdFormatError('transition_id provided is invalid');
        }
        let condition_to_link = await this.findOne(condition_id,
            application_id);
        if (condition_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let transition_to_link = await tcore_controller.findOne(transition_id,
            application_id);
        if (transition_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let conditionTransitions = await this.findByTransition(transition_id,
            application_id, true);
        let filteredByCondition = conditionTransitions.filter(
            ct => ct.condition._id.toString() === condition_id);
        if (!filteredByCondition || filteredByCondition.length === 0) {
            throw new error.NotFoundError('Not found');
        }
        const order = filteredByCondition[0].order;
        await ConditionTransition
        .model()
        .deleteOne({
            condition: condition_id,
            transition: transition_id,
            application: application_id
        });
        for (let i = order + 1; i < conditionTransitions.length; i++) {
            let conditionTransitionToUpdate = Object.assign(
                conditionTransitions[i].conditionTransition,
                {order: conditionTransitions[i].order - 1});
            await conditionTransitionToUpdate.save();
        }
    }

    async moveLinkToTransitionOrder(condition_id, transition_id, new_order,
        application_id) {
        if (!mongoose.Types.ObjectId.isValid(condition_id)) {
            throw new error.IdFormatError('condition_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(transition_id)) {
            throw new error.IdFormatError('transition_id provided is invalid');
        }
        let condition_to_link = await this.findOne(condition_id,
            application_id);
        if (condition_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let transition_to_link = await tcore_controller.findOne(transition_id,
            application_id);
        if (transition_to_link == null) {
            throw new error.NotFoundError('Not found');
        }
        let conditionTransitions = await this.findByTransition(transition_id,
            application_id, true);
        let filteredByCondition = conditionTransitions.filter(
            ct => ct.condition._id.toString() === condition_id);
        if (!filteredByCondition || filteredByCondition.length === 0) {
            throw new error.NotFoundError('Not found');
        }
        const order = filteredByCondition[0].order;
        if (conditionTransitions.length === 1) {
            return conditionTransitions[0];
        }
        if (new_order > conditionTransitions.length) {
            new_order = conditionTransitions.length - 1;
        } else if (new_order < 0) {
            new_order = 0;
        }
        let conditionTransitionToUpdate = Object.assign(
            conditionTransitions[order].conditionTransition,
            {order: conditionTransitions.length});
        await conditionTransitionToUpdate.save();
        conditionTransitionToUpdate = Object.assign(
            conditionTransitions[new_order].conditionTransition,
            {order: order});
        await conditionTransitionToUpdate.save();
        conditionTransitionToUpdate = Object.assign(
            conditionTransitions[order].conditionTransition,
            {order: new_order});
        await conditionTransitionToUpdate.save();
        delete conditionTransitions[order].conditionTransition;
        conditionTransitions[order].order = new_order;
        return conditionTransitions[order];
    }

    async isAllowed(condition, status_from, transition, workflow, object_id,
        validation) {
        if (!condition.expression) {
            return true;
        }
        let conditional = new ExpressionConditionalOptionsVO(status_from,
            workflow, transition, object_id, condition, validation);
        return await expression_helper.compileBoolean(condition.expression,
            conditional);
    }
}

module.exports = new ConditionController();
