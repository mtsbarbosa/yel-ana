const express = require('express');
const User = require('../models/user');
const bcrypt = require('bcryptjs');
let config = require('../config');
const mongoose = require('mongoose');
const error = require('../helpers/error');
const email = require('../services/email');
const activation_link = process.env.YEL_ANA_BASE_URL + '/user/activate';
const reset_link = process.env.YEL_ANA_BASE_URL + '/user/redirect-reset';

class UserController {
    findOne(user, callback) {
        /*bcrypt.hash("hash_to_discover", config.bcrypt.saltRounds).then(function(hash) {
            console.log(hash);
        });*/
        User
        .model()
        .findOne(user)
        .then(function (user) {
            callback(null, user);
        });
    }

    async findWithPasswordByEmail(email) {
        return await User.model()
        .findOne({email: email})
        .select('_id name email password blocked email_token');
    }

    async findById(user_id) {
        return await User.model()
        .findOne({_id: user_id})
        .select('_id name email password blocked email_token');
    }

    async findByToken(token) {
        return await User.model()
        .findOne({email_token: token})
        .select('_id name email password blocked email_token');
    }

    async save(user) {
        if (process.env.NODE_ENV !== 'test') {
            const hash = await bcrypt.hash(user.password,
                config.bcrypt.saltRounds);
            user.password = hash;
        }
        user = await User.model().create(user);
        await email.sendEmail('user_activation', user.email,
            {name: user.name, link: `${activation_link}/${user.email_token}`});

        let new_user = JSON.parse(JSON.stringify(user));
        delete new_user.password;
        delete new_user.email_token;
        return new_user;
    }

    async resetPassword(user) {
        let user_to_update = await this.findWithPasswordByEmail(
            user.email);
        if (user_to_update) {
            user_to_update = Object.assign(user_to_update, user);
            await email.sendEmail('user_reset_password', user_to_update.email, {
                name: user_to_update.name,
                link: `${reset_link}/${user_to_update.email_token}`
            });
            await user_to_update.save();
        }
    }

    async encrypt_password(user) {
        if (process.env.NODE_ENV !== 'test') {
            const hash = await bcrypt.hash(user.password,
                config.bcrypt.saltRounds);
            user.password = hash;
        }
        return user;
    }

    async update(user) {
        if (!mongoose.Types.ObjectId.isValid(user._id)) {
            throw new error.IdFormatError('_id provided is invalid');
        }
        let user_to_update = await this.findById(user._id);
        if (user_to_update == null) {
            throw new error.NotFoundError('Not found');
        }

        user_to_update = Object.assign(user_to_update, user);
        return await user_to_update.save();
    }

    async activateByToken(token) {
        let user_to_update = await this.findByToken(token);
        if (user_to_update == null) {
            throw new error.NotFoundError('Not found');
        }
        if (user_to_update.email_token !== token) {
            throw new error.NotAuthorizedError('Failed to authenticate token.');
        }
        user_to_update.blocked = false;
        user_to_update.email_token = undefined;
        user_to_update = await user_to_update.save();
        let user = JSON.parse(JSON.stringify(user_to_update));
        delete user.password;
        return user;
    }

    async activate(user_id, token) {
        if (!mongoose.Types.ObjectId.isValid(user_id)) {
            throw new error.IdFormatError('_id provided is invalid');
        }
        let user_to_update = await this.findById(user_id);
        if (user_to_update == null) {
            throw new error.NotFoundError('Not found');
        }
        if (user_to_update.email_token !== token) {
            throw new error.NotAuthorizedError('Failed to authenticate token.');
        }
        user_to_update.blocked = false;
        user_to_update.email_token = undefined;
        user_to_update = await user_to_update.save();
        let user = JSON.parse(JSON.stringify(user_to_update));
        delete user.password;
        return user;

    }
}

module.exports = new UserController();
