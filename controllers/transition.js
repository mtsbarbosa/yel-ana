const express = require('express');
const Transition = require('../models/transition');
const RecordTransition = require('../models/record_transition');
const workflow_controller = require('./workflow');
const condition_controller = require('./condition');
const tcore_controller = require('./transition_core');
const validation_controller = require('./validation');
const status_controller = require('./status');
const mongoose = require('mongoose');
let error = require('../helpers/error');

class TransitionController {
    async findOne(transition_id, application_id) {
        return await tcore_controller.findOne(transition_id, application_id);
    }

    async findByWorkflow(workflow_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(workflow_id)) {
            return null;
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Transition
        .model()
        .find({workflow: workflow_id, application: application_id})
        .select('_id code name description status_from status_to workflow');
    }

    async findInitialByWorkflow(workflow_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(workflow_id)) {
            return null;
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Transition
        .model()
        .find({
            workflow: workflow_id,
            status_from: null,
            application: application_id
        })
        .select('_id code name description status_from status_to workflow');
    }

    async findAllowedInitialByWorkflow(workflow_id, application_id) {
        let transitions = await this.findInitialByWorkflow(
            workflow_id, application_id);
        return await this.filterAllowedTransitions(transitions,
            workflow_id, {}, application_id);
    }

    async findByWorkflowAndStatusFrom(workflow_id, status_from,
        application_id) {
        if (!mongoose.Types.ObjectId.isValid(workflow_id)) {
            return null;
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            return null;
        }
        return await Transition
        .model()
        .find({
            workflow: workflow_id,
            status_from: status_from,
            application: application_id
        })
        .select('_id code name description status_from status_to workflow');
    }

    async findAllowedByWorkflowAndStatusFrom(workflow_id, status_from,
        application_id) {
        let transitions = await this.findByWorkflowAndStatusFrom(
            workflow_id, status_from, application_id);
        return await this.filterAllowedTransitions(transitions,
            workflow_id, status_from, application_id);
    }

    async filterAllowedTransitions(transitions, workflow_id, status_from,
        application_id) {
        let allowed_transitions = [];
        if (transitions) {
            for (let idx in transitions) {
                let transition = transitions[idx];
                let t_conditions = await condition_controller.findByTransition(
                    transition._id, application_id, true);
                if (t_conditions) {
                    let allowed = true;
                    for (let t_condition_idx in t_conditions) {
                        let t_condition = t_conditions[t_condition_idx];
                        let condition = t_condition.condition;

                        let condition_wrapped = {
                            id: condition._id,
                            name: condition.name,
                            expression: condition.expression,
                            description: condition.description,
                            entity: condition.entity
                        };

                        let status_wrapped = {
                            id: status_from._id,
                            name: status_from.name,
                            code: status_from.code,
                            description: status_from.description,
                            entity: status_from.entity
                        };

                        let transition_wrapped = {
                            id: transition._id,
                            name: transition.name,
                            code: transition.code,
                            description: transition.description,
                            workflow: transition.workflow,
                            status_from: transition.status_from,
                            status_to: transition.status_to
                        };

                        if (!(await condition_controller.isAllowed(
                            condition_wrapped, status_wrapped,
                            transition_wrapped, {id: workflow_id}, ''))) {
                            allowed = false;
                            break;
                        }
                    }
                    if (allowed) {
                        allowed_transitions.push(transition);
                    }
                } else {
                    allowed_transitions.push(transition);
                }
            }
            return allowed_transitions;
        }
        return null;
    }

    async save(transition) {
        return await Transition.model().create(transition);
    }

    async update(transition) {
        if (!mongoose.Types.ObjectId.isValid(transition._id)) {
            throw new error.IdFormatError('_id provided is invalid');
        }
        let transition_to_update = await this.findOne(transition._id,
            transition.application);
        if (transition_to_update == null) {
            throw new error.NotFoundError('Not found');
        }
        transition_to_update = Object.assign(transition_to_update, transition);
        return await transition_to_update.save();
    }

    async delete(transition_id, application_id) {
        if (!mongoose.Types.ObjectId.isValid(transition_id)) {
            throw new error.IdFormatError('transition_id provided is invalid');
        }
        const transition_to_delete = await this.findOne(transition_id,
            application_id);
        if (transition_to_delete == null) {
            throw new error.NotFoundError('Not found');
        }

        await Transition
        .model()
        .deleteOne({_id: transition_id});
    }

    async advance(transition_id, object_id, application_id, object_data,
        record_data) {
        if (!mongoose.Types.ObjectId.isValid(transition_id)) {
            throw new error.IdFormatError('transition_id provided is invalid');
        }
        if (!mongoose.Types.ObjectId.isValid(application_id)) {
            throw new error.IdFormatError('application_id provided is invalid');
        }
        let transition_to_advance = await this.findOne(transition_id,
            application_id);
        if (transition_to_advance == null) {
            throw new error.NotFoundError('Not found');
        }
        let workflow = await workflow_controller.findOne(
            transition_to_advance.workflow, application_id);
        let status_to = await status_controller.findOne(
            transition_to_advance.status_to, application_id);
        let status_from = await status_controller.findOne(
            transition_to_advance.status_from, application_id);
        let v_transitions = await validation_controller.findByTransition(
            transition_id, application_id, true);
        let c_transitions = await condition_controller.findByTransition(
            transition_id, application_id, true);

        let status_wrapped = {};
        if (status_from) {
            status_wrapped = {
                id: status_from._id,
                name: status_from.name,
                code: status_from.code,
                description: status_from.description,
                entity: status_from.entity
            };
        }

        let workflow_wrapped = {
            id: workflow._id,
            name: workflow.name,
            code: workflow.code,
            description: workflow.description,
            entity: workflow.entity
        };

        let transition_wrapped = {
            id: transition_to_advance._id,
            name: transition_to_advance.name,
            code: transition_to_advance.code,
            description: transition_to_advance.description,
            workflow: transition_to_advance.workflow,
            status_from: transition_to_advance.status_from,
            status_to: transition_to_advance.status_to
        };

        if (c_transitions) {
            for (let c_transition_idx in c_transitions) {
                let c_transition = c_transitions[c_transition_idx];
                let condition = c_transition.condition;

                let condition_wrapped = {
                    id: condition._id,
                    name: condition.name,
                    expression: condition.expression,
                    description: condition.description,
                    entity: condition.entity
                };

                if (!(await condition_controller.isAllowed(condition_wrapped,
                    status_wrapped, transition_wrapped, workflow_wrapped,
                    object_id))) {
                    throw new error.ConditionError(
                        'transition is not allowed by condition');
                }
            }
        }
        if (v_transitions) {
            for (let v_transition_idx in v_transitions) {
                let v_transition = v_transitions[v_transition_idx];
                let validation = v_transition.validation;
                let validation_wrapped = {
                    id: validation._id,
                    name: validation.name,
                    expression: validation.expression,
                    description: validation.description,
                    entity: validation.entity
                };
                if (!(await validation_controller.isAllowed(status_wrapped,
                    workflow_wrapped, transition_wrapped, object_id,
                    validation_wrapped))) {
                    throw new error.ValidationError(
                        'transition is not allowed by validation');
                }
            }
        }
        let record_transition = {
            transition: transition_to_advance._id,
            workflow: transition_to_advance.workflow,
            entity: workflow.entity,
            application: application_id,
            status_from: transition_to_advance.status_from,
            status_to: transition_to_advance.status_to,
            object_id: object_id,
            start_date: new Date()
        };
        if (typeof (object_data) !== 'undefined') {
            record_transition.object_data = object_data;
        }
        if (typeof (record_data) !== 'undefined') {
            record_transition.record_data = record_data;
        }
        await RecordTransition
        .model()
        .create(record_transition);
        return {
            status: status_to,
            result: Transition.transition_execute_result.success,
            message: (object_id + " was advanced to Status{id='"
                + transition_to_advance.status_to + "',code='" + status_to.code
                + "'}")
        };
    }

    async findRecordTransitionsByObjectId(object_id, application_id) {
        return await RecordTransition
        .model()
        .find({object_id: object_id, application: application_id})
        .select(
            '_id transition workflow entity status_from status_to object_id object_data record_data start_date');
    }
}

module.exports = new TransitionController();
