class ValidationTransitionVO{
    constructor(_id, validation, transition, expression, order, validationTransition) {
        this._id = _id;
        this.validation = validation;
        this.transition = transition;
        this.expression = expression;
        this.order = order;
        this.validationTransition = validationTransition;
    }

    get id() {
        return this._id;
    }


    set id(value) {
        this._id = value;
    }
}

module.exports = ValidationTransitionVO;