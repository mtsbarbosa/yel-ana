class ExpressionResponseVO{
    private _data: object;
    private _status: Number;
    private _statusText: string;
    private _headers: object;

    constructor(data?: object, status?: Number, statusText?: string, headers?: object){
        this.data = data;
        this.status = status;
        this.statusText = statusText;
        this.headers = headers;
    }


    get data(): object {
        return this._data;
    }

    set data(value: object) {
        this._data = value;
    }

    get status(): Number {
        return this._status;
    }

    set status(value: Number) {
        this._status = value;
    }

    get statusText(): string {
        return this._statusText;
    }

    set statusText(value: string) {
        this._statusText = value;
    }

    get headers(): object {
        return this._headers;
    }

    set headers(value: object) {
        this._headers = value;
    }
}

export = ExpressionResponseVO;