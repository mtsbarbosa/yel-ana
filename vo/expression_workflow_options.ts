import ExpressionOptionsVO from "./expression_options";

class ExpressionWorkflowOptionsVO extends ExpressionOptionsVO{
    protected _status_from: object;
    protected _workflow: object;
    protected _object_id: string;

    constructor(status_from : object, workflow: object, object_id: string) {
        super();
        this._status_from = status_from;
        this._workflow = workflow;
        this._object_id = object_id;
    }

    get status_from(): object {
        return this._status_from;
    }

    set status_from(value: object) {
        this._status_from = value;
    }

    get workflow(): object {
        return this._workflow;
    }

    set workflow(value: object) {
        this._workflow = value;
    }

    get object_id(): string {
        return this._object_id;
    }

    set object_id(value: string) {
        this._object_id = value;
    }

    public objectList(): object{
        let objectList = super.objectList();
        if(this.status_from){
            objectList["status_from"] = this.status_from;
        }
        if(this.workflow){
            objectList["workflow"] = this.workflow;
        }
        if(this.object_id){
            objectList["object_id"] = this.object_id;
        }
        return objectList;
    }
}

export = ExpressionWorkflowOptionsVO;