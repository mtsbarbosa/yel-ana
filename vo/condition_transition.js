class ConditionTransitionVO{
    constructor(_id, condition, transition, expression, order, conditionTransition) {
        this._id = _id;
        this.condition = condition;
        this.transition = transition;
        this.expression = expression;
        this.order = order;
        this.conditionTransition = conditionTransition;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }
}

module.exports = ConditionTransitionVO;