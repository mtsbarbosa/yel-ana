import ExpressionResponseVO from "./expression_response";

const axios = require('axios');

enum HttpRequestMethod{
    GET,
    POST,
    PUT,
    PATCH,
    DELETE
}

class ExpressionOptionsVO{
    private _replacedObjects: object = {};

    constructor(){}


    get replacedObjects(): object {
        return this._replacedObjects;
    }

    set replacedObjects(value: object) {
        this._replacedObjects = value;
    }

    public objectList(): object{
        return {};
    }

    public functionList(): object{
        let localOptions: ExpressionOptionsVO = this;
        return {
            req: async function(method: HttpRequestMethod, url: string, headers?: string, body?: string): Promise<ExpressionResponseVO> {
                let response: ExpressionResponseVO;
                let options: object = {
                    baseURL: url,
                    timeout: 10000,
                    method: method.toString().toLowerCase()
                };
                let json = null;
                if(localOptions.replacedObjects
                    && Object.keys(localOptions.replacedObjects).length > 0
                    && headers
                    && headers.length > 0){
                    options["headers"] = localOptions.replacedObjects[headers];
                }
                if(localOptions.replacedObjects
                    && Object.keys(localOptions.replacedObjects).length > 1
                    && body
                    && body.length > 0){
                    json = localOptions.replacedObjects[body];
                }
                const axiosInstance = axios.create(options);
                let HttpMethods: object = {
                    "GET": axiosInstance.get,
                    "POST": axiosInstance.post,
                    "PUT": axiosInstance.put,
                    "PATCH": axiosInstance.patch,
                    "DELETE": axiosInstance.delete
                };
                if(method == HttpRequestMethod.GET || !json) {
                    response = await HttpMethods[method.toString()]("");
                }else{
                    json = json? json : {};
                    response = await HttpMethods[method.toString()]("", json);
                }
                return response;
            }
        };
    }
}

export = ExpressionOptionsVO;