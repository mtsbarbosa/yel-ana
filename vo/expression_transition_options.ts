import ExpressionWorkflowOptionsVO from "./expression_workflow_options";

class ExpressionTransitionOptionsVO extends ExpressionWorkflowOptionsVO{
    private _after_function: object;
    private _before_function: object;
    private _status_to: object;
    private _transition: object;

    constructor(status_from : object, workflow: object, object_id: string, status_to: object, transition: object, after_function? : object, before_function?: object) {
        super(status_from, workflow, object_id);
        this._after_function = after_function;
        this._before_function = before_function;
        this._status_to = status_to;
        this._transition = transition;
    }

    get after_function(): object {
        return this._after_function;
    }

    set after_function(value: object) {
        this._after_function = value;
    }

    get before_function(): object {
        return this._before_function;
    }

    set before_function(value: object) {
        this._before_function = value;
    }

    get status_to(): object {
        return this._status_to;
    }

    set status_to(value: object) {
        this._status_to = value;
    }

    get transition(): object {
        return this._transition;
    }

    set transition(value: object) {
        this._transition = value;
    }

    public objectList(): object{
        let objectList = super.objectList();
        if(this.after_function){
            objectList["after_function"] = this.after_function;
        }
        if(this.before_function){
            objectList["before_function"] = this.before_function;
        }
        if(this.status_to){
            objectList["status_to"] = this.status_to;
        }
        if(this.transition){
            objectList["transition"] = this.transition;
        }
        return objectList;
    }
}

export = ExpressionTransitionOptionsVO;