import ExpressionWorkflowOptionsVO from "./expression_workflow_options";

class ExpressionConditionalOptionsVO extends ExpressionWorkflowOptionsVO{
    private _condition: object;
    private _validation: object;
    private _transition: object;

    constructor(status_from : object, workflow: object, transition: object, object_id: string, condition?: object, validation?: object) {
        super(status_from, workflow, object_id);
        this.condition = condition;
        this.validation = validation;
        this.transition = transition;
    }

    get condition(): object {
        return this._condition;
    }

    set condition(value: object) {
        this._condition = value;
    }

    get validation(): object {
        return this._validation;
    }

    set validation(value: object) {
        this._validation = value;
    }

    get transition(): object {
        return this._transition;
    }

    set transition(value: object) {
        this._transition = value;
    }

    public objectList(): object{
        let objectList = super.objectList();
        if(this.condition){
            objectList["condition"] = this.condition;
        }else if(this.validation){
            objectList["validation"] = this.validation;
        }
        objectList["transition"] = this.transition;
        return objectList;
    }
}

export = ExpressionConditionalOptionsVO;