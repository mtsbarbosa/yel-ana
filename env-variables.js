module.exports = {
    "dev": {
        mongoose: {
            autoIndex: true,
        },
        morgan: {
            logger: 'dev',
        },
    },
    "test": {
        mongoose: {
            autoIndex: true,
        },
        morgan: {
            logger: 'dev',
        },
    },
    "test-2": {
        mongoose: {
            autoIndex: false,
        },
        morgan: {
            logger: 'dev',
        },
    },
    "qa": {
        mongoose: {
            autoIndex: false,
        },
        morgan: {
            logger: 'dev',
        },
    },
    "prod": {
        mongoose: {
            autoIndex: false,
        },
        morgan: {
            logger: 'dev',
        },
    },
};
