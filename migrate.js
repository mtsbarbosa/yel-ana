const express = require('express');
const fs = require('fs');
const fs_async = require('fs').promises;

if (process.env.NODE_ENV !== 'test') {
    require('dotenv').config();
}

const db = require('./db');

const app = express();
app.locals.db = db;

const migrate = async (app) => {
    app.locals.db = db;
    await db.initialize();

    let migrations = fs.readdirSync('./migrations');
    for (let i = 0; i < migrations.length; i++) {
        let file = migrations[i].substr(0, migrations[i].lastIndexOf('.'));
        if(file !== 'migrated'){
            const { up } = require('./migrations/' + file);
            try {
                const data = await fs_async.readFile('./migrations/migrated.json');
                let json = JSON.parse(data);
                if(json.migrated.indexOf(file) === -1){
                    await up();
                    console.log("WARNING",file + " was executed");
                    json.migrated.push(file);
                    await fs_async.writeFile("./migrations/migrated.json", JSON.stringify(json));
                    console.log("DEBUG",JSON.stringify(json) + " was written to ./migrations/migrated.json");
                }else{
                    console.log("DEBUG",file + " was skipped");
                }
            }catch (error) {
                console.log("error to load migrated.json => " + error);
            }
        }
    }
    console.log("DEBUG","Migration was finished successfully");
};

(async () => {
    try {
        await migrate(app);
    }catch (error) {
        console.log("ERROR","error to migrate");
    }
    process.exit();
})();
