// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = {
    schema: function () {
        let schema =
            (new Schema({
                _id: {type: Schema.Types.ObjectId, auto: true},
                transition: {
                    type: Schema.Types.ObjectId,
                    ref: 'Transition',
                    required: true
                },
                workflow: {
                    type: Schema.Types.ObjectId,
                    ref: 'Workflow',
                    required: true
                },
                entity: {
                    type: Schema.Types.ObjectId,
                    ref: 'Entity',
                    required: true
                },
                status_from: {
                    type: Schema.Types.ObjectId,
                    ref: 'Status',
                    required: false
                },
                status_to: {
                    type: Schema.Types.ObjectId,
                    ref: 'Status',
                    required: true
                },
                object_id: {type: String, required: true},
                object_data: {type: String, required: false},
                record_data: {type: String, required: false},
                start_date: {type: Date, default: Date.now, required: true},
                application: {
                    type: Schema.Types.ObjectId,
                    ref: 'Application',
                    required: true
                },
            }));

        return schema;
    },
    model: function () {
        return mongoose.model('RecordTransition');
    }
};
