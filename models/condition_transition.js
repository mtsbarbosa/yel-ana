// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = {
    schema: function () {
        let schema =
            (new Schema({
                condition: {
                    type: Schema.Types.ObjectId,
                    ref: 'Condition',
                    required: true
                },
                transition: {
                    type: Schema.Types.ObjectId,
                    ref: 'Transition',
                    required: true
                },
                order: {type: Number, required: true},
                application: {
                    type: Schema.Types.ObjectId,
                    ref: 'Application',
                    required: true
                },
            }));
        schema.index({condition: 1, transition: 1}, {unique: true});
        schema.index({condition: 1, transition: 1, order: 1}, {unique: true});

        return schema;
    },
    model: function () {
        return mongoose.model('ConditionTransition');
    }
};
