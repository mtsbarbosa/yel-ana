// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = {
    schema: function () {
        let schema =
            (new Schema({
                _id: {type: Schema.Types.ObjectId, auto: true},
                key: {type: String, required: true},
                value: {type: Schema.Types.Mixed, required: true},
                description: {type: String, required: false},
            }));
        schema.index({key: 1}, {unique: true});

        return schema;
    },
    model: function () {
        return mongoose.model('Configuration');
    },
    constants: {
        cors_whitelist: "cors_whitelist"
    }
};
