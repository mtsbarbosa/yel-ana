// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = {
    schema: function () {
        let schema =
            (new Schema({
                _id: {type: Schema.Types.ObjectId, auto: true},
                code: {type: String, required: true},
                name: {type: String, required: true},
                description: {type: String, required: false},
                workflow: {
                    type: Schema.Types.ObjectId,
                    ref: 'Workflow',
                    required: true
                },
                status_from: {
                    type: Schema.Types.ObjectId,
                    ref: 'Status',
                    required: false
                },
                status_to: {
                    type: Schema.Types.ObjectId,
                    ref: 'Status',
                    required: true
                },
                application: {
                    type: Schema.Types.ObjectId,
                    ref: 'Application',
                    required: true
                },
            }));
        schema.index({code: 1, workflow: 1}, {unique: true});

        return schema;
    },
    transition_execute_result: {
        success: "success",
        validation_failed: "validation_failed",
        failed: "failed"
    },
    model: function () {
        return mongoose.model('Transition');
    }
};
