// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = {
    schema: function () {
        let schema =
            (new Schema({
                _id: {type: Schema.Types.ObjectId, auto: true},
                code: {type: String, required: true},
                name: {type: String, required: true},
                description: {type: String, required: false},
                entity: {
                    type: Schema.Types.ObjectId,
                    ref: 'Entity',
                    required: true
                },
                application: {
                    type: Schema.Types.ObjectId,
                    ref: 'Application',
                    required: true
                },
            }));
        schema.index({code: 1, entity: 1}, {unique: true});

        return schema;
    },
    model: function () {
        return mongoose.model('Workflow');
    }
};
