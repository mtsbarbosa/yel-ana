// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = {
    schema: function () {
        let schema =
            (new Schema({
                _id: {type: Schema.Types.ObjectId, auto: true},
                name: {type: String, required: true, unique: true},
                description: {type: String, required: false},
                hash_key: {type: String, required: true},
                root: {type: Boolean, required: true},
                user: {
                    type: Schema.Types.ObjectId,
                    ref: 'User',
                    required: true
                },
            }));
        schema.index({name: 1, user: 1}, {unique: true});
        return schema;
    },
    model: function () {
        return mongoose.model('Application');
    }
};
