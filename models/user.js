// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = {
    schema: function () {
        let schema =
            (new Schema({
                _id: {type: Schema.Types.ObjectId, auto: true},
                name: {type: String, required: true},
                email: {type: String, required: true, unique: true},
                password: {type: String, required: true},
                blocked: {type: Boolean, required: true, default: true},
                email_token: {type: String}
            }));

        return schema;
    },
    model: function () {
        return mongoose.model('User');
    }
};
