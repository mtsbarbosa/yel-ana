const express = require('express');
let router = express.Router();
let status_controller = require('../controllers/status');
let validator = require('../helpers/validate_object');
let error = require('../helpers/error');

/* GET users listing. */
router.get('/:status_id', async(req, res, next) => {
  try{
    const status_id = req.params.status_id;
    const application_id = req.locals.application;
    if(typeof(status_id) == 'undefined' || status_id == null)
      res.status(400).json({ success: false, message: 'status_id is required' });
    else{
      const status = await status_controller.findOne(status_id, application_id);
      if(typeof(status) == 'undefined' || status == null)
        res.status(404).json({ success: true, message: 'Not found' });
      else
        res.status(200).json({ success: true, message: status });
    }
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.get('/entity/:entity_id', async(req, res, next) => {
  try{
    const entity_id = req.params.entity_id;
    const application_id = req.locals.application;
    if(typeof(entity_id) == 'undefined' || entity_id == null)
      res.status(400).json({ success: false, message: 'entity_id is invalid' });
    else{
      const status = await status_controller.findByEntity(entity_id, application_id);
      res.status(200).json({ success: true, message: status });
    }
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.post('/', async(req, res, next) => {
  try{
    const status_param = validator.extract(
      req.body,
      ['code','name','entity'],//validation
      ['code','name','description','entity']//allowed
    );
    Object.assign(status_param, req.locals);
    const status = await status_controller.save(status_param);
    res.status(204).send();
  }catch(err){
    if(err instanceof validator.ParamNotFoundError){
      res.status(400).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

router.put('/', async(req, res, next) => {
  try{
    const status_param = validator.extract(
      req.body,
      ['_id','code','name'],//validation
      ['_id','code','name','description']//allowed
    );
    Object.assign(status_param, req.locals);
    const status = await status_controller.update(status_param);
    res.status(204).send();
  }catch(err){
    if(err instanceof validator.ParamNotFoundError
        || err instanceof error.IdFormatError){
      res.status(400).json({ success: false, message: err.message });
    }else if(err instanceof error.NotFoundError){
      res.status(404).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

router.delete('/:status_id', async(req, res, next) => {
  try{
    const status_id = req.params.status_id;
    const application_id = req.locals.application;
    if(typeof(status_id) == 'undefined' || status_id == null)
      res.status(400).json({ success: false, message: 'status_id is required' });
    else{
      const status = await status_controller.delete(status_id,application_id);
      res.status(204).send();
    }
  }catch(err){
    if(err instanceof validator.ParamNotFoundError
        || err instanceof error.IdFormatError){
      res.status(400).json({ success: false, message: err.message });
    }else if(err instanceof error.NotFoundError){
      res.status(404).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

module.exports = router;
