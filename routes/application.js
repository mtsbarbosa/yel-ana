const express = require('express');
const router = express.Router();
const application_controller = require('../controllers/application');
const temporary_token_interceptor = require('../middlewares/temporary_token_interceptor');
const bcrypt = require('bcryptjs');
const validator = require('../helpers/validate_object');
const temporary_token_helper = require('../helpers/temporary_token');
const error = require('../helpers/error');

router.get('/', async(req, res, next) => {
    try{
        const application_id = req.params.application_id;
        const user_id = req.locals.user;
        if(typeof(user_id) == 'undefined' || user_id == null)
            res.status(403).json({ success: false, message: 'Not found' });
        const applications = await application_controller.findByUser(user_id);
        res.status(200).json({ success: true, message: applications });
    }catch(err){
        console.log('err',err);
        res.status(500).json({ success: false, message: 'Internal error' });
        next(err);
    }
});

router.get('/:application_id', async(req, res, next) => {
    try{
        const application_id = req.params.application_id;
        const user_id = req.locals.user;
        if(typeof(application_id) == 'undefined' || application_id == null) {
            const applications = await application_controller.findByUser(user_id);
            res.status(200).json({ success: true, message: applications });
        }else{
            const application = await application_controller.findOne(application_id, user_id);
            if(typeof(application) == 'undefined' || application == null)
                res.status(404).json({ success: false, message: 'Not found' });
            else
                res.status(200).json({ success: true, message: application });
        }
    }catch(err){
        console.log('err',err);
        res.status(500).json({ success: false, message: 'Internal error' });
        next(err);
    }
});

router.post('/:application_id/hash_key', async(req, res, next) => {
    try{
        const application_id = req.params.application_id;
        const user_id = req.locals.user;
        let app_param = validator.extract(
            req.body,
            ['email','password'],//validation
            ['email','password']//allowed
        );
        if(typeof(application_id) == 'undefined' || application_id == null) {
            res.status(400).json({ success: false, message: 'application_id is mandatory' });
        }else{
            const application = await application_controller.findOne(application_id, user_id);
            if(typeof(application) == 'undefined' || application == null)
                res.status(404).json({ success: false, message: 'Not found' });
            else{
                const token = await temporary_token_helper.authenticate_for_temp_token(app_param.email, app_param.password, 5);
                res.status(200).json({
                    success: true,
                    token: token
                });
            }
        }
    }catch(err){
        if(err instanceof validator.ParamNotFoundError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.NotAuthorizedError){
            res.status(403).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal error' });
        }
        next(err);
    }
});

router.post('/', async(req, res, next) => {
  // find the app
  try{
    let app_param = validator.extract(
        req.body,
        ['name','hash_key'],//validation
        ['name','hash_key','description']//allowed
    );
    Object.assign(app_param, req.locals);
    const app = await application_controller.save(app_param);
    res.status(200).json({ success: true, message: app });
  }catch(err){
      if(err instanceof validator.ParamNotFoundError){
          res.status(400).json({ success: false, message: err.message });
      }else{
          console.log('err',err);
          res.status(500).json({ success: false, message: 'Internal Error' });
      }
      next(err);
  }
});

router.put('/', async(req, res, next) => {
    try{
        let application_param = validator.extract(
            req.body,
            ['_id','name'],//validation
            ['_id','name','description']//allowed
        );
        Object.assign(application_param, req.locals);
        let application = await application_controller.update(application_param);
        res.status(200).json({ success: true, message: application });
    }catch(err){
        if(err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, messagevar: 'Internal Error' });
        }
        next(err);
    }
});

router.patch('/', temporary_token_interceptor, async(req, res, next) => {
    try{
        const user_id = req.locals.user;
        let application_param = validator.extract(
            req.body,
            ['_id','hash_key_new','hash_key_new_confirmation'],//validation
            ['_id','hash_key_new','hash_key_new_confirmation']//allowed
        );
        Object.assign(application_param, req.locals);
        if(application_param.hash_key_new !== application_param.hash_key_new_confirmation){
            throw new error.WrongCredentialsError('hash_key_new does not match hash_key_new_confirmation');
        }
        let application = await application_controller.findOne(application_param._id,user_id);
        if(!application){
            throw new error.NotFoundError('Not Found');
        }
        let app_to_update = {_id: application_param._id, hash_key: application_param.hash_key_new};
        Object.assign(app_to_update, req.locals);
        await application_controller.update(app_to_update);
        res.status(204).send();
    }catch(err){
        if(err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.WrongCredentialsError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

router.delete('/:application_id', async(req, res, next) => {
    try{
        const application_id = req.params.application_id;
        const user_id = req.locals.user;
        if(typeof(application_id) == 'undefined' || application_id == null)
            res.status(400).json({ success: false, message: 'application_id is required' });
        else{
            await application_controller.delete(application_id,user_id);
            res.status(204).send();
        }
    }catch(err){
        if(err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

module.exports = router;
