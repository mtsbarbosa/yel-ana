const express = require('express');
let router = express.Router();
const validation_controller = require('../controllers/validation');
const validator = require('../helpers/validate_object');
const error = require('../helpers/error');

router.get('/:validation_id', async (req, res, next) => {
    try {
        let validation_id = req.params.validation_id;
        let application_id = req.locals.application;
        if (typeof(validation_id) == 'undefined' || validation_id == null)
            res.status(400).json({success: false, message: 'validation_id is required'});
        else {
            let validation = await validation_controller.findOne(validation_id, application_id);
            if (typeof(validation) == 'undefined' || validation == null)
                res.status(404).json({success: true, message: 'Not found'});
            else
                res.status(200).json({success: true, message: validation});
        }
    } catch (err) {
        console.log('err', err);
        res.status(500).json({success: false, message: 'Internal error'});
        next(err);
    }
});

router.get('/entity/:entity_id', async (req, res, next) => {
    try {
        let entity_id = req.params.entity_id;
        let application_id = req.locals.application;
        if (typeof(entity_id) == 'undefined' || entity_id == null)
            res.status(400).json({success: false, message: 'entity_id is invalid'});
        else {
            let validation = await validation_controller.findByEntity(entity_id, application_id);
            res.status(200).json({success: true, message: validation});
        }
    } catch (err) {
        console.log('err', err);
        res.status(500).json({success: false, message: 'Internal error'});
        next(err);
    }
});

router.post('/', async (req, res, next) => {
    try {
        let validation_param = validator.extract(
            req.body,
            ['name', 'entity', 'expression'],//validation
            ['name', 'description', 'entity', 'expression']//allowed
        );
        Object.assign(validation_param, req.locals);
        let validation = await validation_controller.save(validation_param);
        res.status(200).send({success: true, message: validation});
    } catch (err) {
        if (err instanceof validator.ParamNotFoundError) {
            res.status(400).json({success: false, message: err.message});
        } else {
            console.log('err', err);
            res.status(500).json({success: false, message: 'Internal Error'});
        }
        next(err);
    }
});

router.put('/', async (req, res, next) => {
    try {
        let validation_param = validator.extract(
            req.body,
            ['_id', 'name', 'expression'],//validation
            ['_id', 'name', 'description', 'expression']//allowed
        );
        Object.assign(validation_param, req.locals);
        let validation = await validation_controller.update(validation_param);
        res.status(200).send({success: true, message: validation});
    } catch (err) {
        if (err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError) {
            res.status(400).json({success: false, message: err.message});
        } else if (err instanceof error.NotFoundError) {
            res.status(404).json({success: false, message: err.message});
        } else {
            console.log('err', err);
            res.status(500).json({success: false, message: 'Internal Error'});
        }
        next(err);
    }
});

router.delete('/:validation_id', async (req, res, next) => {
    try {
        let validation_id = req.params.validation_id;
        let application_id = req.locals.application;
        if (typeof(validation_id) == 'undefined' || validation_id == null)
            res.status(400).json({success: false, message: 'validation_id is required'});
        else {
            let validation = await validation_controller.delete(validation_id, application_id);
            res.status(204).send();
        }
    } catch (err) {
        if (err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError) {
            res.status(400).json({success: false, message: err.message});
        } else if (err instanceof error.NotFoundError) {
            res.status(404).json({success: false, message: err.message});
        } else {
            console.log('err', err);
            res.status(500).json({success: false, message: 'Internal Error'});
        }
        next(err);
    }
});

module.exports = router;
