const express = require('express');
let router = express.Router();
const validation_controller = require('../controllers/validation');
const validator = require('../helpers/validate_object');
const error = require('../helpers/error');

router.get('/transition/:transition_id', async (req, res, next) => {
    try {
        let transition_id = req.params.transition_id;
        let application_id = req.locals.application;
        if (typeof(transition_id) == 'undefined' || transition_id == null)
            res.status(400).json({success: false, message: 'transition_id is required'});
        else {
            let validation_transitions = await validation_controller.findByTransition(transition_id, application_id);
            if (typeof(validation_transitions) == 'undefined' || validation_transitions == null)
                res.status(404).json({success: true, message: 'Not found'});
            else
                res.status(200).json({success: true, message: validation_transitions});
        }
    } catch (err) {
        console.log('err', err);
        res.status(500).json({success: false, message: 'Internal error'});
        next(err);
    }
});

router.post('/', async(req, res, next) => {
    try{
        let post_param = validator.extract(
            req.body,
            ['validation','transition'],//validation
            ['validation','transition','order']//allowed
        );
        Object.assign(post_param, req.locals);
        let saved = await validation_controller.linkToTransition(post_param);
        res.status(200).send({success: true, message: saved});
    }catch(err){
        if(err instanceof validator.ParamNotFoundError || err instanceof error.AlreadyLinkedError) {
            res.status(400).json({success: false, message: err.message});
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

router.delete('/transition/:transition_id/validation/:validation_id', async(req, res, next) => {
    try{
        let validation_id = req.params.validation_id;
        let transition_id = req.params.transition_id;
        let application_id = req.locals.application;
        if(typeof(validation_id) == 'undefined' || validation_id == null)
            res.status(400).json({ success: false, message: 'validation_id is required' });
        else if(typeof(transition_id) == 'undefined' || transition_id == null)
            res.status(400).json({ success: false, message: 'transition_id is required' });
        else{
            await validation_controller.deleteLinkToTransition(validation_id,transition_id,application_id);
            res.status(204).send();
        }
    }catch(err){
        if(err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

router.patch('/transition/:transition_id/validation/:validation_id/order/:order', async(req, res, next) => {
    try{
        let validation_id = req.params.validation_id;
        let transition_id = req.params.transition_id;
        let order = req.params.order;
        let application_id = req.locals.application;

        if(typeof(validation_id) == 'undefined' || validation_id == null)
            res.status(400).json({ success: false, message: 'validation_id is required' });
        else if(typeof(transition_id) == 'undefined' || transition_id == null)
            res.status(400).json({ success: false, message: 'transition_id is required' });
        else if(typeof(order) == 'undefined' || order == null || isNaN(order))
            res.status(400).json({ success: false, message: 'order is required or in bad format' });
        else{
            let moved = await validation_controller.moveLinkToTransitionOrder(validation_id,transition_id,order,application_id);
            res.status(200).send({success: true, message: moved});
        }
    }catch(err){
        if(err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

module.exports = router;