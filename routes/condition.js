const express = require('express');
let router = express.Router();
const condition_controller = require('../controllers/condition');
const validator = require('../helpers/validate_object');
const error = require('../helpers/error');

router.get('/:condition_id', async (req, res, next) => {
    try {
        let condition_id = req.params.condition_id;
        let application_id = req.locals.application;
        if (typeof(condition_id) == 'undefined' || condition_id == null)
            res.status(400).json({success: false, message: 'condition_id is required'});
        else {
            let condition = await condition_controller.findOne(condition_id, application_id);
            if (typeof(condition) == 'undefined' || condition == null)
                res.status(404).json({success: true, message: 'Not found'});
            else
                res.status(200).json({success: true, message: condition});
        }
    } catch (err) {
        console.log('err', err);
        res.status(500).json({success: false, message: 'Internal error'});
        next(err);
    }
});

router.get('/entity/:entity_id', async (req, res, next) => {
    try {
        let entity_id = req.params.entity_id;
        let application_id = req.locals.application;
        if (typeof(entity_id) == 'undefined' || entity_id == null)
            res.status(400).json({success: false, message: 'entity_id is invalid'});
        else {
            let condition = await condition_controller.findByEntity(entity_id, application_id);
            res.status(200).json({success: true, message: condition});
        }
    } catch (err) {
        console.log('err', err);
        res.status(500).json({success: false, message: 'Internal error'});
        next(err);
    }
});

router.post('/', async (req, res, next) => {
    try {
        let condition_param = validator.extract(
            req.body,
            ['name', 'entity', 'expression'],//validation
            ['name', 'description', 'entity', 'expression']//allowed
        );
        Object.assign(condition_param, req.locals);
        let condition = await condition_controller.save(condition_param);
        res.status(200).send({success: false, message: condition});
    } catch (err) {
        if (err instanceof validator.ParamNotFoundError) {
            res.status(400).json({success: false, message: err.message});
        } else {
            console.log('err', err);
            res.status(500).json({success: false, message: 'Internal Error'});
        }
        next(err);
    }
});

router.put('/', async (req, res, next) => {
    try {
        let condition_param = validator.extract(
            req.body,
            ['_id', 'name', 'expression'],//validation
            ['_id', 'name', 'description', 'expression']//allowed
        );
        Object.assign(condition_param, req.locals);
        let condition = await condition_controller.update(condition_param);
        res.status(200).send({success: false, message: condition});
    } catch (err) {
        if (err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError) {
            res.status(400).json({success: false, message: err.message});
        } else if (err instanceof error.NotFoundError) {
            res.status(404).json({success: false, message: err.message});
        } else {
            console.log('err', err);
            res.status(500).json({success: false, message: 'Internal Error'});
        }
        next(err);
    }
});

router.delete('/:condition_id', async (req, res, next) => {
    try {
        let condition_id = req.params.condition_id;
        let application_id = req.locals.application;
        if (typeof(condition_id) == 'undefined' || condition_id == null)
            res.status(400).json({success: false, message: 'condition_id is required'});
        else {
            let condition = await condition_controller.delete(condition_id, application_id);
            res.status(204).send();
        }
    } catch (err) {
        if (err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError) {
            res.status(400).json({success: false, message: err.message});
        } else if (err instanceof error.NotFoundError) {
            res.status(404).json({success: false, message: err.message});
        } else {
            console.log('err', err);
            res.status(500).json({success: false, message: 'Internal Error'});
        }
        next(err);
    }
});

module.exports = router;
