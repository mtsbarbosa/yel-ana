const express = require('express');
let router = express.Router();
const condition_controller = require('../controllers/condition');
const validator = require('../helpers/validate_object');
const error = require('../helpers/error');

router.get('/transition/:transition_id', async (req, res, next) => {
    try {
        let transition_id = req.params.transition_id;
        let application_id = req.locals.application;
        if (typeof(transition_id) == 'undefined' || transition_id == null)
            res.status(400).json({success: false, message: 'transition_id is required'});
        else {
            let condition_transitions = await condition_controller.findByTransition(transition_id, application_id);
            if (typeof(condition_transitions) == 'undefined' || condition_transitions == null)
                res.status(404).json({success: true, message: 'Not found'});
            else
                res.status(200).json({success: true, message: condition_transitions});
        }
    } catch (err) {
        console.log('err', err);
        res.status(500).json({success: false, message: 'Internal error'});
        next(err);
    }
});

router.post('/', async(req, res, next) => {
    try{
        let post_param = validator.extract(
            req.body,
            ['condition','transition'],//validation
            ['condition','transition','order']//allowed
        );
        Object.assign(post_param, req.locals);
        let saved = await condition_controller.linkToTransition(post_param);
        res.status(200).send({success: true, message: saved});
    }catch(err){
        if(err instanceof validator.ParamNotFoundError || err instanceof error.AlreadyLinkedError) {
            res.status(400).json({success: false, message: err.message});
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

router.delete('/transition/:transition_id/condition/:condition_id', async(req, res, next) => {
    try{
        let condition_id = req.params.condition_id;
        let transition_id = req.params.transition_id;
        let application_id = req.locals.application;
        if(typeof(condition_id) == 'undefined' || condition_id == null)
            res.status(400).json({ success: false, message: 'condition_id is required' });
        else if(typeof(transition_id) == 'undefined' || transition_id == null)
            res.status(400).json({ success: false, message: 'transition_id is required' });
        else{
            await condition_controller.deleteLinkToTransition(condition_id,transition_id,application_id);
            res.status(204).send();
        }
    }catch(err){
        if(err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

router.patch('/transition/:transition_id/condition/:condition_id/order/:order', async(req, res, next) => {
    try{
        let condition_id = req.params.condition_id;
        let transition_id = req.params.transition_id;
        let order = req.params.order;
        let application_id = req.locals.application;

        if(typeof(condition_id) == 'undefined' || condition_id == null)
            res.status(400).json({ success: false, message: 'condition_id is required' });
        else if(typeof(transition_id) == 'undefined' || transition_id == null)
            res.status(400).json({ success: false, message: 'transition_id is required' });
        else if(typeof(order) == 'undefined' || order == null || isNaN(order))
            res.status(400).json({ success: false, message: 'order is required or in bad format' });
        else{
            let moved = await condition_controller.moveLinkToTransitionOrder(condition_id,transition_id,order,application_id);
            res.status(200).send({success: true, message: moved});
        }
    }catch(err){
        if(err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

module.exports = router;