const express = require('express');
const router = express.Router();
const application_controller = require('../controllers/application');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config');

router.post('/', async (req, res, next) => {
  try {
      let application = await application_controller.findByExample({
          name: req.body.name
      });
      if (!application) {
          res.status(403).json({ success: false, message: 'Authentication failed.' });
      } else if (application) {
          if(process.env.NODE_ENV === 'test'){
              if(req.body.hash_key === application.hash_key){
                  delete application.hash_key;
                  const payload = {
                      root: application.root,
                      application: application,
                      date: new Date()
                  };
                  const token = jwt.sign(payload, config.secret, {
                      expiresIn: 1440 // expires in 24 hours
                  });
                  res.status(200).json({
                      success: true,
                      message: 'The token for your application is here',
                      token: token
                  });
              }else{
                  res.status(403).json({ success: false, message: 'Authentication failed.' });
              }
          }else{
              // check if password matches
              let hash_res = await bcrypt.compare(req.body.hash_key, application.hash_key);
              if(hash_res == true){
                  // if user is found and password is right
                  // create a token with only our given payload
                  // we don't want to pass in the entire user since that has the password
                  delete application.hash_key;
                  const payload = {
                      root: application.root,
                      application: application,
                      date: new Date()
                  };
                  const token = jwt.sign(payload, config.secret, {
                      expiresIn: 1440 // expires in 24 hours
                  });
                  // return the information including token as JSON
                  res.status(200).json({
                      success: true,
                      message: 'The token for your application is here',
                      token: token
                  });
              }else{
                  res.status(403).json({ success: false, message: 'Authentication failed.' });
              }
          }
      }
  }catch (err) {
      res.status(500).json({ success: false, message: 'Internal error' });
  }
});

router.post('/id/:application_id', async(req, res, next) => {
    try{
        const application_id = req.params.application_id;
        const user_id = req.locals.user;
        if(typeof(application_id) == 'undefined' || application_id == null) {
            res.status(400).json({ success: false, message: 'application_id is mandatory' });
        }else{
            let application = await application_controller.findByExample({_id: application_id, user: user_id});
            if(typeof(application) == 'undefined' || application == null)
                res.status(403).json({ success: false, message: 'Not Authorized' });
            else {
                delete application.hash_key;
                const payload = {
                    root: application.root,
                    application: application,
                    date: new Date()
                };
                const token = jwt.sign(payload, config.secret, {
                    expiresIn: 1440 // expires in 24 hours
                });
                // return the information including token as JSON
                res.status(200).json({
                    success: true,
                    message: 'The token for your application is here',
                    token: token
                });
            }
        }
    }catch(err){
        console.log('err',err);
        res.status(500).json({ success: false, message: 'Internal error' });
        next(err);
    }
});

module.exports = router;
