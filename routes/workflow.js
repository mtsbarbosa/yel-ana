const express = require('express');
let router = express.Router();
let workflow_controller = require('../controllers/workflow');
let validator = require('../helpers/validate_object');
let error = require('../helpers/error');

/* GET users listing. */
router.get('/:workflow_id', async(req, res, next) => {
  try{
    const workflow_id = req.params.workflow_id;
    const application_id = req.locals.application;
    if(typeof(workflow_id) == 'undefined' || workflow_id == null)
      res.status(400).json({ success: false, message: 'workflow_id is required' });
    else{
      const workflow = await workflow_controller.findOne(workflow_id, application_id);
      if(typeof(workflow) == 'undefined' || workflow == null)
        res.status(404).json({ success: true, message: 'Not found' });
      else
        res.status(200).json({ success: true, message: workflow });
    }
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.get('/entity/:entity_id', async(req, res, next) => {
  try{
    const entity_id = req.params.entity_id;
    const application_id = req.locals.application;
    if(typeof(entity_id) == 'undefined' || entity_id == null)
      res.status(400).json({ success: false, message: 'entity_id is invalid' });
    else{
      const workflow = await workflow_controller.findByEntity(entity_id,application_id);
      res.status(200).json({ success: true, message: workflow });
    }
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.post('/', async(req, res, next) => {
  try{
    const workflow_param = validator.extract(
      req.body,
      ['code','name','entity'],//validation
      ['code','name','description','entity']//allowed
    );
    Object.assign(workflow_param, req.locals);
    const workflow = await workflow_controller.save(workflow_param);
    res.status(204).send();
  }catch(err){
    if(err instanceof validator.ParamNotFoundError){
      res.status(400).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

router.put('/', async(req, res, next) => {
  try{
    const workflow_param = validator.extract(
      req.body,
      ['_id','code','name'],//validation
      ['_id','code','name','description']//allowed
    );
    Object.assign(workflow_param, req.locals);
    const workflow = await workflow_controller.update(workflow_param);
    res.status(204).send();
  }catch(err){
    if(err instanceof validator.ParamNotFoundError
        || err instanceof error.IdFormatError){
      res.status(400).json({ success: false, message: err.message });
    }else if(err instanceof error.NotFoundError){
      res.status(404).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

router.delete('/:workflow_id', async(req, res, next) => {
  try{
    const workflow_id = req.params.workflow_id;
    const application_id = req.locals.application;
    if(typeof(workflow_id) == 'undefined' || workflow_id == null)
      res.status(400).json({ success: false, message: 'workflow_id is required' });
    else{
      const workflow = await workflow_controller.delete(workflow_id,application_id);
      res.status(204).send();
    }
  }catch(err){
    if(err instanceof validator.ParamNotFoundError
        || err instanceof error.IdFormatError){
      res.status(400).json({ success: false, message: err.message });
    }else if(err instanceof error.NotFoundError){
      res.status(404).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

module.exports = router;
