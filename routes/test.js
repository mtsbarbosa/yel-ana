const express = require('express');
let router = express.Router();
let email_service = require('../services/email');
let user_controller = require('../controllers/user');
let custom_data_controller = require('../tests/controllers/test_data');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json({ success: true, message: req.decoded.application });
});

router.get('/get_token/:user_id', async function(req, res, next) {
    const user_id = req.params.user_id;
    const user = await user_controller.findById(user_id);
    if(user.email_token){
        res.json({ success: true, message: user.email_token });
    }else{
        res.json({ success: false });
    }
});

router.get('/get_token/get_user/:user_id', async function(req, res, next) {
    const user_id = req.params.user_id;
    const user = await user_controller.findById(user_id);
    res.json({ success: true, message: user });
});

router.get('/activate', async function(req, res, next) {
    res.status(200).json({ success: true });
});

router.get('/change-password/:email/:token', async function(req, res, next) {
    const email = req.params.email;
    const token = req.params.token;
    if(!email || !token){
        res.status(500).json({ success: false });
    }
    res.status(200).json({ success: true });
});

router.post('/send_email/:template', async function(req, res, next) {
    const template = req.params.template;
    await email_service.sendEmail(template, req.body.to, req.body);
    res.status(200).json({ success: true });
});

router.post('/custom_data', async function(req, res, next) {
    const custom_data = req.body;
    custom_data_controller.custom_data = custom_data;
    if(req.header("header_present")){
        custom_data_controller.custom_header = {header_present: req.header("header_present")};
    }
    res.status(200).json({ success: true });
});

router.get('/custom_data', async function(req, res, next) {
    res.set(custom_data_controller.custom_header).status(200).json({ success: true, message: custom_data_controller.custom_data });
});

router.get('/bool_data', async function(req, res, next) {
    res.status(200).json({ success: true, message: true });
});

module.exports = router;
