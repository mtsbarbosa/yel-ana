const express = require('express');
const router = express.Router();
const user_controller = require('../controllers/user');
const validator = require('../helpers/validate_object');
const error = require('../helpers/error');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config');

router.post('/authenticate', function(req, res, next) {
    // find the user
    user_controller.findOne({
        email: req.body.email
    }, function(err, user) {
        if (err) throw err;

        if (!user) {
            res.status(403).json({ success: false, message: 'Authentication failed.' });
        } else if (user.blocked) {
            res.status(403).json({ success: false, message: 'User is blocked' });
        } else {
            if(process.env.NODE_ENV === 'test'){
                if(req.body.password === user.password){
                    delete user.password;
                    const payload = {
                        root: user.root,
                        user: user,
                        date: new Date()
                    };
                    const token = jwt.sign(payload, config.secret, {
                        expiresIn: 1440 // expires in 24 hours
                    });
                    res.status(200).json({
                        success: true,
                        message: 'The token for your user is here',
                        token: token
                    });
                }else{
                    res.status(403).json({ success: false, message: 'Authentication failed.' });
                }
            }else{
                // check if password matches
                bcrypt.compare(req.body.password, user.password,function(err,password) {
                    if(password == true){
                        // if user is found and password is right
                        // create a token with only our given payload
                        // we don't want to pass in the entire user since that has the password
                        delete user.password;
                        const payload = {
                            root: user.root,
                            user: user,
                            date: new Date()
                        };
                        const token = jwt.sign(payload, config.secret, {
                            expiresIn: 1440 // expires in 24 hours
                        });
                        // return the information including token as JSON
                        res.status(200).json({
                            success: true,
                            message: 'The token for your user is here',
                            token: token
                        });
                    }else{
                        res.status(403).json({ success: false, message: 'Authentication failed.' });
                    }
                });
            }
        }
    });
});

router.post('/', async(req, res, next) => {
  try{
      const user_param = validator.extract(
          req.body,
          ['name','email','password'],//validation
          ['name','email','password']//allowed
      );
      const payload = {
          root: false
      };
      const token = jwt.sign(payload, config.secret, {
          expiresIn: 10 // minutes
      });
      user_param.email_token = token;
      const user = await user_controller.save(user_param);
      res.status(200).json({ success: true, message: user });
  }catch(err){
      if(err instanceof validator.ParamNotFoundError){
          res.status(400).json({ success: false, message: err.message });
      }else{
          console.log('err',err);
          res.status(500).json({ success: false, message: 'Internal Error' });
      }
      next(err);
  }
});

router.get('/activate/:token', async(req, res, next) => {
    try{
        const token = req.params.token;
        if(typeof(token) == 'undefined' || token == null)
            res.status(400).json({ success: false, message: 'token is required' });
        else{
            let user = await user_controller.activateByToken(token);
            if(typeof(user) == 'undefined' || user == null)
                res.status(404).json({ success: true, message: 'Not found' });
            else
                res.redirect(process.env.YEL_ANA_ACTIVATE_REDIRECT);
        }
    }catch(err){
        if(err instanceof error.NotAuthorizedError){
            res.status(403).json({ success: false, message: err.message });
        } else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        } else {
            console.log('err', err);
            res.status(500).json({success: false, message: 'Internal error'});
        }
        next(err);
    }
});

router.get('/redirect-reset/:token', async(req, res, next) => {
    try{
        const token = req.params.token;
        if(typeof(token) == 'undefined' || token == null)
            res.status(400).json({ success: false, message: 'token is required' });
        else{
            let user = await user_controller.findByToken(token);
            if(typeof(user) == 'undefined' || user == null)
                res.status(404).json({ success: true, message: 'Not found' });
            else
                res.redirect(`${process.env.YEL_ANA_RESET_REDIRECT}/${user.email}/${user.email_token}`);
        }
    }catch(err){
        if(err instanceof error.NotAuthorizedError){
            res.status(403).json({ success: false, message: err.message });
        } else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        } else {
            console.log('err', err);
            res.status(500).json({success: false, message: 'Internal error'});
        }
        next(err);
    }
});

router.post('/reset_password', async(req, res, next) => {
    try{
        const user_param = validator.extract(
            req.body,
            ['email'],//validation
            ['email']//allowed
        );
        const payload = {
            root: false
        };
        const token = jwt.sign(payload, config.secret, {
            expiresIn: 10 // minutes
        });
        user_param.email_token = token;
        await user_controller.resetPassword(user_param);
        res.status(200).json({ success: true, message: 'Reset email was sent to you' });
    }catch(err){
        if(err instanceof validator.ParamNotFoundError){
            res.status(400).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

router.patch('/password', async(req, res, next) => {
    try{
        let user_param = validator.extract(
            req.body,
            ['token','password_new','password_new_confirmation'],//validation
            ['token','password_new','password_new_confirmation']//allowed
        );
        if(user_param.password_new !== user_param.password_new_confirmation){
            throw new error.WrongCredentialsError('password_new does not match password_new_confirmation');
        }
        let user = await user_controller.findByToken(user_param.token);
        if(!user){
            throw new error.WrongCredentialsError('Failed to authenticate token.');
        }
        user.password = user_param.password_new;
        user.email_token = undefined;
        user = await user_controller.encrypt_password(user);
        await user_controller.update(user);
        res.status(204).send();
    }catch(err){
        if(err instanceof validator.ParamNotFoundError
            || err instanceof error.IdFormatError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.WrongCredentialsError){
            res.status(400).json({ success: false, message: err.message });
        }else if(err instanceof error.NotFoundError){
            res.status(404).json({ success: false, message: err.message });
        }else{
            console.log('err',err);
            res.status(500).json({ success: false, message: 'Internal Error' });
        }
        next(err);
    }
});

module.exports = router;
