const express = require('express');
const router = express.Router();
const entity_controller = require('../controllers/entity');
const validator = require('../helpers/validate_object');
const error = require('../helpers/error');

/* GET users listing. */
router.get('/:entity_id', async(req, res, next) => {
  try{
    const entity_id = req.params.entity_id;
    if(typeof(entity_id) == 'undefined' || entity_id == null)
      res.status(400).json({ success: false, message: 'entity_id is required' });
    else{
      const entity = await entity_controller.findOne(entity_id, req.decoded.application._id);
      if(typeof(entity) == 'undefined' || entity == null)
        res.status(404).json({ success: true, message: 'Not found' });
      else
        res.status(200).json({ success: true, message: entity });
    }
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.get('/', async(req, res, next) => {
  try{
    const entity = await entity_controller.findByApplication(req.decoded.application._id);
    res.status(200).json({ success: true, message: entity });
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.post('/', async(req, res, next) => {
  try{
    let entity_param = validator.extract(
      req.body,
      ['code','name'],//validation
      ['code','name','description']//allowed
    );
    entity_param.application = req.decoded.application._id;
    const entity = await entity_controller.save(entity_param);
    res.status(204).send();
  }catch(err){
    if(err instanceof validator.ParamNotFoundError){
      res.status(400).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

router.put('/', async(req, res, next) => {
  try{
    let entity_param = validator.extract(
      req.body,
      ['_id','code','name'],//validation
      ['_id','code','name','description']//allowed
    );
    entity_param.application = req.decoded.application._id;
    const entity = await entity_controller.update(entity_param);
    res.status(204).send();
  }catch(err){
    if(err instanceof validator.ParamNotFoundError
        || err instanceof error.IdFormatError){
      res.status(400).json({ success: false, message: err.message });
    }else if(err instanceof error.NotFoundError){
      res.status(404).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

router.delete('/:entity_id', async(req, res, next) => {
  try{
    const entity_id = req.params.entity_id;
    if(typeof(entity_id) == 'undefined' || entity_id == null)
      res.entity(400).json({ success: false, message: 'entity_id is required' });
    else{
      const entity = await entity_controller.delete(entity_id,req.decoded.application._id);
      res.status(204).send();
    }
  }catch(err){
    if(err instanceof validator.ParamNotFoundError
        || err instanceof error.IdFormatError){
      res.status(400).json({ success: false, message: err.message });
    }else if(err instanceof error.NotFoundError){
      res.status(404).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

module.exports = router;
