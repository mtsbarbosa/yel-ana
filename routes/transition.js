const express = require('express');
let router = express.Router();
const transition_controller = require('../controllers/transition');
const validator = require('../helpers/validate_object');
const error = require('../helpers/error');

router.get('/:transition_id', async(req, res, next) => {
  try{
    let transition_id = req.params.transition_id;
    let application_id = req.locals.application;
    if(typeof(transition_id) === 'undefined' || transition_id == null)
      res.status(400).json({ success: false, message: 'transition_id is required' });
    else{
      let transition = await transition_controller.findOne(transition_id, application_id);
      if(typeof(transition) === 'undefined' || transition == null)
        res.status(404).json({ success: true, message: 'Not found' });
      else
        res.status(200).json({ success: true, message: transition });
    }
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.get('/workflow/:workflow_id', async(req, res, next) => {
  try{
    let workflow_id = req.params.workflow_id;
    let application_id = req.locals.application;
    if(typeof(workflow_id) === 'undefined' || workflow_id == null)
      res.status(400).json({ success: false, message: 'workflow_id is invalid' });
    else{
      let transition = await transition_controller.findByWorkflow(workflow_id,application_id);
      res.status(200).json({ success: true, message: transition });
    }
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.get('/workflow/:workflow_id/status/:status_id', async(req, res, next) => {
  try{
    let workflow_id = req.params.workflow_id;
    let status_from = req.params.status_id;
    let application_id = req.locals.application;
    if(typeof(workflow_id) === 'undefined' || workflow_id == null
        || typeof(status_from) === 'undefined' || status_from == null)
      res.status(400).json({ success: false, message: 'workflow_id or status_id are invalid' });
    else{
      let transition = await transition_controller.findByWorkflowAndStatusFrom(workflow_id,status_from,application_id);
      res.status(200).json({ success: true, message: transition });
    }
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.get('/allowed/workflow/:workflow_id/status/:status_id', async(req, res, next) => {
    try{
        let workflow_id = req.params.workflow_id;
        let status_from = req.params.status_id;
        let application_id = req.locals.application;
        if(typeof(workflow_id) === 'undefined' || workflow_id == null
            || typeof(status_from) === 'undefined' || status_from == null)
            res.status(400).json({ success: false, message: 'workflow_id or status_id are invalid' });
        else{
            let transition = await transition_controller.findAllowedByWorkflowAndStatusFrom(workflow_id,status_from,application_id);
            res.status(200).json({ success: true, message: transition });
        }
    }catch(err){
        console.log('err',err);
        res.status(500).json({ success: false, message: 'Internal error' });
        next(err);
    }
});

router.get('/initial/workflow/:workflow_id', async(req, res, next) => {
  try{
    let workflow_id = req.params.workflow_id;
    let application_id = req.locals.application;
    if(typeof(workflow_id) === 'undefined' || workflow_id == null)
      res.status(400).json({ success: false, message: 'workflow_id is invalid' });
    else{
      let transition = await transition_controller.findInitialByWorkflow(workflow_id,application_id);
      res.status(200).json({ success: true, message: transition });
    }
  }catch(err){
    console.log('err',err);
    res.status(500).json({ success: false, message: 'Internal error' });
    next(err);
  }
});

router.get('/allowed/initial/workflow/:workflow_id', async(req, res, next) => {
    try{
        let workflow_id = req.params.workflow_id;
        let application_id = req.locals.application;
        if(typeof(workflow_id) === 'undefined' || workflow_id == null)
            res.status(400).json({ success: false, message: 'workflow_id is invalid' });
        else{
            let transition = await transition_controller.findAllowedInitialByWorkflow(workflow_id,application_id);
            res.status(200).json({ success: true, message: transition });
        }
    }catch(err){
        console.log('err',err);
        res.status(500).json({ success: false, message: 'Internal error' });
        next(err);
    }
});

router.post('/', async(req, res, next) => {
  try{
    let transition_param = validator.extract(
      req.body,
      ['code','name','workflow', 'status_to'],//validation
      ['code','name','description','workflow', 'status_to', 'status_from']//allowed
    );
    Object.assign(transition_param, req.locals);
    let transition = await transition_controller.save(transition_param);
    res.status(204).send();
  }catch(err){
    if(err instanceof validator.ParamNotFoundError){
      res.status(400).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

router.post('/advance', async(req, res, next) => {
  try{
    let transition_param = validator.extract(
      req.body,
      ['transition_id','object_id'],//validation
      ['transition_id','object_id','object_data','record_data']//allowed
    );
    Object.assign(transition_param, req.locals);
    let transition = await transition_controller.advance(transition_param.transition_id,
                                                          transition_param.object_id,
                                                          transition_param.application,
                                                          transition_param.object_data,
                                                          transition_param.record_data);
    res.status(200).json({ success: true, message: transition });
  }catch(err){
    if(err instanceof validator.ParamNotFoundError
        || err instanceof error.ConditionError
        || err instanceof error.ValidationError
        || err instanceof error.IdFormatError){
      res.status(400).json({ success: false, message: err.message });
    }else if(err instanceof error.NotFoundError){
      res.status(404).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

router.put('/', async(req, res, next) => {
  try{
    let transition_param = validator.extract(
      req.body,
      ['_id','code','name', 'status_to'],//validation
      ['_id','code','name','description', 'status_to', 'status_from']//allowed
    );
    Object.assign(transition_param, req.locals);
    let transition = await transition_controller.update(transition_param);
    res.status(204).send();
  }catch(err){
    if(err instanceof validator.ParamNotFoundError
        || err instanceof error.IdFormatError){
      res.status(400).json({ success: false, message: err.message });
    }else if(err instanceof error.NotFoundError){
      res.status(404).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

router.delete('/:transition_id', async(req, res, next) => {
  try{
    let transition_id = req.params.transition_id;
    let application_id = req.locals.application;
    if(typeof(transition_id) === 'undefined' || transition_id == null)
      res.status(400).json({ success: false, message: 'transition_id is required' });
    else{
      let transition = await transition_controller.delete(transition_id, application_id);
      res.status(204).send();
    }
  }catch(err){
    if(err instanceof validator.ParamNotFoundError
        || err instanceof error.IdFormatError){
      res.status(400).json({ success: false, message: err.message });
    }else if(err instanceof error.NotFoundError){
      res.status(404).json({ success: false, message: err.message });
    }else{
      console.log('err',err);
      res.status(500).json({ success: false, message: 'Internal Error' });
    }
    next(err);
  }
});

module.exports = router;
