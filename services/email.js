const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const config = require("../config");
const template_helper = require("../helpers/template");

module.exports = {
    sendEmail: async(template, to, variables) => {
        if(process.env.NODE_ENV === 'test'){
            return;
        }
        const oauth2Client = new OAuth2(
                                    config.email.auth.clientId,
                                    config.email.auth.clientSecret,
                                    config.email.auth.oauth2Client
                                );

        const mailOptions = {
            from: config.email.auth.user,
            to: to,
            subject: await template_helper.renderTemplate("email/" + template + "/subject.pug", variables),
            generateTextFromHTML: true,
            html: await template_helper.renderTemplate("email/" + template + "/html.pug", variables)
        };

        oauth2Client.setCredentials({
            refresh_token: config.email.auth.refreshToken
        });
        const accessToken = oauth2Client.getAccessToken();

        const smtpTransport = nodemailer.createTransport({
            service: config.email.auth.service,
            auth: {
                type: "OAuth2",
                user: config.email.auth.user,
                clientId: config.email.auth.clientId,
                clientSecret: config.email.auth.clientSecret,
                refreshToken: config.email.auth.refreshToken,
                accessToken: accessToken
            }
        });

        await smtpTransport.sendMail(mailOptions, (error, response) => {
            error ? console.log(error) : console.log(response);
            smtpTransport.close();
        });
    }
};