const express = require('express');
const Configuration = require("./models/configuration");

class ConfigurationSetup {
    constructor() {
        this.secret = process.env.YEL_ANA_SALT_SECRET;
        this.database = require('./db-connections').host;
        this.root_pass = process.env.YEL_ANA_RT_PASS || null;
        this.bcrypt = {
            saltRounds: 10,
        };
        this.mongoose = {
            debug: false,
            autoIndex: require(
                './env-variables')[process.env.NODE_ENV].mongoose.autoIndex
        };
        this.morgan = {
            logger: require(
                './env-variables')[process.env.NODE_ENV].morgan.logger
        };
        this.email = {
            auth: {
                type: 'oauth2',
                service: 'gmail',
                user: process.env.YEL_ANA_MASTER_EMAIL,
                clientId: process.env.YEL_ANA_MASTER_EMAIL_CLIENT_ID,
                clientSecret: process.env.YEL_ANA_MASTER_EMAIL_CLIENT_SECRET,
                refreshToken: process.env.YEL_ANA_MASTER_EMAIL_REFRESH_KEY,
                oauth2Client: process.env.YEL_ANA_MASTER_EMAIL_O2CLIENT,
            }
        };
        //postman testing: this.methods_bypass_cors = ['GET','POST','PUT','DELETE'];
        this.methods_bypass_cors = [];
        this.persistent_configuration = {};
    }

    async initializePersistentConfigs() {
        (await Configuration.model().find({})).forEach(c => {
            this.persistent_configuration[c.key] = c.value;
        });
        console.log("DEBUG", "module.exports.persistent_configuration",
            this.persistent_configuration);
    }
}

module.exports = new ConfigurationSetup();
