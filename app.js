const express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let fs = require('fs');
let cors = require('cors');
const Configuration = require("./models/configuration");

if (process.env.NODE_ENV !== 'test') {
    require('dotenv').config();
}

let config = require('./config');
let db = require('./db');

let app = express();
app.locals.db = db;

let initialize = async (app) => {
    app.locals.db = db;
    await db.initialize();
    await config.initializePersistentConfigs();

    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'pug');

    // uncomment after placing your favicon in /public
    // app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(logger(config.morgan.logger));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));

    // setup index route
    app.use('/yel-ana/', require('./routes/index'));

    //setup middlewares
    if (process.env.NODE_ENV !== 'test') {
        const whitelist = config.persistent_configuration[Configuration.constants.cors_whitelist];
        const corsOptions = {
            origin: function (origin, callback) {
                if (whitelist.indexOf(origin) !== -1) {
                    callback(null, true);
                } else {
                    callback(new Error('Not allowed by CORS'));
                }
            }
        };
        app.use(/^((?!\/user\/redirect-reset|\/user\/activate).)*$/, cors(corsOptions));
    }
    app.use(require('./middlewares/authentication_interceptor'));
    app.use(require('./middlewares/application_interceptor'));
    app.use(require('./middlewares/authentication_user_interceptor'));
    app.use(require('./middlewares/user_interceptor'));

    // setup routes
    let routes = fs.readdirSync('./routes');
    for (let i = 0; i < routes.length; i++) {
        let file = routes[i].substr(0, routes[i].lastIndexOf('.'));
        if (file === 'test' && process.env.NODE_ENV === 'prod') {
            continue;
        }
        app.use('/yel-ana/' + file, require('./routes/' + file));
    }

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        let err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handler
    app.use(function (err, req, res, next) {
        // set locals, only providing error in dev
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'dev' ? err : {};

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });

    if (process.env.NODE_ENV === 'test') {
        app.listen(3000, () => {
            console.log("Server started on port: " + 3000);
            app.emit('started');
        });
    }

};
initialize(app);
module.exports = app;
