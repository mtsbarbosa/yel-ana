const jwt = require('jsonwebtoken');
let config = require('../config');

module.exports = function(req, res, next) {
  if(req.originalUrl.includes('/yel-ana/authenticate')
      || req.originalUrl.includes('/user/authenticate')
      || req.originalUrl.includes('/yel-ana/application')
      || req.originalUrl === '/yel-ana/user'
      || req.originalUrl.startsWith('/yel-ana/user/activate')
      || req.originalUrl.startsWith('/yel-ana/user/password')
      || req.originalUrl.startsWith('/yel-ana/user/redirect-reset')
      || req.originalUrl.startsWith('/yel-ana/user/reset_password')
      || req.originalUrl.startsWith('/yel-ana/test/bool_data')
      || req.originalUrl.startsWith('/yel-ana/test/activate')
      || req.originalUrl.startsWith('/yel-ana/test/change-password')
      || req.originalUrl.startsWith('/yel-ana/test/custom_data'))
    return next();
  // check header or url parameters or post parameters for token
  let token = req.headers['x-access-token'];

  // decode token
  if (token) {
    if((req.originalUrl.startsWith('/yel-ana/test/get_token'))
        && config.root_pass != null && token === config.root_pass){
      return next();
    }

    // verifies secret and checks exp
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        return res.status(403).json({ success: false, message: 'Failed to authenticate token.' });
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({
        success: false,
        message: 'No token provided.'
    });

  }
};
