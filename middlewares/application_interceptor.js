const string_helper = require('../helpers/string_helper');

module.exports = function (req, res, next) {
    if (string_helper.containsAtLeastOne(req.originalUrl,
        ['/status',
            '/condition',
            '/validation',
            '/transition',
            '/workflow'])) {
        if (typeof(req.locals) === 'undefined') {
            req.locals = {};
        }
        if (!req.decoded.root) {
            req.locals.application = req.decoded.application._id;
        } else {
            if (typeof(req.params) != 'undefined') {
                if (req.headers.hasOwnProperty('application_id')) {
                    req.locals.application = req.headers.application_id;
                } else {
                    req.locals.application = req.decoded.application._id;
                }
            } else if (typeof(req.body) != 'undefined') {
                if (req.body.hasOwnProperty('application')) {
                    req.locals.application = req.body.application._id;
                } else {
                    req.locals.application = req.decoded.application._id;
                }
            }
        }
    }
    next();
};
