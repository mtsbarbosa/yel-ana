const config = require('../config');
const jwt = require('jsonwebtoken');
let token_request = require('../token_request');

module.exports = function(req, res, next) {
    // check header or url parameters or post parameters for token
    const token = req.headers['x-temp-token'];
    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) {
                return res.status(403).json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                req.token_request = token_request.requests[token];
                next();
            }
        });

    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }
};
