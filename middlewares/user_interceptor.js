const string_helper = require('../helpers/string_helper');

module.exports = function(req, res, next) {
  if(string_helper.containsAtLeastOne(req.originalUrl,
        ['/application','/authenticate/id'])){
          if(typeof(req.locals) == 'undefined'){
            req.locals = {};
          }
        req.locals.user = req.decoded.user._id;
  }
  next();
};
