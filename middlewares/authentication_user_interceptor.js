let config = require('../config');
let jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {
  if(req.originalUrl.includes('/yel-ana/application')
      || req.originalUrl.includes('/yel-ana/authenticate/id')){
      // check header or url parameters or post parameters for token
      const token = req.body.token || req.query.token || req.headers['x-user-token'];
      // decode token
      if (token) {
          // verifies secret and checks exp
          jwt.verify(token, config.secret, function(err, decoded) {
              if (err) {
                  return res.status(403).json({ success: false, message: 'Failed to authenticate token.' });
              } else {
                  // if everything is good, save to request for use in other routes
                  req.decoded = decoded;
                  next();
              }
          });

      } else {
          // if there is no token
          // return an error
          return res.status(403).send({
              success: false,
              message: 'No token provided.'
          });

      }
  }else{
      return next();
  }
};
