# Description
Yel-Ana is the goddess of the winds <br/> 
Its a powerful standalone server containing a state-machine / workflow API, ready to use <br/> 
Its totally open source, feel free to fork, to copy, to distribute, to use commercially <br/> 

# Features
- Authentication through JWT with multiple Users
- User is capable of having multiple applications allowed to have their own state-machine configured
- Workflow can be all managed through the API
- If needed, its possible to use the execute the transition of the objects between different states using the engine, the history of state changes is then stored

# Pre requisites
- Have mongo db running somewhere in your private network or locally

# Setup for local dev and testing
Copy env as .env and add your environment variables:

- NODE_DATABASE_URL=mongodb://localhost:27017/yel-ana
This is your mongodb url, yel-ana does not support user and password for it yet, so keep it in your private network or locally and do not open its port externally

- YEL_ANA_BASE_URL=http://localhost:3000/yel-ana
This will be the base url for your api, change it to your dns url if you want to or ip

- YEL_ANA_RT_PASS=secret
This is the password for the master user, capable of creating initial credentials and doing all admin operations, pick it wisely

- YEL_ANA_MASTER_EMAIL=yourmaster@someemail.com
This is master admin user email, used for login in and doing admin operations

- YEL_ANA_MASTER_EMAIL_CLIENT_ID=9999999-xmxmxmxs
This is the email OAUTH2 client ID, which will be used to connect to your email server and send all emails

- YEL_ANA_MASTER_EMAIL_CLIENT_SECRET=xXxxxxxXxxxXXXXXX
This is the secret for your OAUTH2 email client, which will be used to connect yo your email server and send all emails

- YEL_ANA_MASTER_EMAIL_REFRESH_KEY=?
This is the refresh token for your OAUTH2 email client, which will be used to connect yo your email server and send all emails

- YEL_ANA_MASTER_EMAIL_O2CLIENT=https://someOauth2Url.com
This is the oauth2 server url where your email server access will be moderated with, which will be used to send all emails

- YEL_ANA_SALT_SECRET=secret
This is the salt secret to generate all JWT tokens, pick it wisely and change it sometimes

- YEL_ANA_ACTIVATE_REDIRECT=http://localhost:3001
When activate email is sent, the link in the email will activate user then, redirect to this success url

- YEL_ANA_RESET_REDIRECT=http://localhost:3001/change_password
When reset password email is sent, the link in the email will start process of changing email, <br/> 
generating a token, then it will redirect user to this redirect url, being the final address the concatenation of: <br/> 
{this env variable redirect url}/{user email}/{temporary token generated} <br/>
In this page which you should provide, in max 10 minutes, user will be able to submit a <br/>
[PATCH] _/user/password_ with body: {token, password_new, password_new_confirmation} <br/>
Saving his new password

- PORT=3000
this is **optional** to setup the port where your server will be running, default is **3000**

# Testing
## Pre requisites
- Setup mongodb locally with url localhost:27017 and leave it running, <br/> or change mongo env passed in **test** command at **package.json**
- Be sure you have a database called yel-ana-test, <br/> or change end of mongodb env passed in **test** command at **package.json**

## Executing tests
- npm run test-unit : execute all unit tests
- npm run test-integration : execute all integration tests
- npm run test : execute unit tests + integration tests

# Executing the server as a developer
npm run dev <br/>
It will read your .env file automatically to start your server

Default api url:
http://localhost:3000/yel-ana


# Deploying with docker
## Pre requisites
- Docker + Docker compose
- If you want to build and keep images somewhere not to need building them before downloading, <br/> you need a paid docker registry or to deploy your own, <br/>then use optional commands bellow
- Mongo db server running in accessible location with yel-ana database created (can be empty)
- If you are running mongo in the same location as your server, look for mongod.conf and add a comma and your local ipv4 address to bindIp (e. g. bindIp: 127.0.0.1,192.148.0.26)
- **Linux or MAC**, I've specified bellow commands for linux-like terminals, if you prefer Windows or any other OS, feel free to adapt them

## Build
- (optional) docker login -u="username" -p="password" docker_repo_url <br/>
- docker build -t yel-ana:{version} . <br/>
- (optional) docker image push yel-ana:{version} <br/>
- (optional) docker logout docker_repo_url <br/>

## Deployment
- git checkout ssh://<your_repo_url> .git
- cp yel-ana/deployment <your desired path for storing server files e. g. /etc/server/yel-ana >
- rm -rf yel-ana/deployment 
- cd /etc/server/yel-ana (or your path)
- (optional) docker login -u="username" -p="password" docker_repo_url
- (optional) docker image pull yel-ana:{version}
- generate or write manually .env file based on /development/env
- YEL_ANA_RELEASE=1.0.0-SNAPSHOT docker-compose up -d --force-recreate
- (optional) docker logout docker_repo_url <br/>

## Useful docker commands
- docker ps : list all running containers
- docker ps -a : list all containers, even stopped ones
- docker start <container_name>
- docker stop <container_name>
- docker rm <container_name> : remove container from local docker
- docker image ls : list all local pulled or generated images
- docker rmi <image_id or image_name> : remove image from local docker
- docker logs <container_name> : check startup logs for container
- docker exec -it <container_name> /bin/bash : open command line inside running docker container



Enjoy :)
