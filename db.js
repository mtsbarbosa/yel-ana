const mongoose = require( 'mongoose' );
const express = require('express');
let path = require('path');
let fs = require('fs');
let config = require('./config');
const string_helper = require('./helpers/string_helper');

let Schema   = mongoose.Schema;

let models = fs.readdirSync('./models');

for (let i = 0; i < models.length; i++) {
    const file = models[i].substr(0, models[i].lastIndexOf('.'));
    mongoose.model( string_helper.transformUnderlineToCamelCase(file), require('./models/' + file).schema());
}

mongoose.Promise = global.Promise;

if(config.mongoose.debug){
  mongoose.set('debug', function (collectionName, method, query, doc) {
    let set = {
          coll: collectionName,
          method: method,
          query: query,
          doc: doc
      };
    console.log('QUERY=>',set);
  });
}

mongoose.set('useCreateIndex', true);

module.exports = {
  connection: null,
  initialize: async() => {
     mongoose.Promise = global.Promise;
     console.log('database',config.database);
     await mongoose.connect( config.database, { useNewUrlParser: true, useUnifiedTopology: true });
     return mongoose.connection;
  }
};
