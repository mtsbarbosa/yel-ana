const Configuration = require('../models/configuration');

exports.up = async function(){
  await Configuration.model().deleteOne({
    key: Configuration.constants.cors_whitelist,
  });
  await Configuration.model().create({
    key: Configuration.constants.cors_whitelist,
    value: ["http://localhost:3001","http://0.0.0.0:3001","https://localhost:3001","https://0.0.0.0:3001","http://127.0.0.1:3001","https://127.0.0.1:3001"]
  });
};

exports.down = function(){
};
