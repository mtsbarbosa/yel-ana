const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config');
let token_request = require('../token_request');
const user_controller = require('../controllers/user');
const error = require('../helpers/error');

module.exports = {
    authenticate_for_temp_token: async function(email, password, minutes) {
        const user = await user_controller.findWithPasswordByEmail(email);
        if(!user){
            throw new error.NotAuthorizedError('Not authorized');
        }

        const assignToken = function() {
            const payload = {
                root: user.root
            };
            const token = jwt.sign(payload, config.secret, {
                expiresIn: minutes
            });
            token_request.requests[token] = {
                user: user,
                date: new Date()
            };
            return token;
        };

        if(process.env.NODE_ENV === 'test'){
            if(password === user.password){
                return assignToken();
            }else{
                throw new error.NotAuthorizedError('Not authorized');
            }
        }else{
            const password = await bcrypt.compare(password, user.password);
            try{
                if(password == true){
                    return assignToken();
                }else{
                    throw new error.NotAuthorizedError('Not authorized');
                }
            }catch (err) {
                throw new error.NotAuthorizedError('Not authorized');
            }
        }
    }
}
