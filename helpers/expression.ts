import ExpressionConditionalOptionsVO from "../vo/expression_conditional_options";
import ExpressionWorkflowOptionsVO from "../vo/expression_workflow_options";

const filtrex = require('@m93a/filtrex');
const randomstring = require("randomstring");

class ExpressionHelper {
    static async compileBoolean(expression: string, options: ExpressionConditionalOptionsVO){
        let filtrexOptions = options.functionList();
        let objects: object = options.objectList();
        expression = await this.preExecute(expression,options,filtrexOptions,objects,this.addObjects);
        expression = await this.preExecute(expression,options,filtrexOptions,objects,this.preExecuteRequest);
        let boolExec = filtrex.compileExpression(expression, filtrexOptions);
        let result: any = boolExec(objects);
        return (result === 1 || result === true || result === "true" || result === "1");
    }

    static async compile<T extends ExpressionWorkflowOptionsVO>(expression: string, options: T){
        let filtrexOptions = options.functionList();
        let objects: object = options.objectList();
        expression = await this.preExecute(expression,options,filtrexOptions,objects,this.addObjects);
        expression = await this.preExecute(expression,options,filtrexOptions,objects,this.preExecuteRequest);
        let exec = filtrex.compileExpression(expression, filtrexOptions);
        return exec(objects);
    }

    static async preExecute<T extends ExpressionWorkflowOptionsVO>(expression: string, options: T, filtrexOptions: object, objects: object, method: Function): Promise<string>{
        let execReq: ExecutedReplace = await method(expression,filtrexOptions, options);
        if(execReq){
            expression = execReq.expression;
            for (let key in execReq.objects) {
                let value = execReq.objects[key];
                objects[key] = value;
            }
            return this.preExecute(expression,options,filtrexOptions,objects,method);
        }
        return expression;
    }

    static async preExecuteRequest<T extends ExpressionWorkflowOptionsVO>(expression: string, filtrexOptions: object, options: T): Promise<ExecutedReplace>{
        let regexp = /(req)+\([^\)]*\)(\.[^\)]*\))?/;
        let result = expression.match(regexp);

        if(result){
            let key = randomstring.generate({
                length: 12,
                charset: 'alphabetic'
            });
            let execReq: ExecutedReplace  = new ExecutedReplace(expression.replace(result[0],key),{});
            let exec = filtrex.compileExpression(result[0], filtrexOptions);
            execReq.objects[key] = await exec(options.objectList());
            return execReq;
        }
        return null;
    }

    static async addObjects<T extends ExpressionWorkflowOptionsVO>(expression: string, filtrexOptions: object, options: T): Promise<object>{
        let regexp = /(\{[^\{\}]+\}|\{\})/;
        let result = expression.match(regexp);

        if(result){
            let key = randomstring.generate({
                length: 12,
                charset: 'alphabetic'
            });
            let execReq: ExecutedReplace  = new ExecutedReplace(expression.replace(result[0],'"' + key + '"'),{});
            options.replacedObjects[key] = JSON.parse(result[0]);
            return execReq;
        }
        return null;
    }
}

class ExecutedReplace {
    private _expression: string;
    private _objects: object;

    constructor(expression: string, objects: object) {
        this._expression = expression;
        this._objects = objects;
    }

    get expression(): string {
        return this._expression;
    }

    set expression(value: string) {
        this._expression = value;
    }

    get objects(): object {
        return this._objects;
    }

    set objects(value: object) {
        this._objects = value;
    }
}

export = ExpressionHelper;