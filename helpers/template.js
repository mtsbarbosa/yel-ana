const pug = require('pug');

module.exports = {
    renderTemplate: async(template, variables) => {
        const compiledFunction = pug.compileFile('resources/templates/' + template);
        return compiledFunction(variables);
    }
};