module.exports = {
    ExtendableError: class ExtendableError extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.stack = (new Error()).stack;
            this.name = this.constructor.name;
        }
    },
    IdFormatError: class IdFormatError extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.stack = (new Error()).stack;
            this.name = this.constructor.name;
        }
    },
    NotFoundError: class NotFoundError extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.stack = (new Error()).stack;
            this.name = this.constructor.name;
        }
    },
    ValidationError: class ValidationError extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.stack = (new Error()).stack;
            this.name = this.constructor.name;
        }
    },
    ConditionError: class ConditionError extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.stack = (new Error()).stack;
            this.name = this.constructor.name;
        }
    },
    AlreadyLinkedError: class AlreadyLinkedError extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.stack = (new Error()).stack;
            this.name = this.constructor.name;
        }
    },
    NotAuthorizedError: class NotFoundError extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.stack = (new Error()).stack;
            this.name = this.constructor.name;
        }
    },
    WrongCredentialsError: class NotFoundError extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.stack = (new Error()).stack;
            this.name = this.constructor.name;
        }
    }
};
